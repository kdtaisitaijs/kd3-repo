{
	"Symbol": "unit",
	"T": "C°",
	"m": "kg",
	"v": "m/s",
	"a": "m/s^2",
	"t": "s",
	"F": "N",
	"I": "A",
	"U": "V",
	"R": "Ω",
	"P": "W",
	"p": "pa",	
}