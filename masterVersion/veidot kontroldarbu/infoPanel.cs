﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Xml.Linq;

namespace masterVersion
{
    class infoPanel
    {
        TableLayoutPanel taskMainPanel = new TableLayoutPanel();
        Panel scrollPanel = new Panel();
        TableLayoutPanel uzdevumuPanel = new TableLayoutPanel();
        // TableLayoutPanel tasks = new TableLayoutPanel();
        TableLayoutPanel[] uzdevumsPanel;
        KlasuTemas klasuTemas = new KlasuTemas();
        DatabaseAccess database = new DatabaseAccess();
        Button temuPanel = new Button();
        Uzdevums uzdevums = new Uzdevums();
        List<Uzdevums> uzdevumuListTema = new List<Uzdevums>();
        SubMenuTestGen Parent;
        int temuRow;
        string tema;
        TableLayoutPanel[] taskInfo;
        TextBox[] formText;
        RichTextBox[] text;
        TableLayoutPanel[] extraInfo;
        PictureBox[] difficultyArt;
        PictureBox[] iconTheory;
        PictureBox[] iconMulitpleChoice;
        PictureBox[] iconCalclulations;
        PictureBox[] iconHasPicture;
        ToolTip toolTip = new ToolTip();
        string Tooltext;
        int controlHeight = 300;

        public Panel createTaskPanel(int klase, int pageLocationX, int toolTipHeight, Size clientSize, SubMenuTestGen parent, string _tema = "nav")
        {
            Parent = parent;
            tema = _tema;

            taskMainPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
            taskMainPanel.Height = clientSize.Height - (clientSize.Height - parent.Height - toolTipHeight - 10);
            taskMainPanel.Width = clientSize.Width / 3;
            taskMainPanel.Anchor = AnchorStyles.None;
            taskMainPanel.Location = new Point(10, toolTipHeight-2);
            uzdevumuPanel.Width = taskMainPanel.Width;
           // uzdevumuPanel.Width = taskMainPanel.Width - 20;
            uzdevumuPanel.Height = taskMainPanel.Height;

            this.makeTemuPanels(tema);
            return taskMainPanel;
        }


        private void makeTemuPanels(string tema)
        {
            temuPanel.Name = tema;
            temuPanel.Text = tema;
            temuPanel.TextAlign = ContentAlignment.MiddleCenter;
            temuPanel.Font = new System.Drawing.Font(temuPanel.Font.Name, temuPanel.Font.Size + 5, temuPanel.Font.Style, temuPanel.Font.Unit);
            temuPanel.Size = new Size(uzdevumuPanel.Width - 10, 50);
            temuPanel.FlatStyle = FlatStyle.Flat;
            temuPanel.FlatAppearance.BorderColor = Color.DarkBlue;
            temuPanel.FlatAppearance.BorderSize = 1;
            temuPanel.ForeColor = Color.White;
            temuPanel.BackColor = Color.FromArgb(66, 139, 202);
            temuPanel.FlatStyle = FlatStyle.Flat;

            uzdevumuPanel.Controls.Add(temuPanel, 0, 0);
            taskMainPanel.Controls.Add(uzdevumuPanel);
            uzdevumuPanel.Height = taskMainPanel.Height - 10;
            temuRow += 1;

            this.populateTasks(tema);
            this.makePanels();
        }

        private void populateTasks(string topic)
        {
            if (topic == "Visas")
            {
                topic = null;
            }
            if (database.Connect())
            {
                if (uzdevumuListTema != database.findExerciseBytopic(topic))
                {
                    uzdevumuListTema = database.findExerciseBytopic(topic);
                }
                database.closeConncetion();
            }
        }

        private void makePanels()
        {
            uzdevumsPanel = new TableLayoutPanel[uzdevumuListTema.Count];
            List<string> formulas = new List<string>();
            int count = uzdevumuListTema.Count;
            formText = new TextBox[count];
            text = new RichTextBox[count];
            extraInfo = new TableLayoutPanel[count];
            taskInfo = new TableLayoutPanel[count];
            Button[] addBut = new Button[count];
            difficultyArt = new PictureBox[count];
            iconTheory = new PictureBox[count];
            iconCalclulations = new PictureBox[count];
            iconHasPicture = new PictureBox[count]; 
            iconMulitpleChoice = new PictureBox[count];

            int tasksToAdd = count;
            uzdevumuPanel.SuspendLayout();

            for (int i = 0; i < tasksToAdd; i++)
            {
                uzdevums = uzdevumuListTema.ElementAt(i);
                uzdevumsPanel[i] = new TableLayoutPanel();
                uzdevumsPanel[i].Name = i.ToString(); //
                uzdevumsPanel[i].Size = new Size(uzdevumuPanel.Width - 20, 100);
               // uzdevumsPanel[i].CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                uzdevumsPanel[i].CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
                uzdevumsPanel[i].ColumnCount = 3;
                uzdevumsPanel[i].BackColor = Color.White;

                if (uzdevums.getEquationAsString() != null)
                {
                    formulas = uzdevums.getEquation().ToList();
                }
     
                extraInfo[i] = new TableLayoutPanel();
                extraInfo[i].Size = new Size(uzdevumuPanel.Width, 100);
                extraInfo[i].BackColor = Color.White;
                extraInfo[i].Visible = false;
                //TRUE JA IR ATTĒLS, šobrīd attēls čakarējas 
  
                formText[i] = new TextBox();
                formText[i].Name = uzdevumsPanel[i].Name; 

                if (formulas.Count > 0)
                {
                    //formText[i].Text = formulas.ElementAt(0);
                    for (int k = 0; k < formulas.Count; k++)
                    {
                        if(formulas.ElementAt(k)!= "NULL")
                            formText[i].Text += formulas.ElementAt(k) + " ";
                    }
                }
                formText[i].Size = new Size(100,  25);
                formText[i].BorderStyle = BorderStyle.None;
                formText[i].Font = new System.Drawing.Font(formText[i].Font.Name, formText[i].Font.Size + 3, formText[i].Font.Style, formText[i].Font.Unit);
                formText[i].MouseHover += form_hover;
                formText[i].ReadOnly = true;
                formText[i].BackColor = Color.White;
                formText[i].BorderStyle = BorderStyle.None;

                text[i] = new RichTextBox();
                text[i].MaxLength = 450;
                text[i].Font = new System.Drawing.Font(text[i].Font.Name, text[i].Font.Size + 3, text[i].Font.Style, text[i].Font.Unit);
                text[i].Size = new Size(uzdevumsPanel[i].Width - 34, 65);

                //make text box bigger
                if (uzdevums.getText().Length > 200)
                {
                    text[i].Size = new Size(uzdevumsPanel[i].Width - 34, 100);
                    uzdevumsPanel[i].Height = uzdevumsPanel[i].Height + 35;

                    if (uzdevums.getText().Length > 300)
                    {
                        text[i].Size = new Size(uzdevumsPanel[i].Width - 34, 120);
                        uzdevumsPanel[i].Height = uzdevumsPanel[i].Height + 20;

                        if (uzdevums.getText().Length > 400)
                        {
                            text[i].Size = new Size(uzdevumsPanel[i].Width - 34, 140);
                            uzdevumsPanel[i].Height = uzdevumsPanel[i].Height + 20;
                        }
                    }
                }


                text[i].Text = uzdevums.getText();
                text[i].BorderStyle = BorderStyle.None;
                text[i].Name = uzdevumsPanel[i].Name;
                text[i].Click += More_Click; 

                taskInfo[i] = new TableLayoutPanel();
                taskInfo[i].Size = new Size(uzdevumuPanel.Width - 50, 30);
                taskInfo[i].Name = uzdevumsPanel[i].Name;
                taskInfo[i].Controls.Add(formText[i], 0, 0);
                //taskInfo[i].CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

                addBut[i] = new Button();
                addBut[i].Name = i.ToString();
                addBut[i].Text = "+";
                addBut[i].Size = new Size(20, uzdevumsPanel[i].Height - 4);
                addBut[i].Click += InfoPanel_Click;

                difficultyArt[i] = new PictureBox();
                difficultyArt[i].Size = new Size(200, 20);
                difficultyArt[i].Tag = "diff";
                difficultyArt[i].MouseHover += toolTip_hover;


                iconTheory[i] = new PictureBox();
                iconTheory[i].Size = new Size(20, 20);
                iconTheory[i].Tag = "theory";
                iconTheory[i].MouseHover += toolTip_hover;


                iconCalclulations[i]= new PictureBox();
                iconCalclulations[i].Size = new Size(20, 20);
                iconCalclulations[i].Tag = "calc";
                iconCalclulations[i].MouseHover += toolTip_hover;


                iconHasPicture[i] = new PictureBox();
                iconHasPicture[i].Size = new Size(20, 20);
                iconHasPicture[i].Tag = "picture";
                iconHasPicture[i].MouseHover += toolTip_hover;


                iconMulitpleChoice[i] = new PictureBox();
                iconMulitpleChoice[i].Size = new Size(20, 20);
                iconMulitpleChoice[i].Tag = "multiple";
                iconMulitpleChoice[i].MouseHover += toolTip_hover;


                int difficulty = uzdevums.getDifficulty();
                switch (difficulty)
                {
                    case 1:
                        difficultyArt[i].Image = Properties.Resources.diffculty_1;
                        break;
                    case 2:
                        difficultyArt[i].Image = Properties.Resources.diffculty_2;
                        break;
                    case 3:
                        difficultyArt[i].Image = Properties.Resources.diffculty_3;
                        break;
                    case 4:
                        difficultyArt[i].Image = Properties.Resources.diffculty_4;
                        break;
                    case 5:
                        difficultyArt[i].Image = Properties.Resources.diffculty_5;
                        break;

                    default:
                        difficultyArt[i].Image = Properties.Resources.diffculty_2;
                        break;
                }



                if(uzdevums.getIsMultChoice() == true)
                {
                    
                    iconMulitpleChoice[i].Image = Properties.Resources.icon_3;
                }
                else
                {
                    iconMulitpleChoice[i].Image = Properties.Resources.icon_4;
                    iconMulitpleChoice[i].Tag = "bez";
                }

                if(uzdevums.getIsTheory() == true)
                {
                   
                    iconTheory[i].Image = Properties.Resources.icon;
                }
                else
                {
                    iconTheory[i].Image = Properties.Resources.icon_4;
                    iconTheory[i].Tag = "bez";
                }

                if(uzdevums.getEquationAsString() != null)
                {
                    
                    iconCalclulations[i].Image = Properties.Resources.icon_1;
                }
                else
                {
                    iconCalclulations[i].Image = Properties.Resources.icon_4;
                    iconCalclulations[i].Tag = "bez";
                }

                if(uzdevums.getImagePaths() != null)
                {
                    
                    iconHasPicture[i].Image = Properties.Resources.icon_2;
                }
                else
                {
                    iconHasPicture[i].Image = Properties.Resources.icon_4;
                    iconHasPicture[i].Tag = "bez";
                }
                taskInfo[i].Controls.Add(iconMulitpleChoice[i], 3, 0);
                taskInfo[i].Controls.Add(iconTheory[i], 1, 0);
                taskInfo[i].Controls.Add(iconCalclulations[i], 2, 0);
                taskInfo[i].Controls.Add(iconHasPicture[i], 4, 0);
                taskInfo[i].Controls.Add(difficultyArt[i], 5, 0);
                
                uzdevumsPanel[i].Name = i.ToString();
                uzdevumsPanel[i].Controls.Add(addBut[i], 3, 0);
                uzdevumsPanel[i].SetRowSpan(addBut[i], 3);
                uzdevumsPanel[i].Controls.Add(text[i], 1, 1);
                uzdevumsPanel[i].Controls.Add(taskInfo[i], 1, 0);
                
                uzdevumsPanel[i].AutoScroll = false;

                uzdevumsPanel[i].Click += More_Click;
                uzdevumuPanel.Controls.Add(uzdevumsPanel[i], 0, temuRow);
                controlHeight += uzdevumsPanel[i].Height;
                if (controlHeight >= taskMainPanel.Height)
                {
                    uzdevumuPanel.Height += uzdevumsPanel[i].Height;
                }

                if (uzdevums.getImagePaths() != null)
                {
                    uzdevumuPanel.Controls.Add(extraInfo[i], 0, temuRow + 1);
                }
                temuRow += 2;
                uzdevumuPanel.BackColor = Color.FromArgb(212, 224, 252);     
            }
        
            uzdevumuPanel.AutoScroll = false;
            uzdevumuPanel.HorizontalScroll.Enabled = false;
            uzdevumuPanel.HorizontalScroll.Visible = false;
            uzdevumuPanel.HorizontalScroll.Maximum = 0;
            uzdevumuPanel.AutoScroll = true;  

            taskMainPanel.AutoScroll = false;
            taskMainPanel.HorizontalScroll.Enabled = false;
            taskMainPanel.HorizontalScroll.Visible = false;
            taskMainPanel.HorizontalScroll.Maximum = 0;
            taskMainPanel.AutoScroll = true;
            uzdevumuPanel.ResumeLayout();
        }

        private void InfoPanel_Click(object sender, EventArgs e)
        {
            Button test = sender as Button;
            int id = int.Parse(test.Name);
            Uzdevums toAdd = uzdevumuListTema.ElementAt(id);
            Parent.currentPage.addUzdevums(toAdd);
        }

        private void toolTip_hover(object sender, EventArgs e)
        {
            PictureBox picture = sender as PictureBox;

            if (picture.Tag == "bez")
            {
                Tooltext = "";
            }
            else if(picture.Tag == "diff") 
            {
                Tooltext = "Grūtība";
            }
            else if (picture.Tag == "theory")
            {
                Tooltext = "Teorijas uzdevums";
            }
            else if (picture.Tag == "calc")
            {
                Tooltext = "Rēķināms uzdevums";
            }
            else if (picture.Tag == "picture")
            {
                Tooltext = "Uzdevums ar attēlu";
            }
            else if (picture.Tag == "multiple")
            {
                Tooltext = "Uzdevums ar atbilžu variantiem";
            }

            toolTip.SetToolTip(picture, Tooltext);
        }

        private void form_hover(object sender, EventArgs e)
        {
            TextBox text = sender as TextBox;
            toolTip.SetToolTip(text, text.Text);
        }


        private void More_Click(object sender, EventArgs e)
        {
            Control test = sender as Control;
            int extraInfoRow = int.Parse(test.Name);
                
            if(extraInfo[extraInfoRow].Visible == false)
            {
                //extraInfo[extraInfoRow].Visible = true;
            }
            else
            {
               // extraInfo[extraInfoRow].Visible = false;
            }
                
        }
    }
}


