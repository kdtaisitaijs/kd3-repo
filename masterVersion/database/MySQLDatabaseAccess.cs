﻿using System;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections.Generic;
using masterVersion.database;


namespace masterVersion
{
	public class DatabaseAccess
	{
        private readonly static String Host = "taskdb2.ckxunc6ggcdi.eu-central-1.rds.amazonaws.com";
		//private readonly static String Host = "ingeniumtasks1.ckxunc6ggcdi.eu-central-1.rds.amazonaws.com";
		private readonly static int PORT = 3306;
		private readonly static String DATABASE_NAME = "innodb";
		private readonly static String USER = "ingenium";
		private readonly static String PASSWORD = "tomsnezinparoli";

		private static MySqlConnection connection;

		private Boolean connected = false;

		public DatabaseAccess()
		{

			String connectionString = "Server = " + Host + ";" +
				"Port = " + PORT.ToString() + ";" +
				"Database = " + DATABASE_NAME + ";" +
				"User ID = " + USER + ";" +
				"Password = " + PASSWORD + ";" +
				"Pooling = false;" +
				"charset=utf8;";

			connection = new MySqlConnection(connectionString);
			



		}

		public Boolean Connect() 
		{
			try
			{
				connection.Open();
				connected = true;
				return connected;
			}
			catch(Exception e)
			{
				connected = false;
				Console.Write(e.Message);
				return connected;
			}

		}

		public void closeConncetion()
		{
			try
			{
				connection.Close();
				connected = false;
			}
			catch(Exception e)
			{
				//TODO do something here
			}
			
		}


		public int insertExercise(Uzdevums uzdevums)
		{

			if (!connected)
				return 0;

			int rowsAffected = 0;

			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "INSERT INTO `uzdevumu_table` (grade,topic,text,difficulty,equation,choices,answer,is_theoretical,is_multchoise,subtopic,pictureURL)" +
				"VALUES (@grade,@topic,@text,@difficulty,@equation,@choices,@answer,@is_theoretical,@is_multchoise,@subtopic,@pictureURL);";
			query.Parameters.AddWithValue("@grade", uzdevums.getGrade());
			query.Parameters.AddWithValue("@topic", uzdevums.getTopic());
			query.Parameters.AddWithValue("@text", uzdevums.getText());
			query.Parameters.AddWithValue("@difficulty", uzdevums.getDifficulty());
			query.Parameters.AddWithValue("@equation", uzdevums.getEquationAsString());
			query.Parameters.AddWithValue("@choices", uzdevums.getMultChoicesAsString());
			query.Parameters.AddWithValue("@answer", uzdevums.getAnswer());
			query.Parameters.AddWithValue("@is_theoretical", uzdevums.getIsTheory());
			query.Parameters.AddWithValue("@is_multchoise", uzdevums.getIsMultChoice());
			query.Parameters.AddWithValue("@subtopic", uzdevums.getSubTopic());
			query.Parameters.AddWithValue("@pictureURL", uzdevums.getImagePathsAsString());
			try {
				rowsAffected = query.ExecuteNonQuery();
			}
			catch(MySqlException e) { }
			
			return rowsAffected;

		}
		//public int insertExercise(int grade, String topic, String text, int difficulty)
		//{

		//    int rowsAffected = 0;

		//    MySqlCommand query = connection.CreateCommand();
		//    query.CommandText = "INSERT INTO `Uzdevumi` (grade,topic,text,difficulty,equation) VALUES (@grade,@topic,@text,@difficulty,@equation);";
		//    query.Parameters.AddWithValue("@grade", grade);
		//    query.Parameters.AddWithValue("@topic", topic);
		//    query.Parameters.AddWithValue("@text", text);
		//    query.Parameters.AddWithValue("@difficulty", difficulty);
		//    query.Parameters.AddWithValue("@equation", equation);

		//    rowsAffected = query.ExecuteNonQuery();
		//    return rowsAffected;

		//}


		public int deleteExercise(int id)
		{

			if (!connected)
				return 0;

			int rowsAffected = 0;
			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "DELETE FROM `uzdevumu_table` WHERE id = @id;";
			query.Parameters.AddWithValue("@id", id);
			rowsAffected = query.ExecuteNonQuery();
			return rowsAffected;
		}
		public int deleteExercise(Uzdevums uzdevums)
		{
			if (!connected)
				return 0;

			int rowsAffected = 0;
			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "DELETE FROM `uzdevumu_table` WHERE id = @id;";
			query.Parameters.AddWithValue("@id", uzdevums.getId());
			rowsAffected = query.ExecuteNonQuery();
			return rowsAffected;
		}

		public int updateExercise(Uzdevums uzdevums)
		{
			if (!connected)
				return 0;

			int rowsAffected = 0;
			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "UPDATE `uzdevumu_table` SET grade=@grade, topic=@topic, text=@text," +
				" difficulty=@difficulty, equation=@equation,"+
				" answer=@answer, choices=@choices, is_theroretical=@is_theoretical, " +
				"is_multchoise=@is_multchoise, subtopic=@subtopic, pictureURL=@pictureURL WHERE id = @id;";
			query.Parameters.AddWithValue("@grade", uzdevums.getGrade());
			query.Parameters.AddWithValue("@topic", uzdevums.getTopic());
			query.Parameters.AddWithValue("@text", uzdevums.getText());
			query.Parameters.AddWithValue("@difficulty", uzdevums.getDifficulty());
			query.Parameters.AddWithValue("@equation", uzdevums.getEquationAsString());
			query.Parameters.AddWithValue("@choices", uzdevums.getMultChoicesAsString());
			query.Parameters.AddWithValue("@answer", uzdevums.getAnswer());
			query.Parameters.AddWithValue("@id", uzdevums.getId());
			query.Parameters.AddWithValue("@is_theoretical", uzdevums.getIsTheory());
			query.Parameters.AddWithValue("@is_multchoise", uzdevums.getIsMultChoice());
			query.Parameters.AddWithValue("@subtopic", uzdevums.getSubTopic());
			query.Parameters.AddWithValue("@pictureURL", uzdevums.getImagePathsAsString());

			rowsAffected = query.ExecuteNonQuery();
			return rowsAffected;
		}


		/*public List<Uzdevums> findExercise(int grade, String topic, int difficulty)
		{
			List<Uzdevums> uzdevumi = new List<Uzdevums>();
			if (grade != 0)
			{
			   foreach(Uzdevums uzd in LocalDB.DB)
				{
					if (uzd.getGrade() == grade)
						uzdevumi.Add(uzd);
				}
			}
			else
			{
				if (topic != null)
				{
					foreach (Uzdevums uzd in LocalDB.DB)
					{
						if (uzd.getTopic().Equals(topic))
							uzdevumi.Add(uzd);
					}

				}
				else
				{
					//Difficulty
					foreach (Uzdevums uzd in LocalDB.DB)
					{
						if (uzd.getDifficulty() == difficulty)
							uzdevumi.Add(uzd);
					}

				}

			}
				
			



				return uzdevumi;
		}*/
	   public List<Uzdevums> findExercise(int grade, String topic, int difficulty)
		{
			if (!connected)
				return null;

			List<Uzdevums> uzdevumi = new List<Uzdevums>();

			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "SELECT * FROM `uzdevumu_table` WHERE" +
				" KLASE LIKE @grade AND TEMA LIKE @topic AND GRUTIBA LIKE @difficulty";
			if (grade != 0)
				query.Parameters.AddWithValue("@grade", "%" + grade + "%");
			else
				query.Parameters.AddWithValue("@grade", "%");
			if (topic != null)
				query.Parameters.AddWithValue("@topic", "%" + topic + "%");
			else
				query.Parameters.AddWithValue("@topic", "%");
			if (difficulty != 0)
				query.Parameters.AddWithValue("@difficulty", "%" + difficulty + "%");
			else
				query.Parameters.AddWithValue("@difficulty", "%");


			MySqlDataReader dataReader = query.ExecuteReader();
			if (dataReader != null)
				while (dataReader.Read())
				{
					Uzdevums uzd = new Uzdevums(dataReader.GetInt32("ID"), dataReader.GetInt32("KLASE"), dataReader.GetString("TEMA"),
						 dataReader.GetString("TEKSTS"), dataReader.GetInt32("GRUTIBA"));

					String vs;
					String os;
					String eq;
					String answ;
					String choices;

					try
					{
						eq = dataReader.GetString("VIENADOJUMS");
					}
					catch (Exception e)
					{
						eq = null;
					}
					try
					{
						answ = dataReader.GetString("ATBILDE");
						uzd.setIsTheory(answ.ToLower() != "null");
					}
					
					catch (Exception e)
					{
						answ = null;
					}
					try
					{
						uzd.setSubTopic(dataReader.GetString("APAKSTEMAl"));
					}
					catch (Exception e) { }				
				
					try
					{
						choices = dataReader.GetString("ATB_VAR");
                        uzd.setIsMultChoice(choices.ToLower() != "null");
                    }
					catch (Exception e)
					{
						choices = null;
					}
					try
					{
						uzd.setImagePathsFromString(dataReader.GetString("ATTELS"));
					}catch (Exception e)
					{}
					uzdevumi.Add(uzd);
					uzd.parseEquation(eq);
					uzd.parseMultChoices(choices);
					uzd.setAnswer(answ);

				}
			dataReader.Close();
			return uzdevumi;
		}

		public List<Uzdevums> findExerciseByGrade(int grade)
		{
			return findExercise(grade, null, 0);
		}

		public List<Uzdevums> findExerciseBytopic(String topic)
		{
			return findExercise(0, topic, 0);
		}

		public List<Uzdevums> findExerciseBydifficulty(int difficulty)
		{
			return findExercise(0, null, difficulty);
		}

		public List<Uzdevums> findExerciseBytopics(String[] topics)
		{
			List<Uzdevums> uzdevumi = new List<Uzdevums>() ;
			foreach (String topic in topics)
				uzdevumi.AddRange(findExercise(0, topic, 0));
			return uzdevumi;
		}
		public List<Uzdevums> findExerciseByDifficulties(int[] difficulty)
		{
			List<Uzdevums> uzdevumi = new List<Uzdevums>();
			foreach (int i in difficulty)
				uzdevumi.AddRange(findExercise(0, null, i));
			return uzdevumi;
		}
		public List<Uzdevums> findExerciseByDifficultyRange(int minimalDifficulty,int maximalDifficulty)
		{
			List<Uzdevums> uzdevumi = new List<Uzdevums>();
			for (int i=minimalDifficulty;i<=maximalDifficulty;i++)
				uzdevumi.AddRange(findExercise(0, null, i));
			return uzdevumi;
		}

	   public Uzdevums findExerciseByID(int id)
		{
			if (!connected)
				return null;

			

			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "SELECT * FROM `uzdevumu_table` WHERE " + 
				"id = @id";
				query.Parameters.AddWithValue("@id", id);

			Uzdevums uzd =null;
			MySqlDataReader dataReader = query.ExecuteReader();
			if (dataReader != null)
				while (dataReader.Read())
				{
					uzd = new Uzdevums(dataReader.GetInt32("id"), dataReader.GetInt32("grade"), dataReader.GetString("topic"),
						 dataReader.GetString("text"), dataReader.GetInt32("difficulty"));

					String vs;
					String os;
					String eq;
					String answ;
					String choices;

					try
					{
						eq = dataReader.GetString("equation");
					}
					catch (Exception e)
					{
						eq = null;
					}
					try
					{
						answ = dataReader.GetString("answer");
						uzd.setIsTheory(true);
					}
					catch (Exception e)
					{
						answ = null;
					}
					try
					{
						choices = dataReader.GetString("choices");
					}
					catch (Exception e)
					{
						choices = null;
					}
					try
					{
						uzd.setImagePathsFromString(dataReader.GetString("pictures"));
					}
					catch (Exception e)
					{ }
					//uzdevumi.Add(uzd);
					uzd.parseEquation(eq);
					uzd.parseMultChoices(choices);
					uzd.setAnswer(answ);

				}
			dataReader.Close();
			return uzd;
		}
		public List<Uzdevums> findExerciseByID(int[] id)
		{
			List<Uzdevums> uzdevumi = new List<Uzdevums>();
			foreach(int i in id)
			{
				uzdevumi.Add(findExerciseByID(i));
			}

			return uzdevumi;

		}


		// Topic

		public List<String> searchTopics(int grade)
		{
			List<String> topics = new List<String>();

			MySqlCommand query = connection.CreateCommand();
			query.CommandText = "SELECT * FROM `uzdevumu_table` WHERE" +
				" KLASE = @grade";



			return topics;
		}

	}
}

