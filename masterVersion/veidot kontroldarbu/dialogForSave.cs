﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class dialogForSave : Form
    {
        string testName;
        public dialogForSave()
        {
            InitializeComponent();
            cancelButton.DialogResult = DialogResult.Cancel;
            okButton.DialogResult = DialogResult.OK;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //cancel
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //ok
            if (textBox1.Text == null)
            {
                MessageBox.Show("Ievadi kontroldarba nosaukumu");
            }
            else
            {
                testName = textBox1.Text;
            }
        }
        public string getTestName()
        {
            return testName;
        }

        private void dialogForSave_Load(object sender, EventArgs e)
        {
            cancelButton.DialogResult = DialogResult.Cancel;
            okButton.DialogResult = DialogResult.OK;
        }
    }
}
