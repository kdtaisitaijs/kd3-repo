﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion
{
    public static class TablesAndConstants
    {
        public static Dictionary<String, int> BlivumaTabula = new Dictionary<string, int>();
        public static Dictionary<String, float> FizikasConst = new Dictionary<string, float>();
        public static String[] greekLetters = { "α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ", "λ", "μ", "ν", "ξ", "ο", "π", "ρ", "σ", "τ", "υ", "φ", "χ", "ψ", "ω" };
        public static String[] functions = { "sin", "cos", "acos", "asin", "tan", "atan", "sqrt", "abs", "exp", "log", "log10", "log2" };
        public static Dictionary<int, Dictionary<string, string[]>> subTemas = new Dictionary<int, Dictionary<string, string[]>>();
        public static void InitializeTables()
        {
            FillBlivumaTabula();
        }
        public enum tableTypes { blivums, konstantes, grieku,funkcijas };
        private static void FillBlivumaTabula()
        {
            
            string readText = File.ReadAllText("Data//blivums.json");
            BlivumaTabula = JsonConvert.DeserializeObject<Dictionary<String, int>>(readText);
            readText = File.ReadAllText("Data//konstantes.json");
            FizikasConst = JsonConvert.DeserializeObject<Dictionary<String, float>>(readText);           
            /*int mar;
            foreach (String key in BlivumaTabula.Keys)
            {
                BlivumaTabula.TryGetValue(key, out mar);
                Console.WriteLine("Nosaukums: " + key + " Blivums: " + mar.ToString());
            }
            BlivumaTabula.TryGetValue("Marmors", out mar);*/

        }
    }
}
