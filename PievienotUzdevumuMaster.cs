﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace masterVersion
{
    public partial class PievienotUzdevumuMaster : Form
    {
        SubMenuTestGen mainForm;
        ShuntingYardSimpleMath SY;
        Random rnd;
        String answer;
        float taskTime = 5;
        int difficulty = -1;
        int formulaCount = 0;
        int klase = 10;
        string tema = "";
        bool multipleChoice = false;
        bool theoretical = false;
        TextBox currentTextBox;
        KlasuTemas klTemas;
        RichTextBox currentRichTextBox;
        List<object[]> formulaArray;
        List<object[]> numberArray;
        List<string> formulas;
        List<object[]> orderArray;
        List<String> solutions;
        List<TextBox> unitBoxes;
        List<Button> unitButtons;
        List<Button> diffButtons;
        List<Stack<char>> operatorStack;
        String[] multipleChoiceOptions;
        Dictionary<String, String> unitDict;
        Dictionary<String, float> TextConstDict;
        Uzdevums Uzd;
        TaskSolve taskSolve;
        public PievienotUzdevumuMaster()
        {
            klTemas = new KlasuTemas();
            taskSolve = new TaskSolve();            
            InitializeComponent();
            richTextBox2.LostFocus += RichTextBox2_LostFocus;
            diffButtons = new List<Button>();
            diffButtons.Add(diffBtn1);
            diffButtons.Add(diffBtn2);
            diffButtons.Add(diffBtn3);
            diffButtons.Add(diffBtn4);
            diffButtons.Add(diffBtn5);
            Clear();            
            SY = new ShuntingYardSimpleMath();
            rnd = new Random();
            flowLayoutPanel3.HorizontalScroll.Enabled = false;
            flowLayoutPanel3.HorizontalScroll.Visible = false;
             
            string readText = File.ReadAllText("..\\..\\units.txt");
            unitDict = JsonConvert.DeserializeObject<Dictionary<String, String>>(readText);
            TextConstDict = new Dictionary<string, float>();
            FillTextConstDict();
            
        }

        private void RichTextBox2_LostFocus(object sender, EventArgs e)
        {
            try
            {
                List<String> words = SplitFormula(richTextBox2.Text.Split('=')[1]);
                for (int i = 0; i < words.Count; i++)
                {
                    if (TextConstDict.ContainsKey(words[i]))
                    {
                        words[i] = TextConstDict[words[i]].ToString();
                    }
                }
                richTextBox2.Text = richTextBox2.Text.Split('=')[0]+"="+String.Join("", words.ToArray());
            }
            catch { }

        }

        private void Clear()
        {            
            Uzd = new Uzdevums();
            label3.Text = "";            
            label12.Text = "";
            multipleChoiceOptions = new String[4];
            currentRichTextBox = richTextBox2;
            setDiffIcons(1);            
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
            formulas = new List<string>();
            solutions = new List<string>();
            operatorStack = new List<Stack<char>>();
            formulaArray = new List<object[]>();
            numberArray = new List<object[]>();
            orderArray = new List<object[]>();            
            unitButtons = new List<Button>();
            unitBoxes = new List<TextBox>();
            //typeButton.Text = "Mainīt tipu - brīva atblide";
            klase = 10;
            SetThemes(klase);
            tema = "";
            panel1.Visible = false;
            richTextBox1.Width = 631;
            richTextBox1.Text = "";
            richTextBox2.Text = "";
            richTextBox3.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox4.Text = "";
            label3.Text = "";
            taskTime = 5;
            difficulty = 1;
            comboBox1.Text = "5";
            multipleChAns.Text = "";
            theoretical = false;
            multipleChoice = false;
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();
            flowLayoutPanel3.Controls.Clear();
            formulaPanel.Controls.Clear();
            formulaCount = 0;

        }
        private void FillTextConstDict()
        {
            TextConstDict["π"] = 3.1415f;
            TextConstDict["g"] = 9.81f;
            
        }
      
        private void BTTN_backToMain_Click(object sender, EventArgs e)
        {
            if (panel6.Visible)
            {
                mainForm.Show();
                this.Hide();
            }
            else
            {
                typeButton.Visible = true;
                panel6.Visible = true;
                panel5.Visible = false;
            }
        }

        private void AddVar(object sender, EventArgs e) {
            var btn = sender as Button;
            string unit = "";            
            var index = unitButtons.IndexOf(btn);
            unit = unitBoxes.ElementAt(index).Text;
            CheckBox ch = flowLayoutPanel3.Controls[index] as CheckBox;
            string Symbol = "$";
            if (ch.Checked)
            {
                unit = "#" + unit;
            }
            richTextBox1.SelectedText += Symbol + btn.Text + " " + unit;
            richTextBox1.Focus();
            //richTextBox1.SelectionStart = richTextBox1.Text.Length;
            //richTextBox1.SelectionLength = 0;
        }
        private void PassFocus(int cursorPos)
        {
            if (currentTextBox != null)
            {
                currentTextBox.Focus();                
                currentTextBox.SelectionStart = cursorPos;
                currentTextBox.SelectionLength = 0;

            }
            else
            {
                if (currentRichTextBox != null)
                {
                    currentRichTextBox.Focus();
                    currentRichTextBox.SelectionStart = cursorPos;
                    currentRichTextBox.SelectionLength = 0;
                }
            }
        }
        private void ShowMessage(string Text)
        {
            try
            {
                MessageBoxManager.Register();
            }
            catch
            {

            }
            MessageBoxManager.OK = "Labi";
            MessageBox.Show(Text, "Pievienot uzdevumu");
            MessageBoxManager.Unregister();
        }
        private void AddFormula(object sender, EventArgs e)
        {
            
            
                     
            String formula =richTextBox2.Text;
            if (formula == "")
            {
                ShowMessage("Ievadiet formulas tekstu pirms pievienošanas");
                return;
            }
            if (!formula.Contains("="))
            {
                ShowMessage("formulai jāsatur vienādības zīme (=) ");
                return;
            }

                
                      
            List<String> formulaParts = formula.Split('=').ToList<String>();                       
            List<String> forMath = SplitFormula(formulaParts[1]);            
            List<String> formulaSplit = new List<string>();
            foreach (string st in forMath)
            {
                if (Regex.IsMatch(st, @"^[a-zA-Z\p{IsGreekandCoptic}]+$"))
                    formulaSplit.Add(st);
                else if (Regex.IsMatch(st, @"^([A-Za-z]+[0-9]+|[0-9]+[A-Za-z]+)[A-Za-z0-9]*$"))
                    formulaSplit.Add(st);
            }           
            

            
                      

            Tuple<Stack<object>,Stack<char>> formulaStacks= SY.GetStacks(forMath, null);
            //Test solve
            Uzdevums testUzd = new Uzdevums();
            String[] oper = new String[1];          
            char[] charArr = formulaStacks.Item2.ToArray();
            oper[0] = string.Join(",", charArr);
            testUzd.setOperatorStacks(oper);
            String[] vars = new String[1];
            object[] varArr = formulaStacks.Item1.ToArray();
            vars[0] = "TestAns:"+string.Join(",", varArr);
            testUzd.setvarStacks(vars);
            testUzd.setText("$"+string.Join(" $", varArr));
            /*int i = 0;
            foreach(char ch in formulaStacks.Item2)
            {
                oper[i] = ch.ToString();
                i++;
            }
            testUzd.setOperatorStacks(oper);
            i = 0;
            String[] vars = new String[formulaStacks.Item1.Count+1];
            vars[i] = ":testAns";
            foreach (object vr in formulaStacks.Item1)
            {
                i++;
                vars[i] = vr.ToString();
                
            }
            
            testUzd.setvarStacks(vars);*/
            var ans =taskSolve.computeFormula(testUzd);

            //! Test solve
            if(ans.Item1.Contains("Neizdevās izpildīt uzdevumu. Visticamāk nav doti pietiekoši daudz mainīgie."))
            {
                ShowMessage("Formulu neizdevās validēt. Lūdzu pārbaudiet to un meiģiniet velreiz");
                return;
            }

            foreach (string variable in formulaSplit)
            {
                Button varButton = new Button();
                TextBox unitTextBox = new TextBox();
                try
                {
                    unitTextBox.Text = unitDict[variable];
                }
                catch
                {

                }
                unitTextBox.Width = 70;
                unitTextBox.Height = 20;
                unitBoxes.Add(unitTextBox);
                varButton.Text = variable.ToString();
                varButton.Click += new EventHandler(AddVar);
                varButton.Width = 50;
                varButton.Height = 20;
                unitButtons.Add(varButton);
                flowLayoutPanel1.Controls.Add(varButton);
                flowLayoutPanel2.Controls.Add(unitTextBox);
                CheckBox isSI = new CheckBox();
                flowLayoutPanel3.Controls.Add(isSI);
            }

            if (!theoretical)
                panel3.Visible = true;
            solutions.Add(formulaParts[0]);
            formulas.Add(richTextBox2.Text);
            richTextBox2.Text = "";
            Label formulaText = new Label();
            formulaText.Text = formula;
            formulaPanel.Controls.Add(formulaText);



            formulaArray.Add(formulaStacks.Item1.ToArray());
            numberArray.Add(formulaStacks.Item1.ToArray());
            operatorStack.Add(formulaStacks.Item2);
            formulaCount++;
        }

     
        //Parsing of entered formula text
        private List<String> SplitFormula(String toSplit)
        {
            String currentString = "";
            int type = 0;
            
            List<String> toReturn = new List<String>();
            foreach (char item in toSplit)
            {                
                if (Char.IsDigit(item)|| item =='.')
                {
                    if (1.Equals(type) || 2.Equals(type))
                        currentString += item;
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 1;
                }
                else if (Char.IsLetter(item))
                {
                    if (2.Equals(type) || 1.Equals(type))
                    {
                        currentString += item;                        
                    }
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 2;
                }
                else if (Char.Equals(item, '(') || Char.Equals(item, ')'))
                {
                    if (3.Equals(type))
                        currentString += item;
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 3;
                }
                else
                {
                    if (Char.IsPunctuation(item) || Char.IsSymbol(item))
                    {
                        if (4.Equals(type))
                            currentString += item;
                        else
                        {
                            toReturn.Add(currentString);
                            currentString = item.ToString();
                        }
                        type = 4;
                    }
                }
            }
            toReturn.Add(currentString);
            toReturn.RemoveAll(string.IsNullOrWhiteSpace);
            return toReturn;
        }

        //Checks if all variables are set for any equation
        private void computeFormula(object sender, EventArgs e)
        {
            SetupUzdSolve();
            Tuple<string,string> Result = taskSolve.computeFormula(Uzd);
            String taskText = Result.Item1;
            String answer = Result.Item2;
            if (theoretical)
            {
                if (multipleChoice)
                {
                    multipleChAns.Text = answer;
                }
            }
            else
            {

                label3.Text = taskText;
                label12.Text = "Atblide: " + answer;
                multipleChAns.Text = answer.ToString();
                //Tekstu aizpilde atbilžu variantiem
                float result = float.Parse(answer);
                if(textBox1.Text =="")
                    textBox1.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
                if (textBox2.Text == "")
                    textBox2.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
                if (textBox4.Text == "")
                    textBox4.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
            }
        }

        private void diffBtnClick(object sender, EventArgs e)
        {
            difficulty = diffButtons.IndexOf(sender as Button);            
        }

        private void diffBtnHover(object sender, EventArgs e)
        {
            //(sender as Button).ImageIndex = 1;
            int index = diffButtons.IndexOf(sender as Button);
            setDiffIcons(index);
        }
        private void setDiffIcons(int index)
        {
            for (int i = 0; i < 5; i++)
            {
                if (i <= index)
                    diffButtons[i].ImageIndex = 1;
                else
                    diffButtons[i].ImageIndex = 0;

            }
        }

        private void diffBtnLeave(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                if (i <= difficulty)
                    diffButtons[i].ImageIndex = 1;
                else
                    diffButtons[i].ImageIndex = 0;

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            taskTime = float.Parse((sender as ComboBox).Text);
        }

        private void SetupUzdOther()
        {
            if (difficulty == -1)
                difficulty = 3;
            Uzd.setDifficulty(difficulty);
            Uzd.setGrade(klase);
            Uzd.setTopic(tema);
            String[] tags = richTextBox4.Text.Split(',');
            int i = 0;
            foreach(string tag in tags)
            {
               tags[i] = tag.Trim();
               i++;
            }              
            Uzd.setEquation(formulas.ToArray());
            if (multipleChoice)
            {
                String[] MultAns = new String[4];
                MultAns[0] = multipleChAns.Text;
                MultAns[1] = textBox1.Text;
                MultAns[2] = textBox2.Text;
                MultAns[3] = textBox4.Text;
                Uzd.setMultChoices(MultAns);
            }

            
        }

        private void SetupUzdSolve()
        {
            Uzd.setText(richTextBox1.Text);
            Uzd.setEquation(formulas.ToArray());


            List<String> tempList = new List<string>();
            foreach (Stack<char> op in operatorStack)
            {

                char[] charArr = op.ToArray();
                String opString = string.Join(",", charArr);
                tempList.Add(opString);

            }
            Uzd.setOperatorStacks(tempList.ToArray());

            List<String> tempList2 = new List<string>();
            int i = 0;
            foreach (object[] fl in formulaArray)
            {
                object[] charArr = fl.ToArray();
                String opString = string.Join(",", charArr);
                opString = solutions[i] + ":" + opString;
                i++;
                tempList2.Add(opString);
            }
            Uzd.setvarStacks(tempList2.ToArray());
            Uzd.setIsMultChoice(multipleChoice);
            Uzd.setIsTheory(theoretical);
            if (theoretical)
            {
                Uzd.setAnswer(answer);
            }
        }

        private void AddToDatabase(object sender, EventArgs e)
        {
            MessageBoxManager.Yes = "Jā";
            MessageBoxManager.No = "Nē";
            MessageBoxManager.OK = "Labi";
            MessageBoxManager.Register();
            DialogResult result1 = MessageBox.Show("Vai tiešām vēlaties pievienot uzdevumu datubāzei?",                
            "Pievienot uzdevumu", MessageBoxButtons.YesNo);
            if(richTextBox1.Text == "")
            {
                ShowMessage("Ievadiet uzdevuma tekstu pirms tā pievienošanas datubāzei");
                MessageBoxManager.Unregister();
                return;
            }
            if (tema == "")
            {
                ShowMessage("Ievadiet uzdevuma tēmu pirms pievienošans");                
                return;
            }
            if (theoretical)
            {
                if(answer == "")
                {
                    ShowMessage("Ievadiet teorētiskā uzdevuma atblidi pirms tā pievienošanas datubāzei");                
                    return;
                }
                if(multipleChoice && (textBox1.Text =="" || textBox2.Text == "" || textBox4.Text == ""))
                {
                    ShowMessage("Ievadiet uzdevuma atbilžu variantus pirms tā pievienošanas datubāzei");                    
                    return;
                }

            }else
            {
                if (formulaCount < 1)
                {
                    ShowMessage("Uzdevumam ar aprēķinu nepieciešama vismaz viena formula. Lai ievadītu uzdevumu bez formulām nomainat tā tipu uz \"teorētisks\" ");                    
                    return;
                }
            }

            if (result1 == DialogResult.Yes)
            {
                DatabaseAccess db = new DatabaseAccess();
                SetupUzdOther();
                SetupUzdSolve();
                bool isConnected = db.Connect();
                if (isConnected)
                {
                    db.insertExercise(Uzd);
                    List<Uzdevums> aaa = db.findExerciseByGrade(11);
                    db.closeConncetion();
                    Clear();
                    MessageBox.Show("Uzdevums pievienots datubāzei", "Pievienot uzdevumu");
                }else
                {
                    MessageBox.Show("Neizdevās pievienot uzdevumu datubāzei", "Pievienot uzdevumu");
                }
            }
            MessageBoxManager.Unregister();
        }

        private void typeButton_Click(object sender, EventArgs e)
        {
            panel6.Visible = false;
            panel5.Visible = true;
            typeButton.Visible = false;
            currentTextBox = null;
            currentRichTextBox = richTextBox2;
            
        }

        private void theoreticalCheck_CheckedChanged(object sender, EventArgs e)
        {
            theoretical = !theoretical;
            panel2.Visible = theoretical;
            panel3.Visible = !theoretical;
        }

        private void richTextBox3_TextChanged_1(object sender, EventArgs e)
        {
            multipleChAns.Text = richTextBox3.Text;
            answer = richTextBox3.Text;
        }      

        private void button3_Click(object sender, EventArgs e)
        {
            panel4.Visible = !panel4.Visible;
        }
        
        private void button25_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (currentTextBox != null)
            {
                currentTextBox.SelectedText = btn.Text;
                PassFocus(currentTextBox.SelectionStart);
                return;
            }
            if (currentRichTextBox != null)
            {
                
                currentRichTextBox.SelectedText = btn.Text;
                PassFocus(currentRichTextBox.SelectionStart);
            }
            

        }

        private void multipleChAns_Click(object sender, EventArgs e)
        {
            
           currentTextBox = sender as TextBox;
            
            if(currentTextBox== null)
            {
                currentRichTextBox = sender as RichTextBox;
                currentTextBox = null;
            }
        }

        private void PievienotUzdevumuMaster_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.ShiftKey && panel4.Visible)
            {
               foreach( Button btn in panel4.Controls)
                {
                    btn.Text = btn.Text.ToUpper();
                }
            }
        }

        private void PievienotUzdevumuMaster_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey && panel4.Visible)
            {
                foreach (Button btn in panel4.Controls)
                {
                    btn.Text = btn.Text.ToLower();
                }
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {           
            label3.Text = taskSolve.generatePlainText(richTextBox1.Text,true);            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (multipleChoice)
            {
                //typeButton.Text = "Mainīt tipu - brīva atblide";
                panel1.Visible = false;
                richTextBox1.Width = 631;
            }
            else
            {
                richTextBox1.Width = 335;
                panel1.Visible = true;
               //typeButton.Text = "Mainīt tipu - atbližu varianti";
            }
            multipleChoice = !multipleChoice;
        }
        private void SetThemes(int klase)
        {
            comboBox2.Items.Clear();

            //TODO error fix

            String[] texts = klTemas.getTemas(klase).ToArray();
            System.Object[] ItemObject = new System.Object[texts.Length];
            for (int i = 0; i < texts.Length; i++)
            {
                ItemObject[i] = texts[i];
            }
            comboBox2.Items.AddRange(ItemObject);
            tema = "";
            
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            klase = int.Parse(comboBox3.Text);
            SetThemes(klase);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            tema = comboBox2.Text;
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
