﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;
using System.Threading;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class SubMenuTestGen : MetroFramework.Forms.MetroForm
    {
        bool variantuSkats = false;
        infoPanel infoPanel = new infoPanel();
        int variantuSkaits;
        main mainForm;
        int klase;
        Size pageSize;
        string tema;
        float scale;
        string subject;
        public List<page> lapasObjekti = new List<page>();
        List<Panel> lapasPaneli = new List<Panel>();
        public page currentPage;
        int currentVariants = 0;
        new Point pagePoint;
        // Jāzepa kods
        List<List<Uzdevums>> tasks = new List<List<Uzdevums>>();
        List<List<WhiteSpace>> whiteSpaceTypes = new List<List<WhiteSpace>>();
        List<Paragraph> TaskTexts;
        float minDistBetweenPages = 20;
        //bija 40
        //Jāzepa kods beigas
        Panel uzdevumuPanel;
        CreatetPdf pdfGenerator = new CreatetPdf();
        ImportExport exporter = new ImportExport();
        System.Windows.Forms.Timer timer;

        public SubMenuTestGen(main _main, string _subject, int _klase, string _tema)
        {
            // this.Size = new Size(2000,900);

            InitializeComponent();
            Size = new Size(_main.Width - 250, _main.Height);
            Size = this.Size;
            pageSize = new Size(Size.Width, Size.Height - 65);
            klase = _klase;
            tema = _tema;
            subject = _subject;
            flowLayoutPanel1.Width = Size.Width - 100;
            //Jazeps            
            TaskTexts = new List<Paragraph>();
            // !Jazeps

            mainForm = _main;
            button5_Click(null, null);
            AddVariantsDelay();
        }

        //Jāzepa kods
        public enum WhiteSpace { none, grid, lines, blank };
        private void AddVariantsDelay()
        {
            timer = new System.Windows.Forms.Timer();
            timer.Tick += delegate (object senderr, EventArgs ee)
            { AddVariants(senderr, ee); };
            timer.Interval = 10;
            timer.Start();
        }

        public void AddVariants(object sender, EventArgs e)
        {

            if (variantuSkaits < 3 && !variantuSkats)
            {
                page Lapa = new page();
                lapasObjekti.Add(Lapa);

                //Panel newVariants = Lapa.createPage(this.ClientSize.Width, this.ClientSize.Height, tema, klase ,variantuSkaits++, 1f, flowLayoutPanel1);
                Panel newVariants = Lapa.createPage(pageSize.Width, pageSize.Height, tema, klase, variantuSkaits++, 1f, flowLayoutPanel1);
                lapasPaneli.Add(newVariants);

                Button variantButton = new Button();
                variantButton.Size = button6.Size;
                variantButton.Name = (variantuSkaits - 1).ToString();
                variantButton.Text = "Variants " + variantuSkaits.ToString();
                variantButton.FlatStyle = FlatStyle.Flat;
                variantButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
                variantButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
                variantButton.Click += VariantButton_Click;
                variantButton.MouseEnter += button8_MouseEnter;
                variantButton.MouseLeave += button8_MouseLeave;
                flowLayoutPanel1.Controls.Add(variantButton);
                Controls.Add(newVariants);
                if (timer.Enabled)
                {
                    timer.Stop();
                    currentPage = lapasObjekti[0];
                    pagePoint = newVariants.Location;
                }
                /*if (variantuSkats)
                {
                    newVariants.Scale(new SizeF(0.63f, 1f));
                    newVariants.Location = new Point((variantuSkaits - 1) * 515, newVariants.Location.Y);

                    if (variantuSkaits == 3)
                    {
                        for (int i = 0; i < lapasPaneli.Count; i++)
                        {
                            lapasPaneli[i].Location = new Point(i * 515, lapasPaneli[i].Location.Y);
                        }
                    }
                }  */
            }
        }

        private void VariantButton_Click(object sender, EventArgs e)
        {
            SuspendLayout();
            Button btn = sender as Button;
            currentVariants = int.Parse(btn.Name);
            uzdevumuPanel.Visible = true;
            for (int i = 0; i < lapasPaneli.Count; i++)
            {
                if (variantuSkats)
                {
                    if (scale < 1.0f)
                    {
                        lapasPaneli[i].Scale(new SizeF(1 / scale, 1f));
                    }
                    lapasPaneli[i].Location = pagePoint;

                }
                if (i.ToString() == btn.Name)
                {
                    lapasPaneli[i].Visible = true;
                    currentPage = lapasObjekti[i];
                }
                else
                {
                    lapasPaneli[i].Visible = false;
                }
            }
            variantuSkats = false;
            ResumeLayout();
        }

        private void showVariants(object sender, EventArgs e)
        {
            if (!variantuSkats && variantuSkaits > 1)
            {
                uzdevumuPanel.Visible = false;
                SuspendLayout();
                //Aprekins lapu izmeram varianta skataa
                float pageWidth = lapasPaneli[0].Width;
                float allowedW = (Size.Width - (1 + variantuSkaits) * minDistBetweenPages) / variantuSkaits;
                float ScalingFactor = allowedW / pageWidth;
                float padding = minDistBetweenPages;
                scale = ScalingFactor;
                //Aprekins distancei starp lapam
                if (ScalingFactor > 1)
                {
                    padding = (Size.Width - pageWidth * variantuSkaits) / (variantuSkaits + 1);
                }

                for (int i = 0; i < lapasPaneli.Count; i++)
                {
                    lapasPaneli[i].Visible = true;
                    if (ScalingFactor < 1.0f)
                    {
                        lapasPaneli[i].Scale(new SizeF(ScalingFactor, 1f));
                    }
                    lapasPaneli[i].Location = new Point((int)(padding + i * (lapasPaneli[i].Width + padding)), lapasPaneli[i].Location.Y);
                    /*if (variantuSkaits == 2)
                        lapasPaneli[i].Location = new Point(i * 515 + 267, lapasPaneli[i].Location.Y);
                    if (variantuSkaits == 3)
                        lapasPaneli[i].Location = new Point(i * 515, lapasPaneli[i].Location.Y);*/
                }
                variantuSkats = true;
                ResumeLayout();
            }
        }
        //Jāzepa Kods beidzas

        private void button5_Click(object sender, EventArgs e)
        {
            uzdevumuPanel = infoPanel.createTaskPanel(klase, 0, flowLayoutPanel1.Height + 5, this.ClientSize, this, tema);
            Controls.Add(uzdevumuPanel);
        }

        private void SubMenuTestGen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        //Jazeps kods
        private void CreatePdf(object sender, EventArgs e)
        {
            MessageBoxManager.Yes = "Šo variantu";
            MessageBoxManager.No = "Visus variantus";
            MessageBoxManager.Cancel = "Atcelt";
            MessageBoxManager.Register();
            DialogResult result1 = MessageBox.Show("Vai Jūs vēlaties eksportēt aktīvo variantu vai visus variantus?",
            "Kontroldarbu eksports", MessageBoxButtons.YesNoCancel);
            MessageBoxManager.Unregister();
            //try
            {
                if (result1 == DialogResult.Yes)
                {
                    //viens variants


                    //Todo allow to set varianti
                    pdfGenerator.Export(lapasObjekti[currentVariants].TestaVeids, lapasObjekti[currentVariants].Tema, currentVariants,
                        lapasObjekti[currentVariants].TasksInTest, lapasObjekti[currentVariants].puntkiUzdevumos, "3", lapasObjekti[currentVariants].whiteSpace, true);
                    MessageBox.Show("Kontroldarbi veiksmīgi eksportēti", "Kontroldarbu Eksports", MessageBoxButtons.OK);
                }
                if (result1 == DialogResult.No)
                {
                    String TestName = "";
                    String folderName = lapasObjekti[0].TestaVeids + " " + lapasObjekti[0].Tema;
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.FileName = folderName;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        TestName = saveFileDialog1.FileName;
                        //visi varianti
                        for (int i = 0; i < variantuSkaits; i++)
                        {
                            whiteSpaceTypes.Clear();
                            List<WhiteSpace> whiteSpaceList = new List<WhiteSpace>();
                            for (int j = 0; j < lapasObjekti[i].TasksInTest.Count; j++)
                            {
                                whiteSpaceList.Add(WhiteSpace.blank);
                            }
                            whiteSpaceTypes.Add(whiteSpaceList);

                            //Todo allow to set varianti
                            pdfGenerator.Export(lapasObjekti[i].TestaVeids, lapasObjekti[i].Tema, i,
                                lapasObjekti[i].TasksInTest, lapasObjekti[i].puntkiUzdevumos, "3", lapasObjekti[i].whiteSpace, false, TestName);
                        }
                        MessageBox.Show("Kontroldarbi veiksmīgi eksportēti", "Kontroldarbu Eksports", MessageBoxButtons.OK);
                    }

                }

            }
            /*catch (Exception ex)
            {
                MessageBox.Show("Kontroldarbu eksports neizdevās.\n" + ex.Message, "Kontroldarbu Eksports", MessageBoxButtons.OK);
            }*/
        }
        //Jazepa kods beidzas

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void exportTest(object sender, EventArgs e)
        {
            MessageBox.Show("Šobrīd aktīvi stādājam pie šī rīka! Drīzumā būs pieejams!");
            /*
            MessageBoxManager.Yes = "Šo variantu";
            MessageBoxManager.No = "Visus variantus";
            MessageBoxManager.Cancel = "Atcelt";
            MessageBoxManager.Register();
            DialogResult result1 = MessageBox.Show("Vai Jūs vēlaties eksportēt aktīvo variantu vai visus variantus?",
            "Kontroldarbu eksports", MessageBoxButtons.YesNoCancel);
            List<List<Tuple<WhiteSpace, int>>> tempWhiteSpace = new List<List<Tuple<WhiteSpace, int>>>();
            List<Dictionary<int, int>> tempPoints = new List<Dictionary<int, int>>();
            if (result1 == DialogResult.Yes)
            {
                //Eksportēt vienu variantu                
                tasks.Clear();


                tempWhiteSpace.Add(lapasObjekti[currentVariants].whiteSpace);
                tempPoints.Add(lapasObjekti[currentVariants].puntkiUzdevumos);
                tasks.Add(currentPage.TasksInTest);
                KdData forExport = new KdData(tasks, tempPoints, 1, currentPage.TestaVeids, currentPage.Tema, klase, subject, tempWhiteSpace);
                exporter.sendDataForExport(forExport);
            }
            if (result1 == DialogResult.No)
            {
                //Eksportēt vairākus variantus                
                tasks.Clear();
                foreach (page lapa in lapasObjekti)
                {
                    tempWhiteSpace.Add(lapa.whiteSpace);
                    tempPoints.Add(lapa.puntkiUzdevumos);
                    tasks.Add(lapa.TasksInTest);
                }
                KdData forExport = new KdData(tasks, tempPoints, lapasObjekti.Count, currentPage.TestaVeids, currentPage.Tema, klase, subject, tempWhiteSpace);
                exporter.sendDataForExport(forExport);

            }
            MessageBoxManager.Unregister();
            */
        }
        
        private void SubMenuTestGen_Load(object sender, EventArgs e)
        {

        }

        private void button8_MouseEnter(object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.ForeColor = Color.FromArgb(91, 178, 204);
        }

        private void button8_MouseLeave(object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.ForeColor = Color.Black;
        }
    }
}
