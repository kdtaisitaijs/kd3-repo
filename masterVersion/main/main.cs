﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class main : MetroFramework.Forms.MetroForm
    {
        ImportExport import = new ImportExport();
        SubMenuTestGen subMenu;
        KlasuTemas klasuTemas = new KlasuTemas();
        PievienotUzdevumuMaster addTask;

        DialogResult result;
        info info = new info();
        List<int> klases = new List<int>();
        List<string> temas = new List<string>();
        int klase;
        string tema;
        string subject;
        string errorMsg;
        string Error;
        KdData selectedKD = null;
        ToDoDialog toAdd;
        enum IzveletaisTips
        {
            generetKD,
            veidotKD,
            importetKD,
            maniKD,
            PievienotUzd
        }
        IzveletaisTips CurrentSelected;
        public main()
        {
            /* FTP Test
             * FtpConnect ftp = new FtpConnect();
             ftp.uploadFtp("C:\\Users\\User\\Desktop\\testGen\\logo.png");
            try
             {
                 ftp myFtpClient = new ftp("ftp://ec2-34-251-149-202.eu-west-1.compute.amazonaws.com", "test", "test");
                 //myFtpClient.upload( "bildes/test3.jpg", "C:\\Users\\User\\Desktop\\timeline.png");
                 myFtpClient.download("bildes/test3.jpg", "C:\\Games\\image.jpg");
                 //myFtpClient.createDirectory("bildes");
                 //string[] simpleDirectoryListing = myFtpClient.directoryListSimple("bildes");               
                 //int o = 0;
                 //myFtpClient.download("512KB.zip", "C:\\Users\\User\\Downloads\\thingy.zip");                


                 //myFtpClient.upload("/upload/test.zip", "C:\\Users\\User\\Downloads\\512KB.zip");

             }
             catch(Exception e)
             {
                 string msg = e.Message;
             }*/
            TablesAndConstants.InitializeTables();
            CurrentSelected = IzveletaisTips.generetKD;
            InitializeComponent();
            Random rnd = new Random();
            /*float num = 0;
            for (int i = 0; i < 100000; i++)
            {
                num += Math.Abs(rnd.Next(21) - rnd.Next(21));
            }
            num = num / 100000;*/
            //webBrowser1.Navigate("http://brightmedia.pl/?lang=en&site=creation");
            //webBrowser1.Navigate("http://satchmi.com/vinylday/");
            //webBrowser1.Scale(new SizeF(0.5f, 0.5f));
            ComboBox_klase.Enabled = false;
            ComboBox_tema.Enabled = false;

            for (int i = 0; i < info.getAllSubjects().Count; i++)
            {
                ComboBox_subject.Items.Add(info.getAllSubjects().ElementAt(i));
            }
            BTTN_OpenGenTest.Focus();
            TogglePanels();

            /*DatabaseAccess db = new DatabaseAccess();            
            bool isConnected = db.Connect();
            string[] eq = new string[1];
            eq[0] = "f=m*a";
            string[] op = new string[1];
            op[0] = "*";
            string[] var = new string[1];
            var[0] = "f:a,m";
            string[] choices = new string[4];

            if (isConnected)
            {
                for (int i = 0; i < 25; i++)
                {
                    Uzdevums Uzd = new Uzdevums(2000+i,9, "Optika", "Jauns Aprekinu uzdevums nr " + i.ToString(), 7, eq, op, var, false, "42", false, choices);
                    db.insertExercise(Uzd);
                }            }*/
        }
        private bool validate()
        {
            if (ComboBox_klase.SelectedItem == null)
            {
                result = MessageBox.Show("Nav ievadīta klase, tiks atlasītas visas", "Ziņojums", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {

                    return true;
                }
                else
                {
                    return false;
                }

            }
            else if (ComboBox_subject.SelectedItem == null)
            {
                return true;
            }
            else
            {
                return true;
            }

        }
        private void ComboBox_subject_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox_klase.Enabled = true;
            ComboBox_tema.Enabled = true;

            subject = ComboBox_subject.SelectedItem.ToString();


            ComboBox_klase.Items.Clear();
            ComboBox_tema.Items.Clear();

            klases = klasuTemas.prieksmetaKlases(subject);
            temas = klasuTemas.getTemas(0);

            for (int i = 0; i < klases.Count; i++)
            {
                ComboBox_klase.Items.Add(klases.ElementAt(i)).ToString();
            }

            for (int k = 0; k < temas.Count; k++)
            {
                ComboBox_tema.Items.Add(temas.ElementAt(k).ToString());
            }

            ComboBox_tema.Text = "Visas tēmas";
            ComboBox_klase.Text = "8";

        }

        private void BTTN_OpenTestGen_Click()
        {
            if (this.validate() == true)
            {
                if (int.TryParse(ComboBox_klase.SelectedItem.ToString(), out klase) == true)
                {
                    klase = int.Parse(ComboBox_klase.SelectedItem.ToString());
                }
                else
                {
                    klase = 0;
                }
                
                subMenu = new SubMenuTestGen(this, ComboBox_subject.SelectedItem.ToString(), klase, tema);
                subMenu.Show();
                this.Hide();

            }
        }

        private void BTTN_CloseMain_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void BTTN_ImportTetst_Click()
        {
            import.importTest(this);
        }

        private void button1_Click()
        {
            subMenu = new SubMenuTestGen(this, ComboBox_subject.SelectedItem.ToString(), int.Parse(ComboBox_klase.SelectedItem.ToString()), ComboBox_tema.SelectedItem.ToString());
            subMenu.Show();
            this.Hide();
        }

        private void main_Load(object sender, EventArgs e)
        {
            //ToDo no ņemt
            ComboBox_subject.Text = "Fizika";
            ComboBox_klase.Text = "8";
            ComboBox_tema.Text = "Test";
        }

        private void BTTN_OpenTestGen_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Ģenērēt kontroldarbu ievadot parametrus", BTTN_OpenTestGen);
        }

        private void BTTN_OpenGenTest_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Ģenērēt kontroldarbu ievadot parametrus", BTTN_OpenGenTest);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox_tema.Items.Clear();
            if (int.TryParse(ComboBox_klase.SelectedItem.ToString(), out klase) == true)
            {
                temas = klasuTemas.getTemas(klase);
                try
                {
                    for (int i = 0; i < temas.Count; i++)
                    {
                        ComboBox_tema.Items.Add(temas.ElementAt(i));
                    }
                }
                catch
                {

                }
            }
            else
            {
                for (int i = 0; i < klases.Count; i++)
                {
                    ComboBox_klase.Items.Add(klases.ElementAt(i)).ToString();
                }

            }
            ComboBox_tema.Text = "Visas tēmas";
        }

        private void SetEnum(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            CurrentSelected = (IzveletaisTips)Int32.Parse((string)btn.Tag);
            Title.Text = btn.Text.Replace("KD", "kontroldarbu");
            Title.Text = Title.Text.Replace("uzd", "uzdevumu");
            TogglePanels();
            webNav();

        }
        private void TogglePanels()
        {
            if (CurrentSelected == IzveletaisTips.importetKD || CurrentSelected == IzveletaisTips.maniKD)
            {
                panel2.Visible = false;
            }
            else
            {
                panel2.Visible = true;
            }
            /*
            if (CurrentSelected == IzveletaisTips.maniKD)
            {
                if (toAdd == null)
                {
                    toAdd = new ToDoDialog(selectedKD, this);
                    toAdd.Hide();
                }
                webBrowser1.Visible = false;
                folderPanel.Visible = true;
                button3.Visible = false;
                exportButtonPanel.Visible = true;
                testInfoPanel.Visible = true;
            }
            else
            {
                webBrowser1.Visible = true;
                folderPanel.Visible = false;
                button3.Visible = true;
                exportButtonPanel.Visible = false;
                testInfoPanel.Visible = false;
            }
            */
        }
        /*
        private void webNav()
        {
            switch (CurrentSelected)
            {
                case IzveletaisTips.generetKD:
                    webBrowser1.Navigate("http://satchmi.com/vinylday/");
                    break;
                case IzveletaisTips.veidotKD:
                    webBrowser1.Navigate("http://brightmedia.pl/?lang=en&site=creation");
                    break;
                case IzveletaisTips.importetKD:
                    webBrowser1.Navigate("http://www.bevisionare.com/");
                    break;
                case IzveletaisTips.maniKD:
                    CreateBrowser();
                    //webBrowser1.Navigate("http://gardenestudio.com.br/");                   
                    break;
                case IzveletaisTips.PievienotUzd:
                    webBrowser1.Navigate("http://forbetter.coffee/");
                    break;
                default:
                    break;
            }
        }
        
        private void CreateBrowser()
        {
            if (folderPanel.Controls.Count == 0)
            {
                String[] testList = Directory.GetFiles("mani kontroldarbi");
                foreach (string test in testList)
                {
                    Button testBtn = new Button();
                    testBtn.BackgroundImage = Properties.Resources.veidotKD;
                    testBtn.BackgroundImageLayout = ImageLayout.Stretch;
                    testBtn.Size = new Size(100, 100);
                    testBtn.Text = test.Split('\\')[1];
                    testBtn.TextImageRelation = TextImageRelation.ImageAboveText;
                    testBtn.TextAlign = ContentAlignment.BottomCenter;
                    testBtn.Tag = test;
                    testBtn.Click += TestBtn_Click;
                    folderPanel.Controls.Add(testBtn);
                }
            }
        }
        */
        /*
        private void TestBtn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            KdData info = import.importTest(this, btn.Tag.ToString());
            KdNosaukums.Text = info.KdNosaukums;
            KdTema.Text = info.Prieksmets;
            KdVariantuSk.Text = info.VariantuSk;
            KdKlase.Text = info.Klase.ToString();
            string taskCount = "";
            toAdd.setTest(info);
            foreach (List<Uzdevums> uzdSarksts in info.Uzdevumi)
            {
                taskCount += uzdSarksts.Count.ToString() + ", ";
            }
            taskCount = taskCount.Remove(taskCount.Length - 2);
            KdUzdevumuSk.Text = taskCount;
            selectedKD = info;

            int o = 0;
        }
        */
        private void ComboBox_tema_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBox_tema.Text != "Visas tēmas")
            {
                tema = ComboBox_tema.SelectedItem.ToString();
                ComboBox_klase.SelectedItem = klasuTemas.getTemasKlasi(tema);
            }
        }

        private void button2_Click()
        {
            addTask = new PievienotUzdevumuMaster(subject, klase, tema);
            addTask.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void StartProcess(object sender, EventArgs e)
        {
            if (checkSubejectClass() == null)
            {
                switch (CurrentSelected)
                {
                    case IzveletaisTips.generetKD:
                        generateKD();
                        break;
                    case IzveletaisTips.veidotKD:
                        BTTN_OpenTestGen_Click();
                        break;
                    case IzveletaisTips.importetKD:
                        BTTN_ImportTetst_Click();


                        break;
                    case IzveletaisTips.maniKD:
                        button1_Click();
                        break;
                    case IzveletaisTips.PievienotUzd:
                        button2_Click();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                MessageBox.Show(Error);
            }
        }

        private void generateKD()
        {

            GenerateKD generator = new GenerateKD(klase, ComboBox_subject.Text, tema, 2, this);


        }
        private string checkSubejectClass()
        {
            if ((ComboBox_subject.Text == "") || (ComboBox_klase.Text == "") || ComboBox_tema.Text == "")
            {
                errorMsg = "";

                if (ComboBox_subject.Text == "")
                {
                    errorMsg += "Macību priekšmets nav ievadīts/";
                }
                if (ComboBox_klase.Text == "")
                {
                    errorMsg += "Klase nav ievadīta/";
                }
                if (ComboBox_tema.Text == "")
                {
                    errorMsg += "Tēma nav ievadīta/";
                }
                return Error = makeErrorMsg(errorMsg);
            }
            else
            {
                return null;
            }
        }


        private void ComboBox_tema_DrawItem(object sender, DrawItemEventArgs e)
        {

            helpers.centerComboBox.cbxDesign_DrawItem(sender, e);

        }

        private string makeErrorMsg(string errorMsg)
        {
            Error = "";
            Char delimiter = '/';
            String[] subs = errorMsg.Split(delimiter);
            for (int i = 0; i < subs.Count(); i++)
            {
                if (subs.ElementAt(i).ToString() != "")
                {
                    Error += "\u2022" + " " + subs.ElementAt(i) + "\n";
                }
            }

            return "Darbību neizdevās veikt, sekojošu iemeslu dēļ : \n" + Error;
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (selectedKD != null)
                toAdd.doVide(null, null);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (selectedKD != null)
                toAdd.doPdf(null, null);
        }
    }
}
}
