﻿namespace masterVersion
{
    partial class dialog_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_Ok = new System.Windows.Forms.Button();
            this.but_back = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // but_Ok
            // 
            this.but_Ok.BackColor = System.Drawing.Color.Transparent;
            this.but_Ok.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.but_Ok.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.but_Ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.but_Ok.Location = new System.Drawing.Point(166, 63);
            this.but_Ok.Name = "but_Ok";
            this.but_Ok.Size = new System.Drawing.Size(85, 32);
            this.but_Ok.TabIndex = 1;
            this.but_Ok.Text = "LABI";
            this.but_Ok.UseVisualStyleBackColor = false;
            // 
            // but_back
            // 
            this.but_back.BackColor = System.Drawing.Color.Transparent;
            this.but_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.but_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.but_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.but_back.Location = new System.Drawing.Point(257, 63);
            this.but_back.Name = "but_back";
            this.but_back.Size = new System.Drawing.Size(82, 31);
            this.but_back.TabIndex = 2;
            this.but_back.Text = "ATPAKAĻ";
            this.but_back.UseVisualStyleBackColor = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(13, 15);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(310, 42);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // dialog_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 103);
            this.ControlBox = false;
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.but_back);
            this.Controls.Add(this.but_Ok);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dialog_main";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "dialog_main";
            this.Load += new System.EventHandler(this.dialog_main_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_Ok;
        private System.Windows.Forms.Button but_back;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}