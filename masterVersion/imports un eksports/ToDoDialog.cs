﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class ToDoDialog : Form
    {        
        main main = new main();
        SubMenuTestGen openImportedTest;
        KdData importedTasks = new KdData();
        CreatetPdf pdfMaker = new CreatetPdf();
        int currentVar = 0;
        List<SubMenuTestGen.WhiteSpace> whiteSpaceTypes = new List<SubMenuTestGen.WhiteSpace>();
        
        public ToDoDialog(KdData taskList, main _main)
        {
            InitializeComponent();
            importedTasks = taskList;
            main = _main;
        }

        public void setTest(KdData test)
        {
            importedTasks = test;
        }
        public void doVide(object sender, EventArgs e)
        {
            //veidošana
            openImportedTest = new SubMenuTestGen(main, importedTasks.Prieksmets, importedTasks.Klase, importedTasks.KdNosaukums);
            for(int i =0; i<importedTasks.Uzdevumi.Count;i++)
            {

                //timer.Tick += new EventHandler(CountDown);
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Tick += delegate (object senderr, EventArgs ee)
                { CountDown(sender, e, timer); };
                timer.Interval = 10;
                timer.Start();
                

            }
            openImportedTest.currentPage = openImportedTest.lapasObjekti[0];
            openImportedTest.Show();
            this.Hide();


        }
        private void CountDown(object sender, EventArgs e,Timer timer)
        {
            currentVar++;
            if (currentVar-1 != 0)
                openImportedTest.AddVariants(null, null);
            
            openImportedTest.currentPage = openImportedTest.lapasObjekti[currentVar - 1];
            foreach (Uzdevums uzd in importedTasks.Uzdevumi[currentVar - 1])
            {
                openImportedTest.currentPage.addUzdevums(uzd);
            }
            timer.Stop();
        }

        public void doPdf(object sender, EventArgs e)
        {
            //pdf maker
            for( int i = 0; i < importedTasks.Uzdevumi.Count; i++)
            {
                pdfMaker.Export(importedTasks.KdTips+" "+i.ToString()+". Versija", importedTasks.KdNosaukums,1,
                    importedTasks.Uzdevumi[i],importedTasks.Punkti[i], importedTasks.VariantuSk, importedTasks.WhiteSpaces[i],true);

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //back
            this.Close();
            main.Show();
        }
    }
}
