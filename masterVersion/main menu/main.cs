﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Drawing.Drawing2D;


namespace masterVersion
{
    public partial class main : MetroFramework.Forms.MetroForm
    {

        LoginConnect loginConnect = new LoginConnect();

        ImportExport import = new ImportExport();
        SubMenuTestGen subMenu;
        KlasuTemas klasuTemas = new KlasuTemas();
        PievienotUzdevumuMaster addTask;
        MyTests fChild;
        PievienotUzdevumuMaster fChild2;
        Profile fChild3;
        Message message = new Message("");
        Boolean profileDone = false;
        Boolean myTestsDone = false;
        char c = (char)0x2022;

        DialogResult result;
        dialog_main dialog_Main = new dialog_main();
        info info = new info();
        List<int> klases = new List<int>();
        List<string> temas = new List<string>();
        int klase;
        string tema;
        string subject;
        string errorMsg;
        string Error;
        string user;
        KdData selectedKD = null;
        ToDoDialog toAdd;

        enum IzveletaisTips
        {
            generetKD,
            veidotKD,
            importetKD,
            maniKD,
            PievienotUzd
        }
        IzveletaisTips CurrentSelected;


        public main()
        {

            this.SuspendLayout();
            try
            {
                //MessageBox.Show("startedNormally");
                database.LocalDB.LoadDB();
                TablesAndConstants.InitializeTables();
                CurrentSelected = IzveletaisTips.generetKD;
                InitializeComponent();
                Random rnd = new Random();

                //  webBrowser1.Navigate("http://satchmi.com/vinylday/");

                ComboBox_klase.Enabled = false;
                ComboBox_tema.Enabled = false;

                for (int i = 0; i < info.getAllSubjects().Count; i++)
                {
                    ComboBox_subject.Items.Add(info.getAllSubjects().ElementAt(i));
                }
                BTTNOpenGenTest.Focus();
                TogglePanels();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.ResumeLayout();

        }

        private bool validate()
        {
            if (ComboBox_klase.SelectedItem == null)
            {
                result = MessageBox.Show("Nav ievadīta klase, tiks atlasītas visas", "Ziņojums", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {

                    return true;
                }
                else
                {
                    return false;
                }

            }
            else if (ComboBox_subject.SelectedItem == null)
            {
                return true;
            }
            else
            {
                return true;
            }

        }
        private void ComboBox_subject_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox_klase.Enabled = true;
            ComboBox_tema.Enabled = true;

            subject = ComboBox_subject.SelectedItem.ToString();


            ComboBox_klase.Items.Clear();
            ComboBox_tema.Items.Clear();

            klases = klasuTemas.prieksmetaKlases(subject);
            temas = klasuTemas.getTemas(0);

            for (int i = 0; i < klases.Count; i++)
            {
                ComboBox_klase.Items.Add(klases.ElementAt(i)).ToString();
            }

            for (int k = 0; k < temas.Count; k++)
            {
                ComboBox_tema.Items.Add(temas.ElementAt(k).ToString());
            }

            //ComboBox_tema.Text = "Gravity and movement in a gravitational field";
            //ComboBox_klase.Text = "10";

        }

        private void BTTN_OpenTestGen_Click()
        {
            if (this.validate() == true)
            {
                if (int.TryParse(ComboBox_klase.SelectedItem.ToString(), out klase) == true)
                {
                    klase = int.Parse(ComboBox_klase.SelectedItem.ToString());
                }
                else
                {
                    klase = 0;
                }

                subMenu = new SubMenuTestGen(this, ComboBox_subject.SelectedItem.ToString(), klase, tema);
                subMenu.Show();
                this.Hide();

            }
        }

        private void BTTN_ImportTetst_Click()
        {
            import.importTest(this);
        }

        private void button1_Click()
        {
            //TestMaker.Show();
            //subMenu = new SubMenuTestGen(this, ComboBox_subject.SelectedItem.ToString(), int.Parse(ComboBox_klase.SelectedItem.ToString()), ComboBox_tema.SelectedItem.ToString());
            //subMenu.Show();
            //this.Hide();
        }

        private void main_Load(object sender, EventArgs e)
        {
            SuspendLayout();
            LoginMenu.Visible = false;
            // ComboBox_subject.Text = "Physics";
            // ComboBox_klase.Text = "10";
            //ComboBox_tema.Text = "Gravity and movement in a gravitational field";
            this.setLoginStuff();
            buttonPanel.Height = this.Height;
            buttonPanel.Paint += Panel1_Paint;
            buttonPanel.Location = new Point(this.Location.X, this.Location.Y);
            panel2.Location = new Point(buttonPanel.Location.X + buttonPanel.Width + 5, panel2.Location.Y);
            currentFeat.Location = new Point(buttonPanel.Location.X + buttonPanel.Width + 5, currentFeat.Location.Y);
            info_but_help.Location = new Point(buttonPanel.Location.X + buttonPanel.Width + 5, info_but_help.Location.Y);
            info_but_feedback.Location = new Point(info_but_help.Location.X + info_but_help.Width + 5, info_but_help.Location.Y);
            info_but_contacts.Location = new Point(info_but_feedback.Location.X + info_but_feedback.Width + 5, info_but_help.Location.Y);
            button2.Width = buttonPanel.Width;
            Profile.Width = buttonPanel.Width;
            button1.Width = buttonPanel.Width;
            this.closeBut();
            LoginMenu.Location = new Point(0, 0);



            this.LoginMenu.Size = this.Size;
            ResumeLayout();

            if (!Properties.Settings.Default.DontShow)
            {
                new Message("Beta").ShowDialog();
            }

        }

        private void closeBut()
        {
            but_close.FlatStyle = FlatStyle.Flat;
            but_close.FlatAppearance.BorderSize = 0;
            but_close.BackColor = Color.Transparent;
            but_close.Location = new Point(this.Width - 50, 20);
            but_close.Size = new Size(40, 40);
            but_close.Text = "X";
        }

        private void betaMessage()
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            LinearGradientBrush linearGradientBrush =
            new LinearGradientBrush(buttonPanel.ClientRectangle, Color.FromArgb(16, 31, 64), Color.FromArgb(34, 120, 190), 30);

            ColorBlend cblend = new ColorBlend(3);
            cblend.Colors = new Color[3] { Color.FromArgb(16, 31, 64), Color.FromArgb(34, 120, 190), Color.FromArgb(91, 178, 190) };
            cblend.Positions = new float[3] { 0f, 0.5f, 1f };

            linearGradientBrush.InterpolationColors = cblend;

            e.Graphics.FillRectangle(linearGradientBrush, buttonPanel.ClientRectangle);
        }

        private void setLoginStuff()
        {
            Login.Location = new Point(this.ClientSize.Width / 2 - Login.Width / 2, (this.ClientSize.Height / 2 - Login.Height / 2) + 30);
            logo.Location = new Point(Login.Location.X - 200, Login.Location.Y - 280);
            PasswordTxt.Location = new Point(Login.Location.X, Login.Location.Y - 30);
            showPassword.Location = new Point(Login.Location.X + PasswordTxt.Width + 2, Login.Location.Y - 30);
            LoginTxt.Location = new Point(Login.Location.X, Login.Location.Y - 60);
            Register.Location = new Point(Login.Location.X, Login.Location.Y + 41);
            Help.Location = new Point(Login.Location.X, Login.Location.Y + 82);
            Contact.Location = new Point(Login.Location.X, Login.Location.Y + 106);
            buttonPanel.Width = 200;
            buttonPanel.Visible = false;

            PasswordTxt.Text = "manaParole";
            PasswordTxt.PasswordChar = c;
            LoginTxt.Text = "Baiba";
            LoginMenu.Visible = true;

        }

        //#1 MOUSE EVENTS
        //#1.1 MOUSE ENTERS
        private void Mouse_Enter(Object sender, EventArgs e)
        {
            Profile.FlatAppearance.MouseDownBackColor = Color.Transparent;
            Profile.ForeColor = Color.FromArgb(0x5bb2cc);
        }
        private void Mouse_Enter_1(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.BackColor = Color.FromArgb(0x5bb2cc);

        }
        private void Mouse_ShowPassWord_Enter(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.Image = masterVersion.Properties.Resources.eye_open;
            PasswordTxt.PasswordChar = default(char);
        }
        private void Mouse_Login_Menu_Enter(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.ForeColor = Color.FromArgb(91, 178, 204);
        }
        private void Mouse_Exit_Enter(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.BackColor = Color.FromArgb(130, 233, 255);
        }
        private void PasswordTxt_Enter(Object sender, EventArgs e)
        {
            if (PasswordTxt.Text == "Parole")
            {
                PasswordTxt.Text = null;
            }
        }
        private void LoginTxt_Enter(Object sender, EventArgs e)
        {
            if (LoginTxt.Text == "Lietotājvārds")
            {
                LoginTxt.Text = null;
            }
        }

        //#1.1 MOUSE LEAVES
        private void Mouse_Leave(Object sender, EventArgs e)
        {
            Profile.FlatAppearance.MouseDownBackColor = Color.Transparent;
            Profile.ForeColor = Color.White;
        }
        private void Mouse_Leave_1(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.BackColor = Color.Transparent;
        }
        private void Mouse_Login_Menu_Leave(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.ForeColor = Color.Black;
        }
        private void Mouse_ShowPassWord_Leave(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.Image = masterVersion.Properties.Resources.eye_close;
            PasswordTxt.PasswordChar = c;
        }
        private void Mouse_Exit_Leave(Object sender, EventArgs e)
        {
            Button but = sender as Button;
            but.BackColor = Color.Transparent;
        }
        private void PasswordTxt_Leave(Object sender, EventArgs e)
        {
            if (PasswordTxt.Text == "")
            {
                PasswordTxt.Text = "Parole";
            }
        }
        private void LoginTxt_Leave(Object sender, EventArgs e)
        {
            if (LoginTxt.Text == "")
            {
                LoginTxt.Text = "Lietotājvārds";
            }
        }

        //1.2 MOUSE CLICK
        private void BTTN_CloseMain_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void appExit(Object sender, EventArgs e)
        {
            this.Close();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (selectedKD != null)
                toAdd.doVide(null, null);

        }
        private void Contact_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://ingeniumeducation.com/feedback/");
        }

        private string validate_gen_click(string topic)
        {
            return "Nav izvēlēta apakšētēma. Uzdevumu atlase var prasīt ilgu laiku, vai turpināt?";
        }

        private void doStuffForGen(string subejct, int grade, string topic)
        {
            subMenu = new SubMenuTestGen(this, subject, grade, topic);
            subMenu.MdiParent = this;
            panel2.Visible = false;
            subMenu.Show();
            subMenu.Location = new Point(this.Location.X + 180, this.Location.Y);
            subMenu.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            subMenu.ControlBox = false;
            subMenu.MaximumSize = subMenu.Size;
            subMenu.MinimumSize = subMenu.Size;
        }

        private void getTasks()
        {
            string subject = ComboBox_subject.Text;
            currentFeat.Visible = false;
            int grade = Int32.Parse(ComboBox_klase.Text);
            string topic = ComboBox_tema.Text;

            if (topic == "")
            {
                dialog_Main.setText("apakštēma", false);
                dialog_Main.ShowDialog();

                if (dialog_Main.DialogResult == DialogResult.Cancel)
                {
                    dialog_Main.Close();
                    dialog_Main.Dispose();
                }
                else
                {
                    dialog_Main.Dispose();
                    if (subMenu == null)
                    {
                        this.doStuffForGen(subject, grade, topic);
                    }
                    if (panel2.Visible == true)
                    {
                        panel2.Visible = false;
                    }
                }
            }
            else
            {
                this.doStuffForGen(subject, grade, topic);
            }

            if (fChild2 != null)
            {
                fChild2.Close();
                fChild2 = null;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (ComboBox_subject.Text == "" || ComboBox_klase.Text == "" || ComboBox_tema.Text == "")
            {
                MessageBox.Show("Lūdzu ievadi priekšmetu, klasi un tēmu");
            }
            else
            {
                if (ComboBox_tema.Text == "Visas")
                {
                    DialogResult result = MessageBox.Show("Atlasīt visus uzdevumus var prasīt ilgu laiku. Vai turpināt?", "Uzmanību!", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        this.getTasks();
                    }
                }
                else
                {
                    this.getTasks();
                }   
            
            }
        }
        private void profile_Click(object sender, EventArgs e)
        {
            if (profileDone == false)
            {
                // MessageBox.Show("Currently working on this feature! Will be available soon!");
                MessageBox.Show("Šobrīd aktīvi stādājam pie šī rīka! Drīzumā būs pieejams!");
            }
            else
            {
                if (fChild3 == null)
                {
                    fChild3 = new Profile();
                    fChild3.MdiParent = this;
                    fChild3.Show();
                    fChild3.Location = new Point(this.Location.X + 180, this.Location.Y);
                    fChild3.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                }

                if (panel2.Visible == true)
                {
                    panel2.Visible = false;
                }

                if (fChild != null)
                {
                    fChild.Close();
                    fChild = null;
                }

                if (fChild2 != null)
                {
                    fChild2.Close();
                    fChild2 = null;
                }

                if (subMenu != null)
                {
                    subMenu.Close();
                    subMenu = null;
                }
            }
        }
        private void generate_Click(object sender, EventArgs e)
        {
            if(subMenu == null)
            {
                if (fChild2 != null)
                {
                    DialogResult result = MessageBox.Show("Izdarītais darbs tiks zaudēts. Vai turpināt?", "Uzmanību!", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        fChild2.Close();
                        panel2.Visible = true;
                        button5.Visible = false;
                        BTTN_OpenTestGen.Visible = true;
                        currentFeat.Text = "Izveidot kontroldarbu";
                        button3.Enabled = true;
                    }
                }
                else
                {
                    panel2.Visible = true;
                    button5.Visible = false;
                    BTTN_OpenTestGen.Visible = true;
                    currentFeat.Text = "Izveidot kontroldarbu";
                    button3.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Jau lietojiet kontroldarbu veidotāju!");
                panel2.Visible = false;
            }
        }

        private void taskCreator_Click(object sender, EventArgs e)
        {
            BTTN_OpenTestGen.Visible = false;
            button5.Visible = true;
            currentFeat.Text = "Izveidot uzdevumu";
            button3.Enabled = false;
            if(subMenu != null) 
            {
                if (subMenu.Visible == true)
                {
                    DialogResult result = MessageBox.Show("Izdarītais darbs tiks zaudēts. Vai turpināt?", "Uzmanību!", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        subMenu.Close();
                        subMenu.Dispose();
                        panel2.Show();
                        subMenu = null;
                        button5.Visible = true;
                        BTTN_OpenTestGen.Visible = false;
                        currentFeat.Visible = true;
                    }
                }
            }
        }




        private void Register_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://ingeniumeducation.com/register/");
        }
        private void Login_Click(Object sender, EventArgs e)
        {
            if (loginConnect.Connect() == true)
            {
                loginConnect.Connect();
                user = null;

                user = loginConnect.findUser(LoginTxt.Text.Replace("ā", "a"), PasswordTxt.Text);
                if (user == LoginTxt.Text + PasswordTxt.Text)
                {
                    LoginMenu.Visible = false;
                    buttonPanel.Visible = true;
                    panel2.Visible = true;
                    Profile.Location = new Point(buttonPanel.Location.X / 2, buttonPanel.Location.Y + 30);
                    BTTNOpenGenTest.Location = new Point(buttonPanel.Location.X / 2, buttonPanel.Location.Y + 180);
                    button1.Location = new Point(buttonPanel.Location.X / 2, buttonPanel.Location.Y + 230);
                    button2.Location = new Point(buttonPanel.Location.X / 2, buttonPanel.Location.Y + 280);
                    Profile.ForeColor = Color.Gray;
                    button1.ForeColor = Color.Gray;
                    button2.ForeColor = Color.White;
                    BTTNOpenGenTest.ForeColor = Color.White;
                    info_but_help.Visible = true;
                    info_but_contacts.Visible = true;
                    info_but_feedback.Visible = true;
                    Profile.Text = LoginTxt.Text;
                    currentFeat.Text = "Izveidot kontroldarbu";
                    currentFeat.Visible = true;

                }
                else
                {
                    wrong_password.Visible = true;
                    wrong_password.Location = new Point(this.ClientSize.Width / 2 - wrong_password.Width / 2, this.ClientSize.Height / 2 - wrong_password.Height / 2);
                }
                loginConnect.closeConncetion();
            }
            else
            {
                MessageBox.Show("Programmai neizdevās savienoties ar internetu. Lūdzu mēģiniet vēlreiz, ja neizdodas sazinienties ar mums!" + "/r/n" + "e-pasts : ingenium@ingeniumeducation.com" );
            }
        }
        private void button2_Click()
        {
            addTask = new PievienotUzdevumuMaster(subject, klase, tema);
            addTask.Show();
        }
        private void TestBtn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            KdData info = import.importTest(this, btn.Tag.ToString());
            // KdNosaukums.Text = info.KdNosaukums;
            // KdTema.Text = info.Prieksmets;
            // KdVariantuSk.Text = info.VariantuSk;
            // KdKlase.Text = info.Klase.ToString();
            string taskCount = "";
            toAdd.setTest(info);
            foreach (List<Uzdevums> uzdSarksts in info.Uzdevumi)
            {
                taskCount += uzdSarksts.Count.ToString() + ", ";
            }
            taskCount = taskCount.Remove(taskCount.Length - 2);
            // KdUzdevumuSk.Text = taskCount;
            selectedKD = info;

            int o = 0;
        }
        private void myTests_Click(object sender, EventArgs e)
        {
            if (myTestsDone == false)
            {
                //MessageBox.Show("Currently working on this feature! Will be available soon!");
                MessageBox.Show("Šobrīd aktīvi stādājam pie šī rīka! Drīzumā būs pieejams!");
            }
            else
            {
                if (fChild == null)
                {
                    fChild = new MyTests();
                    fChild.MdiParent = this;
                    fChild.Show();
                    fChild.Location = new Point(this.Location.X + 180, this.Location.Y);
                    fChild.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                }

                if (panel2.Visible == true)
                {
                    panel2.Visible = false;
                }

                if (subMenu != null)
                {
                    subMenu.Close();
                    subMenu = null;
                }

                if (fChild2 != null)
                {
                    fChild2.Close();
                    fChild2 = null;
                }

                if (fChild3 != null)
                {
                    fChild3.Close();
                    fChild3 = null;
                }
            }
        }
        private void openTaskCreator(object sender, EventArgs e)
        {

            if (ComboBox_subject.Text == "" || ComboBox_klase.Text == "" || ComboBox_tema.Text == "")
            {
                MessageBox.Show("Lūdzu ievadi priekšmetu, klasi un tēmu");
            }
            else
            {
                int klase1 = Int32.Parse(ComboBox_klase.Text);
                fChild2 = new PievienotUzdevumuMaster(ComboBox_subject.Text, klase1, ComboBox_tema.Text);
                fChild2.MdiParent = this;
                fChild2.Show();
                fChild2.Location = new Point(this.Location.X + 180, this.Location.Y);
                fChild2.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                fChild2.ControlBox = false;
                fChild2.MaximumSize = fChild2.Size;
                fChild2.MinimumSize = fChild2.Size;
                panel2.Visible = false;
            }
        }
      

        private void showPassword_Click(object sender, EventArgs e)
        {
            Button but = sender as Button;
            PasswordTxt.PasswordChar = default(char);
            but.Image = masterVersion.Properties.Resources.eye_open;
        }

        //OTHER EVENTS
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox_tema.Items.Clear();
            if (int.TryParse(ComboBox_klase.SelectedItem.ToString(), out klase) == true)
            {
                temas = klasuTemas.getTemas(klase);
                try
                {
                    for (int i = 0; i < temas.Count; i++)
                    {
                        ComboBox_tema.Items.Add(temas.ElementAt(i));
                    }
                }
                catch
                {

                }
            }
            else
            {
                for (int i = 0; i < klases.Count; i++)
                {
                    ComboBox_klase.Items.Add(klases.ElementAt(i)).ToString();
                }

            }
            ComboBox_tema.Text = "Visas tēmas";
        }
        private void SetEnum(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            CurrentSelected = (IzveletaisTips)Int32.Parse((string)btn.Tag);
            // Title.Text = btn.Text.Replace("KD", "kontroldarbu");
            //Title.Text = Title.Text.Replace("uzd", "uzdevumu");
            TogglePanels();
            // webNav();

        }
        private void TogglePanels()
        {
            if (CurrentSelected == IzveletaisTips.importetKD || CurrentSelected == IzveletaisTips.maniKD)
            {
                //panel2.Visible = false;
            }
            else
            {
                // panel2.Visible = true;
            }

            if (CurrentSelected == IzveletaisTips.maniKD)
            {
                if (toAdd == null)
                {
                    toAdd = new ToDoDialog(selectedKD, this);
                    toAdd.Hide();
                }

                // webBrowser1.Visible = false;
                // folderPanel.Visible = true;
                button3.Visible = false;
                //  exportButtonPanel.Visible = true;
                //  testInfoPanel.Visible = true;

            }

            else
            {
                // webBrowser1.Visible = true;
                // folderPanel.Visible = false;
                button3.Visible = true;
                //  exportButtonPanel.Visible = false;
                // testInfoPanel.Visible = false;
            }

        }
        private void ComboBox_tema_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBox_tema.Text != "Visas tēmas")
            {
                tema = ComboBox_tema.SelectedItem.ToString();
                ComboBox_klase.SelectedItem = klasuTemas.getTemasKlasi(tema);
            }
        }
        private void StartProcess(object sender, EventArgs e)
        {
            if (checkSubejectClass() == null)
            {
                switch (CurrentSelected)
                {
                    case IzveletaisTips.generetKD:
                        generateKD();
                        break;
                    case IzveletaisTips.veidotKD:
                        BTTN_OpenTestGen_Click();
                        break;
                    case IzveletaisTips.importetKD:
                        BTTN_ImportTetst_Click();


                        break;
                    case IzveletaisTips.maniKD:
                        button1_Click();
                        break;
                    case IzveletaisTips.PievienotUzd:
                        button2_Click();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                MessageBox.Show(Error);
            }
        }


        // TEST CREATION

        private void ComboBox_tema_DrawItem(object sender, DrawItemEventArgs e)
        {

            helpers.centerComboBox.cbxDesign_DrawItem(sender, e);

        }

        // MAKE ERROR MSG
        private string makeErrorMsg(string errorMsg)
        {
            Error = "";
            Char delimiter = '/';
            String[] subs = errorMsg.Split(delimiter);
            for (int i = 0; i < subs.Count(); i++)
            {
                if (subs.ElementAt(i).ToString() != "")
                {
                    Error += "\u2022" + " " + subs.ElementAt(i) + "\n";
                }
            }

            return "Darbību neizdevās veikt, sekojošu iemeslu dēļ : \n" + Error;
        }


        //MENUS
       

        private void generateKD()
        {

            GenerateKD generator = new GenerateKD(klase, ComboBox_subject.Text, tema, 1, this);


        }
        private string checkSubejectClass()
        {
            if ((ComboBox_subject.Text == "") || (ComboBox_klase.Text == "") || ComboBox_tema.Text == "")
            {
                errorMsg = "";

                if (ComboBox_subject.Text == "")
                {
                    errorMsg += "Macību priekšmets nav ievadīts/";
                }
                if (ComboBox_klase.Text == "")
                {
                    errorMsg += "Klase nav ievadīta/";
                }
                if (ComboBox_tema.Text == "")
                {
                    errorMsg += "Tēma nav ievadīta/";
                }
                return Error = makeErrorMsg(errorMsg);
            }
            else
            {
                return null;
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            wrong_password.Visible = false;
        }

        private void Profile_MouseHover(object sender, EventArgs e)
        {
            htmlToolTip1.Show("Šobrīd aktīvi strādājam pie šī rīka. Drīzumā būs!", Profile, 0);
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            htmlToolTip1.Show("Šobrīd aktīvi strādājam pie šī rīka. Drīzumā būs!", button1, 0);
        }

        private void showPassword_Click_1(object sender, EventArgs e)
        {

        }

        private void info_but_help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Drīzumā būs!");
        }

        private void info_but_feedback_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://ingeniumeducation.com/feedback/");
        }

        private void info_but_contacts_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://ingeniumeducation.com/feedback/");
        }
    }
}

