﻿namespace masterVersion
{
    partial class SubMenuTestGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.filepanel = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.exportBtn = new System.Windows.Forms.Button();
            this.VariationCount = new System.Windows.Forms.ComboBox();
            this.exportPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.filepanel.SuspendLayout();
            this.exportPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // filepanel
            // 
            this.filepanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.filepanel.Controls.Add(this.button3);
            this.filepanel.Controls.Add(this.button2);
            this.filepanel.Location = new System.Drawing.Point(6, 46);
            this.filepanel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.filepanel.Name = "filepanel";
            this.filepanel.Size = new System.Drawing.Size(128, 182);
            this.filepanel.TabIndex = 0;
            this.filepanel.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(2, 32);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 26);
            this.button3.TabIndex = 1;
            this.button3.Text = "Izvēlēties lontroldarbu";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(2, 3);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 26);
            this.button2.TabIndex = 0;
            this.button2.Text = "Izveidot kontroldarbu";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(4, 28);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(121, 21);
            this.exportBtn.TabIndex = 25;
            this.exportBtn.Text = "Export";
            this.exportBtn.UseVisualStyleBackColor = true;
            // 
            // VariationCount
            // 
            this.VariationCount.FormattingEnabled = true;
            this.VariationCount.Items.AddRange(new object[] {
            "2",
            "4",
            "5",
            "8",
            "10",
            "15",
            "20"});
            this.VariationCount.Location = new System.Drawing.Point(4, 0);
            this.VariationCount.Name = "VariationCount";
            this.VariationCount.Size = new System.Drawing.Size(121, 25);
            this.VariationCount.TabIndex = 26;
            this.VariationCount.Text = "4";
            // 
            // exportPanel
            // 
            this.exportPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.exportPanel.Controls.Add(this.exportBtn);
            this.exportPanel.Controls.Add(this.VariationCount);
            this.exportPanel.Location = new System.Drawing.Point(2, 243);
            this.exportPanel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.exportPanel.Name = "exportPanel";
            this.exportPanel.Size = new System.Drawing.Size(128, 182);
            this.exportPanel.TabIndex = 2;
            this.exportPanel.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(131, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 46);
            this.button1.TabIndex = 3;
            this.button1.Text = "pievienot variantu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.AddVariants);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(122, 46);
            this.button4.TabIndex = 4;
            this.button4.Text = "Skatīt visus variantus";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.showVariants);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button6);
            this.flowLayoutPanel1.Controls.Add(this.button5);
            this.flowLayoutPanel1.Controls.Add(this.button7);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(998, 49);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(261, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 46);
            this.button6.TabIndex = 7;
            this.button6.Text = "Veidot pdf";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.CreatePdf);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(355, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 46);
            this.button5.TabIndex = 6;
            this.button5.Text = "pievienot uzd";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(461, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(99, 46);
            this.button7.TabIndex = 8;
            this.button7.Text = "eksportēt KD";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.exportTest);
            // 
            // SubMenuTestGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(1149, 665);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.exportPanel);
            this.Controls.Add(this.filepanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "SubMenuTestGen";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SubMenuTestGen_FormClosing);
            this.filepanel.ResumeLayout(false);
            this.exportPanel.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel filepanel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button exportBtn;
        private System.Windows.Forms.ComboBox VariationCount;
        private System.Windows.Forms.Panel exportPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}