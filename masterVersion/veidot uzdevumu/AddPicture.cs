﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class AddPicture : Form
    {
        
        PievienotUzdevumuMaster parent;
        Bitmap image = null;
        public AddPicture(PievienotUzdevumuMaster parent)
        {
            this.parent = parent;
            InitializeComponent();
            RemoveImageBtn.Visible = false;
            //addButton.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
        }
        public AddPicture(PievienotUzdevumuMaster parent, Bitmap bmp)
        {
            image = bmp;
            this.parent = parent;
            InitializeComponent();
            RemoveImageBtn.Visible = true;
            addButton.BackgroundImage = image;
            addButton.Image = null;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (image == null)
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "Image files |*.jpg;*png;*gif|All files (*.*)|*.*";
                DialogResult result = open.ShowDialog();
                if (result == DialogResult.OK)
                {
                    addImage(open.FileName);
                }
            }
        }
        private void addImage(String path)
        {
            image = new Bitmap(path);
            addButton.BackgroundImage = image;
            addButton.Image = null;
            parent.AddPicture(path);
            RemoveImageBtn.Visible = true;
        }
        private void RemoveImageBtn_Click(object sender, EventArgs e)
        {
            image = null;
            parent.RemovePicture();
            addButton.BackgroundImage = null;
            addButton.Image = Properties.Resources.add_Image;
            RemoveImageBtn.Visible = false;
        }

        private void addButton_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            string ext = Path.GetExtension(files[0]);
            if(ext == ".png" || ext == ".jpg" || ext == ".jpeg" || ext == ".gif")
            {
                addImage(files[0]);
            }
            else
            {
                helpers.Dialogs.ShowMessage("Šāds formāts netiek atbalstīts.\n Izmantojiet kādu no šiem formātiem: '.png', 'jpeg' vai '.gif'.");
            }
            
        }

        private void addButton_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }
    }
}
