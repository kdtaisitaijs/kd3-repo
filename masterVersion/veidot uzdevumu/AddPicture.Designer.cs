﻿namespace masterVersion
{
    partial class AddPicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.RemoveImageBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(182, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pievienot attēlu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(452, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nospiediet mapes ikonu, lai navigētu uz attēlu, vai ievelkat to šajā logā";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // addButton
            // 
            this.addButton.AllowDrop = true;
            this.addButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.addButton.Image = global::masterVersion.Properties.Resources.add_Image;
            this.addButton.Location = new System.Drawing.Point(12, 54);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(515, 267);
            this.addButton.TabIndex = 2;
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            this.addButton.DragDrop += new System.Windows.Forms.DragEventHandler(this.addButton_DragDrop);
            this.addButton.DragEnter += new System.Windows.Forms.DragEventHandler(this.addButton_DragEnter);
            // 
            // RemoveImageBtn
            // 
            this.RemoveImageBtn.Location = new System.Drawing.Point(495, 63);
            this.RemoveImageBtn.Name = "RemoveImageBtn";
            this.RemoveImageBtn.Size = new System.Drawing.Size(23, 23);
            this.RemoveImageBtn.TabIndex = 3;
            this.RemoveImageBtn.Text = "X";
            this.RemoveImageBtn.UseVisualStyleBackColor = true;
            this.RemoveImageBtn.Click += new System.EventHandler(this.RemoveImageBtn_Click);
            // 
            // AddPicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(539, 333);
            this.Controls.Add(this.RemoveImageBtn);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddPicture";
            this.Text = "Pievienot attēlu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button RemoveImageBtn;
    }
}