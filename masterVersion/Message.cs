﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace masterVersion
{
    public partial class Message : MetroFramework.Forms.MetroForm
    {
        public Message(string funkc)
        {
            InitializeComponent();
            if (funkc == "Beta")
            {
                this.richTextBox1.Text =
                    "Paldies, ka iegādājāties Ingenium kontroldarbu veidotāju! " + "\r\n" +
                    "Mes esam priecīgi redzot, ka lieto mūsu produktu. Tā, kā produkts vēl ir aktīvas izstrādes stadijā, piedod, par nesmukumiem un kādu kļūdiņu " +
                    "Vari ar mums droši sazināties, par jautājumiem saistībā ar Ingenium" +
                    "\r\n" + "e-pasts - ingenium@ingeniumeducation.com" +
                    "\r\n" + "Veiksmi vēlot \r\n Ingenium komanda";
            }
            if (funkc == "userPass")
            {
                checkBox_MF_rem.Visible = false;
                this.richTextBox1.Text =
                    "Lietotājvārds un/vai parole nav pareizs!";
            }
        }

        private void checkBox_MF_rem_CheckedChanged(object sender, EventArgs e)
        {
           // Properties.Settings.Default.DontShow = true;
           // Properties.Settings.Default.Save();
        }

        private void but_MF_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
