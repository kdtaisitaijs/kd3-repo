﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    class GenerateKD
    {
        public int Class;
        public string Subject;
        public string Topic;
        public int VariantuSkaits;
        DatabaseAccess database = new DatabaseAccess();
        List<Uzdevums> uzdevumuListTema = new List<Uzdevums>();
        Uzdevums[] tempList = new Uzdevums[10];
        Random rnd = new Random();
        SubMenuTestGen openImportedTest;
        int hardTaskCount = 0;
        int easyTaskCount = 2;
        int normalTaskCount = 0;
        int currentVar = 0;
        main Main;
        KlasuTemas klasuTemas = new KlasuTemas();

        public GenerateKD()
        {

        }

        public GenerateKD(int Class, string subject, string topic, int variantuSkaits, main main)
        {
            this.Class = Class;
            Subject = subject;
            Topic = topic;
            VariantuSkaits = variantuSkaits;
            Main = main;

            createKD(topic);
        }

        void createKD(string topic)
        {
            openImportedTest = new SubMenuTestGen(Main, Subject, Class, topic);
          //  subMenu = new SubMenuTestGen(this, ComboBox_subject.SelectedItem.ToString(), klase, tema);
            uzdevumuListTema.Clear();
            // if (database.Connect())
            {

                //TODO: add refrence to actual topics                

                uzdevumuListTema = database.findExerciseBytopic(topic);
                //for(int i=8;i<11;i++)
                //uzdevumuListTema.AddRange(database.findExerciseBytopics(klasuTemas.getTemas(i).ToArray()));
                database.closeConncetion();
            }
            for (int i = 0; i < VariantuSkaits; i++)
            {

                //timer.Tick += new EventHandler(CountDown);
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Tick += delegate (object senderr, EventArgs ee)
                { addTasks(senderr, ee, timer); };
                timer.Interval = 10;
                timer.Start();


            }
            //openImportedTest.currentPage = openImportedTest.lapasObjekti[0];
            openImportedTest.Show();            

        }

        private void addTasks(object sender, object e, Timer timer)
        {
            timer.Stop();
            bool generating = true;
            int easy = 0;
            int med = 0;
            int hard = 0;            
            currentVar++;
            //if (currentVar - 1 != 0)
                openImportedTest.AddVariants(null, null);

            openImportedTest.currentPage = openImportedTest.lapasObjekti[currentVar - 1];


            for (int i = 0; i < 7; i++)
            {
                int taskIndex = rnd.Next(uzdevumuListTema.Count);
                Uzdevums task = uzdevumuListTema[taskIndex];
                uzdevumuListTema.Remove(task);
                openImportedTest.currentPage.addUzdevums(task);
            }

          //TODO: fix parametric generation
            /* while (generating)
             {

                 int taskIndex =rnd.Next(uzdevumuListTema.Count);
                 Uzdevums task = uzdevumuListTema[taskIndex];
                 int diff = task.getDifficulty();
                 bool isCalculation = !(task.getIsMultChoice() || task.getIsTheory());
                 //30% easy uzdevumi - 1 vai 2 *, mult ch vai teorija
                 if (diff<3 && !isCalculation && easyTaskCount > easy)
                 {
                     tempList[easy] = task;                    
                     easy++;

                 }else
                 {
                     // 60% normali uzdevumi - 2 lidz 4 zvaigznes, apreķins

                     //TODO: fix db, so isTheroetical and isMultCh can be retrieved
                     if (diff>2 && diff<5 && !isCalculation && normalTaskCount > med)
                     {
                         tempList[easyTaskCount + med] = task;
                         med++;

                     }else
                     {
                         //10 % gruti uzdevumi 5 *.
                         if (diff>4 && hardTaskCount > hard)
                         {
                             tempList[easyTaskCount + normalTaskCount + hard] = task;
                             hard++;
                         }
                     }
                 }
                 uzdevumuListTema.Remove(task);
                 if (uzdevumuListTema.Count < 3)
                 {
                     foreach (Uzdevums uzd in tempList)
                     {
                         openImportedTest.currentPage.addUzdevums(uzd);
                     }
                     generating = false;

                 }
                 if(hard==hardTaskCount && med== normalTaskCount && easy == easyTaskCount)
                 {
                     //visi uzdevumi pievienoti
                     foreach (Uzdevums uzd in tempList)
                     {
                         openImportedTest.currentPage.addUzdevums(uzd);
                     }
                     generating = false;
                 }   
             }*/


        }

      
    }
}
