-- commetars

drop database if exists kdgen;
create database kdgen
CHARACTER SET utf8 COLLATE utf8_latvian_ci;
use kdgen;

DROP TABLE IF EXISTS `Uzdevumi`;

CREATE TABLE `Uzdevumi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grade` int(2) unsigned,
  `topic` varchar(40) ,
  `text` varchar(150) NOT NULL ,
  `difficulty` int(1),
  `equation` varchar(200),
  `choices` varchar(500),
  `answer` varchar(200),
  
  
  
  PRIMARY KEY (`id`)
) ;
