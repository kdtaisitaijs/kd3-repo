﻿using System;

namespace masterVersion
{
    partial class PievienotUzdevumuMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel formulaPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox multipleChAns;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox theoreticalCheck;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.FlowLayoutPanel SpecificPanel;
        private System.Windows.Forms.ComboBox subtemaBox;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroButton diffBtn1;
        private MetroFramework.Controls.MetroButton diffBtn2;
        private MetroFramework.Controls.MetroButton diffBtn3;
        private MetroFramework.Controls.MetroButton diffBtn4;
        private MetroFramework.Controls.MetroButton diffBtn5;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private System.Windows.Forms.Label label6;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PievienotUzdevumuMaster));
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.label7 = new System.Windows.Forms.Label();
			this.formulaPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.multipleChAns = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.richTextBox3 = new System.Windows.Forms.RichTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.button22 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.functionBtn = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.subtemaBox = new System.Windows.Forms.ComboBox();
			this.SpecificPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.diffBtn5 = new MetroFramework.Controls.MetroButton();
			this.diffBtn4 = new MetroFramework.Controls.MetroButton();
			this.diffBtn3 = new MetroFramework.Controls.MetroButton();
			this.diffBtn2 = new MetroFramework.Controls.MetroButton();
			this.diffBtn1 = new MetroFramework.Controls.MetroButton();
			this.theoreticalCheck = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
			this.button2 = new System.Windows.Forms.Button();
			this.AddImageButton = new System.Windows.Forms.Button();
			this.BTTN_addTaskToDB = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel5.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBox1
			// 
			this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.richTextBox1.Location = new System.Drawing.Point(12, 370);
			this.richTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(700, 191);
			this.richTextBox1.TabIndex = 1;
			this.richTextBox1.Text = "";
			this.richTextBox1.Click += new System.EventHandler(this.multipleChAns_Click);
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// richTextBox2
			// 
			this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.richTextBox2.Location = new System.Drawing.Point(12, 30);
			this.richTextBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(477, 54);
			this.richTextBox2.TabIndex = 4;
			this.richTextBox2.Text = "";
			this.richTextBox2.Click += new System.EventHandler(this.multipleChAns_Click);
			this.richTextBox2.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label2.Location = new System.Drawing.Point(349, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(31, 25);
			this.label2.TabIndex = 5;
			this.label2.Text = "SI";
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.AutoScroll = true;
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(7, 26);
			this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(151, 187);
			this.flowLayoutPanel1.TabIndex = 9;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
			this.label3.Location = new System.Drawing.Point(721, 374);
			this.label3.MaximumSize = new System.Drawing.Size(900, 800);
			this.label3.MinimumSize = new System.Drawing.Size(411, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(700, 191);
			this.label3.TabIndex = 10;
			this.label3.Text = "Formatēts uzdevuma teskts";
			this.label3.Click += new System.EventHandler(this.label3_Click);
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.AutoScroll = true;
			this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel2.Location = new System.Drawing.Point(164, 26);
			this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(165, 190);
			this.flowLayoutPanel2.TabIndex = 10;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "empty.png");
			this.imageList1.Images.SetKeyName(1, "full.png");
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label7.Location = new System.Drawing.Point(616, 81);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(98, 25);
			this.label7.TabIndex = 21;
			this.label7.Text = "Formulas ";
			// 
			// formulaPanel
			// 
			this.formulaPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.formulaPanel.Location = new System.Drawing.Point(495, 107);
			this.formulaPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.formulaPanel.Name = "formulaPanel";
			this.formulaPanel.Size = new System.Drawing.Size(348, 172);
			this.formulaPanel.TabIndex = 22;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.72727F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.272727F));
			this.tableLayoutPanel1.Controls.Add(this.multipleChAns, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.textBox4, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.textBox2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.textBox1, 0, 3);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 20);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(7, 7, 7, 7);
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(513, 199);
			this.tableLayoutPanel1.TabIndex = 24;
			// 
			// multipleChAns
			// 
			this.multipleChAns.Enabled = false;
			this.multipleChAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.multipleChAns.Location = new System.Drawing.Point(10, 9);
			this.multipleChAns.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.multipleChAns.Name = "multipleChAns";
			this.multipleChAns.Size = new System.Drawing.Size(481, 30);
			this.multipleChAns.TabIndex = 2;
			this.multipleChAns.Click += new System.EventHandler(this.multipleChAns_Click);
			// 
			// textBox4
			// 
			this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.textBox4.Location = new System.Drawing.Point(10, 69);
			this.textBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(481, 30);
			this.textBox4.TabIndex = 3;
			this.textBox4.Click += new System.EventHandler(this.multipleChAns_Click);
			// 
			// textBox2
			// 
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.textBox2.Location = new System.Drawing.Point(10, 39);
			this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(481, 30);
			this.textBox2.TabIndex = 1;
			this.textBox2.Click += new System.EventHandler(this.multipleChAns_Click);
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.textBox1.Location = new System.Drawing.Point(10, 99);
			this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(481, 30);
			this.textBox1.TabIndex = 0;
			this.textBox1.Click += new System.EventHandler(this.multipleChAns_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.tableLayoutPanel1);
			this.panel1.Location = new System.Drawing.Point(851, 79);
			this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(555, 284);
			this.panel1.TabIndex = 25;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label8.Location = new System.Drawing.Point(17, 0);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(138, 25);
			this.label8.TabIndex = 26;
			this.label8.Text = "Atbilžu varianti";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label9.Location = new System.Drawing.Point(15, 0);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(72, 25);
			this.label9.TabIndex = 28;
			this.label9.Text = "Atbilde";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.richTextBox3);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Location = new System.Drawing.Point(11, 567);
			this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1413, 143);
			this.panel2.TabIndex = 29;
			// 
			// richTextBox3
			// 
			this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.richTextBox3.Location = new System.Drawing.Point(15, 28);
			this.richTextBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.richTextBox3.Name = "richTextBox3";
			this.richTextBox3.Size = new System.Drawing.Size(1395, 75);
			this.richTextBox3.TabIndex = 27;
			this.richTextBox3.Text = "";
			this.richTextBox3.Click += new System.EventHandler(this.multipleChAns_Click);
			this.richTextBox3.TextChanged += new System.EventHandler(this.richTextBox3_TextChanged_1);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label4.Location = new System.Drawing.Point(11, 46);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(1259, 86);
			this.label4.TabIndex = 29;
			this.label4.Text = "Atblide";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label10);
			this.panel3.Controls.Add(this.label11);
			this.panel3.Controls.Add(this.flowLayoutPanel3);
			this.panel3.Controls.Add(this.flowLayoutPanel2);
			this.panel3.Controls.Add(this.flowLayoutPanel1);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Location = new System.Drawing.Point(11, 90);
			this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(477, 219);
			this.panel3.TabIndex = 30;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label10.Location = new System.Drawing.Point(39, 0);
			this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(76, 25);
			this.label10.TabIndex = 31;
			this.label10.Text = "Simboli";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label11.Location = new System.Drawing.Point(193, 0);
			this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(108, 25);
			this.label11.TabIndex = 32;
			this.label11.Text = "Mērvienība";
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(336, 26);
			this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(129, 187);
			this.flowLayoutPanel3.TabIndex = 11;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.button22);
			this.panel5.Controls.Add(this.button3);
			this.panel5.Controls.Add(this.functionBtn);
			this.panel5.Controls.Add(this.button1);
			this.panel5.Controls.Add(this.label6);
			this.panel5.Controls.Add(this.metroComboBox1);
			this.panel5.Controls.Add(this.label3);
			this.panel5.Controls.Add(this.label5);
			this.panel5.Controls.Add(this.subtemaBox);
			this.panel5.Controls.Add(this.SpecificPanel);
			this.panel5.Controls.Add(this.formulaPanel);
			this.panel5.Controls.Add(this.checkBox1);
			this.panel5.Controls.Add(this.richTextBox1);
			this.panel5.Controls.Add(this.diffBtn5);
			this.panel5.Controls.Add(this.panel2);
			this.panel5.Controls.Add(this.diffBtn4);
			this.panel5.Controls.Add(this.panel1);
			this.panel5.Controls.Add(this.diffBtn3);
			this.panel5.Controls.Add(this.diffBtn2);
			this.panel5.Controls.Add(this.diffBtn1);
			this.panel5.Controls.Add(this.richTextBox2);
			this.panel5.Controls.Add(this.theoreticalCheck);
			this.panel5.Controls.Add(this.panel3);
			this.panel5.Controls.Add(this.label7);
			this.panel5.Controls.Add(this.label1);
			this.panel5.Location = new System.Drawing.Point(5, 9);
			this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(1428, 681);
			this.panel5.TabIndex = 34;
			this.panel5.Visible = false;
			this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
			// 
			// button22
			// 
			this.button22.BackColor = System.Drawing.Color.Transparent;
			this.button22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.button22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button22.Location = new System.Drawing.Point(869, 10);
			this.button22.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button22.Name = "button22";
			this.button22.Size = new System.Drawing.Size(119, 60);
			this.button22.TabIndex = 52;
			this.button22.Text = "Konstantes";
			this.button22.UseVisualStyleBackColor = false;
			this.button22.Click += new System.EventHandler(this.button22_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.Transparent;
			this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.Location = new System.Drawing.Point(747, 10);
			this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(119, 60);
			this.button3.TabIndex = 51;
			this.button3.Text = "Grieķu burti";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// functionBtn
			// 
			this.functionBtn.BackColor = System.Drawing.Color.Transparent;
			this.functionBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.functionBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.functionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.functionBtn.Location = new System.Drawing.Point(621, 10);
			this.functionBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.functionBtn.Name = "functionBtn";
			this.functionBtn.Size = new System.Drawing.Size(119, 60);
			this.functionBtn.TabIndex = 50;
			this.functionBtn.Text = "Funkcijas";
			this.functionBtn.UseVisualStyleBackColor = false;
			this.functionBtn.Click += new System.EventHandler(this.functionBtn_Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Transparent;
			this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(499, 10);
			this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(117, 60);
			this.button1.TabIndex = 49;
			this.button1.Text = "Pievienot formulu";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.AddFormula);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(0, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 23);
			this.label6.TabIndex = 0;
			// 
			// metroComboBox1
			// 
			this.metroComboBox1.AutoCompleteCustomSource.AddRange(new string[] {
            "1(easy)\t",
            "2",
            "3(medium)",
            "4",
            "5(hard)"});
			this.metroComboBox1.FormattingEnabled = true;
			this.metroComboBox1.ItemHeight = 24;
			this.metroComboBox1.Items.AddRange(new object[] {
            "1(easy)",
            "2",
            "3(medium)",
            "4",
            "5(hard)"});
			this.metroComboBox1.Location = new System.Drawing.Point(1133, 22);
			this.metroComboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.metroComboBox1.Name = "metroComboBox1";
			this.metroComboBox1.Size = new System.Drawing.Size(280, 30);
			this.metroComboBox1.TabIndex = 48;
			this.metroComboBox1.UseSelectable = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label5.Location = new System.Drawing.Point(13, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(83, 25);
			this.label5.TabIndex = 47;
			this.label5.Text = "Formula";
			this.label5.Click += new System.EventHandler(this.label5_Click);
			// 
			// subtemaBox
			// 
			this.subtemaBox.FormattingEnabled = true;
			this.subtemaBox.Location = new System.Drawing.Point(495, 286);
			this.subtemaBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.subtemaBox.Name = "subtemaBox";
			this.subtemaBox.Size = new System.Drawing.Size(348, 24);
			this.subtemaBox.TabIndex = 45;
			this.subtemaBox.Visible = false;
			// 
			// SpecificPanel
			// 
			this.SpecificPanel.BackColor = System.Drawing.Color.Transparent;
			this.SpecificPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.SpecificPanel.Location = new System.Drawing.Point(863, 278);
			this.SpecificPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.SpecificPanel.Name = "SpecificPanel";
			this.SpecificPanel.Size = new System.Drawing.Size(527, 58);
			this.SpecificPanel.TabIndex = 43;
			// 
			// checkBox1
			// 
			this.checkBox1.Image = global::masterVersion.Properties.Resources.multipleChoice11;
			this.checkBox1.Location = new System.Drawing.Point(1061, 22);
			this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(51, 39);
			this.checkBox1.TabIndex = 40;
			this.toolTip2.SetToolTip(this.checkBox1, "Uzdevums ar atbilžu variantiem");
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// diffBtn5
			// 
			this.diffBtn5.Location = new System.Drawing.Point(1340, 10);
			this.diffBtn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.diffBtn5.Name = "diffBtn5";
			this.diffBtn5.Size = new System.Drawing.Size(65, 60);
			this.diffBtn5.TabIndex = 17;
			this.toolTip3.SetToolTip(this.diffBtn5, "Uzdevuma grūtība");
			this.diffBtn5.UseSelectable = true;
			this.diffBtn5.Visible = false;
			this.diffBtn5.Click += new System.EventHandler(this.diffBtnClick);
			this.diffBtn5.MouseLeave += new System.EventHandler(this.diffBtnLeave);
			this.diffBtn5.MouseHover += new System.EventHandler(this.diffBtnHover);
			// 
			// diffBtn4
			// 
			this.diffBtn4.Location = new System.Drawing.Point(1291, 10);
			this.diffBtn4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.diffBtn4.Name = "diffBtn4";
			this.diffBtn4.Size = new System.Drawing.Size(65, 60);
			this.diffBtn4.TabIndex = 16;
			this.toolTip3.SetToolTip(this.diffBtn4, "Uzdevuma grūtība");
			this.diffBtn4.UseSelectable = true;
			this.diffBtn4.Visible = false;
			this.diffBtn4.Click += new System.EventHandler(this.diffBtnClick);
			this.diffBtn4.MouseLeave += new System.EventHandler(this.diffBtnLeave);
			this.diffBtn4.MouseHover += new System.EventHandler(this.diffBtnHover);
			// 
			// diffBtn3
			// 
			this.diffBtn3.Location = new System.Drawing.Point(1229, 10);
			this.diffBtn3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.diffBtn3.Name = "diffBtn3";
			this.diffBtn3.Size = new System.Drawing.Size(65, 60);
			this.diffBtn3.TabIndex = 15;
			this.toolTip3.SetToolTip(this.diffBtn3, "Uzdevuma grūtība");
			this.diffBtn3.UseSelectable = true;
			this.diffBtn3.Visible = false;
			this.diffBtn3.Click += new System.EventHandler(this.diffBtnClick);
			this.diffBtn3.MouseLeave += new System.EventHandler(this.diffBtnLeave);
			this.diffBtn3.MouseHover += new System.EventHandler(this.diffBtnHover);
			// 
			// diffBtn2
			// 
			this.diffBtn2.Location = new System.Drawing.Point(1180, 10);
			this.diffBtn2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.diffBtn2.Name = "diffBtn2";
			this.diffBtn2.Size = new System.Drawing.Size(65, 60);
			this.diffBtn2.TabIndex = 14;
			this.toolTip3.SetToolTip(this.diffBtn2, "Uzdevuma grūtība");
			this.diffBtn2.UseSelectable = true;
			this.diffBtn2.Visible = false;
			this.diffBtn2.Click += new System.EventHandler(this.diffBtnClick);
			this.diffBtn2.MouseLeave += new System.EventHandler(this.diffBtnLeave);
			this.diffBtn2.MouseHover += new System.EventHandler(this.diffBtnHover);
			// 
			// diffBtn1
			// 
			this.diffBtn1.AutoSize = true;
			this.diffBtn1.Location = new System.Drawing.Point(1131, 10);
			this.diffBtn1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.diffBtn1.Name = "diffBtn1";
			this.diffBtn1.Size = new System.Drawing.Size(65, 60);
			this.diffBtn1.TabIndex = 13;
			this.toolTip3.SetToolTip(this.diffBtn1, "Uzdevuma grūtība");
			this.diffBtn1.UseSelectable = true;
			this.diffBtn1.Visible = false;
			this.diffBtn1.Click += new System.EventHandler(this.diffBtnClick);
			this.diffBtn1.MouseLeave += new System.EventHandler(this.diffBtnLeave);
			this.diffBtn1.MouseHover += new System.EventHandler(this.diffBtnHover);
			// 
			// theoreticalCheck
			// 
			this.theoreticalCheck.Image = ((System.Drawing.Image)(resources.GetObject("theoreticalCheck.Image")));
			this.theoreticalCheck.Location = new System.Drawing.Point(995, 10);
			this.theoreticalCheck.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.theoreticalCheck.Name = "theoreticalCheck";
			this.theoreticalCheck.Size = new System.Drawing.Size(61, 63);
			this.theoreticalCheck.TabIndex = 26;
			this.toolTip1.SetToolTip(this.theoreticalCheck, "Teorētisks uzdevums");
			this.theoreticalCheck.UseVisualStyleBackColor = true;
			this.theoreticalCheck.CheckedChanged += new System.EventHandler(this.theoreticalCheck_CheckedChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
			this.label1.Location = new System.Drawing.Point(15, 338);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(162, 25);
			this.label1.TabIndex = 3;
			this.label1.Text = "Uzdevuma teksts";
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Transparent;
			this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(48, 710);
			this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(420, 60);
			this.button2.TabIndex = 53;
			this.button2.Text = "Aprēķināt";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.computeFormula);
			// 
			// AddImageButton
			// 
			this.AddImageButton.BackColor = System.Drawing.Color.Transparent;
			this.AddImageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.AddImageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.AddImageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.AddImageButton.Location = new System.Drawing.Point(539, 710);
			this.AddImageButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.AddImageButton.Name = "AddImageButton";
			this.AddImageButton.Size = new System.Drawing.Size(420, 60);
			this.AddImageButton.TabIndex = 54;
			this.AddImageButton.Text = "Pievienot attēlu";
			this.AddImageButton.UseVisualStyleBackColor = false;
			this.AddImageButton.Click += new System.EventHandler(this.AddImageButton_Click);
			// 
			// BTTN_addTaskToDB
			// 
			this.BTTN_addTaskToDB.BackColor = System.Drawing.Color.Transparent;
			this.BTTN_addTaskToDB.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.BTTN_addTaskToDB.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.BTTN_addTaskToDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.BTTN_addTaskToDB.Location = new System.Drawing.Point(1013, 710);
			this.BTTN_addTaskToDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.BTTN_addTaskToDB.Name = "BTTN_addTaskToDB";
			this.BTTN_addTaskToDB.Size = new System.Drawing.Size(420, 60);
			this.BTTN_addTaskToDB.TabIndex = 55;
			this.BTTN_addTaskToDB.Text = "Pievienot datubāzei";
			this.BTTN_addTaskToDB.UseVisualStyleBackColor = false;
			this.BTTN_addTaskToDB.Click += new System.EventHandler(this.AddToDatabase);
			// 
			// PievienotUzdevumuMaster
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1435, 772);
			this.Controls.Add(this.BTTN_addTaskToDB);
			this.Controls.Add(this.AddImageButton);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.panel5);
			this.DisplayHeader = false;
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Movable = false;
			this.Name = "PievienotUzdevumuMaster";
			this.Padding = new System.Windows.Forms.Padding(27, 37, 27, 25);
			this.Resizable = false;
			this.Style = MetroFramework.MetroColorStyle.White;
			this.Text = "PievienotUzdevumuMaster";
			this.TransparencyKey = System.Drawing.Color.White;
			this.Load += new System.EventHandler(this.PievienotUzdevumuMaster_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.ResumeLayout(false);

        }

        private void PievienotUzdevumuMaster_Load(object sender, EventArgs e)
        {
            
        }


        #endregion

        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button functionBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button AddImageButton;
        private System.Windows.Forms.Button BTTN_addTaskToDB;
    }
}