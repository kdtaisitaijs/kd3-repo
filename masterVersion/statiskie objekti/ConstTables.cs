﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class ConstTables : Form
    {
        PievienotUzdevumuMaster form;
        TablesAndConstants.tableTypes tableType;
        public ConstTables(PievienotUzdevumuMaster form, TablesAndConstants.tableTypes tableType)
        {
            InitializeComponent();
            this.tableType = tableType;
            this.form = form;
            switch (tableType)
            {
                case TablesAndConstants.tableTypes.blivums:
                    foreach (String key in TablesAndConstants.BlivumaTabula.Keys)
                    {
                        Button density = new Button();
                        String newKey = key;
                        char test = key[key.Length - 1];
                        if (key[key.Length - 1].Equals('a'))
                        {
                            newKey = key.Substring(0, key.Length - 1) + "s";
                        }
                        density.Text = newKey;
                        density.Name = key;
                        flowLayoutPanel1.Controls.Add(density);
                        density.Click += new EventHandler(AddConst);
                    }
                    break;
                case TablesAndConstants.tableTypes.konstantes:
                    foreach (String key in TablesAndConstants.FizikasConst.Keys)
                    {
                        Button costant = new Button();
                        String newKey = key;
                        char test = key[key.Length - 1];                        
                        costant.Text = newKey;
                        costant.Name = key;
                        flowLayoutPanel1.Controls.Add(costant);
                        costant.Click += new EventHandler(AddConst);
                    }
                    break;
                case TablesAndConstants.tableTypes.grieku:
                    foreach(String letter in TablesAndConstants.greekLetters)
                    {
                        Button greekLetter = new Button();  
                        greekLetter.Text = letter;                        
                        flowLayoutPanel1.Controls.Add(greekLetter);
                        greekLetter.Click += new EventHandler(AddConst);
                    }
                    break;
                case TablesAndConstants.tableTypes.funkcijas:
                    foreach (String functionName in TablesAndConstants.functions)
                    {
                        Button function = new Button();
                        function.Text = functionName;
                        function.Name = functionName + "(_)";
                        flowLayoutPanel1.Controls.Add(function);
                        function.Click += new EventHandler(AddConst);
                    }
                    break;
                default:
                    break;
            }
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            VeidotUzdViela.table = null;
            base.OnFormClosing(e);
            
        }
        private void AddConst(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            int value;
            float val;
            switch (tableType)
            {
                case TablesAndConstants.tableTypes.blivums:

                    TablesAndConstants.BlivumaTabula.TryGetValue(btn.Name, out value);
                    form.GetTaskField().Text += btn.Name + " // {ρ=" + value.ToString()+"}";
                    break;
                case TablesAndConstants.tableTypes.konstantes:
                    TablesAndConstants.FizikasConst.TryGetValue(btn.Name, out val);
                    form.GetTaskField().Text += " // {" + btn.Name + "=" + val.ToString() +"}";
                    break;
                case TablesAndConstants.tableTypes.grieku:
                    form.GetCurrentTextField().Text += " " + btn.Text;
                    break;
                case TablesAndConstants.tableTypes.funkcijas:
                    form.GetFormulaField().Text += btn.Name;
                    break;
                default:
                    break;
            }

        }
        

        private void densityTable_Load(object sender, EventArgs e)
        {

        }

       
        //TODO: detect Shift and capitalize greek letters!
        private void ConstTables_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey && tableType.Equals(TablesAndConstants.tableTypes.grieku))
            {
                foreach (Button btn in flowLayoutPanel1.Controls)
                {
                    btn.Text = btn.Text.ToUpper();
                }
            }
        }

        private void ConstTables_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey && tableType.Equals(TablesAndConstants.tableTypes.grieku))
            {
                foreach (Button btn in flowLayoutPanel1.Controls)
                {
                    btn.Text = btn.Text.ToLower();
                }
            }
        }

        private void ConstTables_KeyPress(object sender, KeyPressEventArgs e)
        {
            int o = 0;
        }
    }
}
