﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    class CreatetPdf
    {
        //public enum WhiteSpace { none, grid, lines, blank };
        System.Drawing.Bitmap blank = Properties.Resources.blank;
        System.Drawing.Bitmap linePanelImg = Properties.Resources.line_panel;
        System.Drawing.Bitmap gridPanelImg = Properties.Resources.grid_panel;
        bool Ok = true;
        public CreatetPdf()
        {
            

        }
        //Jazeps
        


        public void Export(string testaVeids, String nosaukums,int varinataNr, List<Uzdevums> TasksInTest, Dictionary<int,int> points,
            String VariationCount, List<Tuple<SubMenuTestGen.WhiteSpace,int>> whiteSpaceTypes,bool ShowSaveDialog,String name= "")
        {
            varinataNr++;
            testaVeids = testaVeids.Trim();
            nosaukums = nosaukums.Trim();
            Random rnd = new Random();
            String date = DateTime.Today.ToShortDateString();
            date = date.Replace(".", "-");
            String folderName = testaVeids + " " + nosaukums;
            if (testaVeids == "" && nosaukums == "")
                folderName = date;

            String fileName = name;
            if (ShowSaveDialog)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.FileName = folderName;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Ok = true;
                    fileName = saveFileDialog1.FileName;
                }else
                {
                    Ok = false;
                }
            }

            if (Ok)
            {
                //Directory.CreateDirectory(saveFileDialog1.FileName);
                Document document = new Document();
                PdfWriter.GetInstance(document, new FileStream(fileName+" "+varinataNr.ToString()+". varinats.pdf", FileMode.Create));
                document.Open();
                TaskSolve ts = new TaskSolve();
                List<String[]> atblides = new List<string[]>();


                var bigFont = FontFactory.GetFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, 12);
                var TextFont = FontFactory.GetFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, 12);
                Paragraph Type = new Paragraph(testaVeids, bigFont);
                Type.Alignment = Element.ALIGN_CENTER;
                Type.Font.Size = 20;
                Paragraph Name = new Paragraph(nosaukums, bigFont);
                Name.Alignment = Element.ALIGN_CENTER;
                Name.Font.Size = 20;

                
                for (int j = 0; j < int.Parse(VariationCount); j++)
                {
                    //PDF generation test code
                    document.NewPage();

                    //Headera izveide                
                    document.Add(Type);
                    document.Add(Name);
                    document.Add(new Paragraph((j + 1).ToString() + ". Variants", bigFont));
                    var bigFont2 = FontFactory.GetFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, 14);

                    document.Add(new Paragraph("Klase\nVards             Uzvards\n", bigFont2));

                    //Uzdevumu pievienošana                
                    int i = 1;
                    String[] atb = new String[TasksInTest.Count];
                    foreach (Uzdevums uzd in TasksInTest)
                    {
                        string UzdText = i.ToString() + ". Uzdevums";
                        if (points.ContainsKey(i))
                        {
                            UzdText += "                                                                   "+points[i].ToString();
                            if(points[i] == 1)
                                UzdText+= " punkts";
                            else
                                UzdText += " punkti";
                        }


                        document.Add(new Paragraph(UzdText, bigFont));
                        string taskText = "";
                        try
                        {
                            Tuple<String, String> result = ts.computeFormula(uzd);
                            atb[i - 1] = result.Item2;
                            taskText = result.Item1;

                        }
                        catch
                        {
                            taskText = ts.generatePlainText(uzd.getText(), false);
                            if (uzd.getAnswer() != null)
                                atb[i - 1] = uzd.getAnswer();
                            else
                                atb[i - 1] = "No answer";
                        }
                        document.Add(new Paragraph(taskText, TextFont));
                        //Add whitespace
                        if (uzd.getIsMultChoice())
                        {
                            String[] multAtblides;
                            if (uzd.getIsTheory())
                            {
                                multAtblides = uzd.getMultChoices();
                            }
                            else
                            {
                                multAtblides = new String[4];
                                int correctAns = rnd.Next(0, 4);
                                multAtblides[correctAns] = atb[i - 1].ToString();
                                for(int k = 0; k < 4; k++)
                                {
                                    if (k != correctAns)
                                    {
                                        String ToAdd;
                                        do
                                        {
                                             ToAdd= (float.Parse(multAtblides[correctAns]) + (int)(((double)rnd.Next(1, 10) - 5f) * float.Parse(multAtblides[correctAns]) / 5)).ToString();

                                        }while (multAtblides.Contains(ToAdd));
                                        multAtblides[k] = ToAdd;
                                    }
                                }
                                atb[i-1] = ((char)('a' + (char)((correctAns ) % 26))).ToString();
                                int o = 0;
                            }
                            Paragraph multCh = new Paragraph();
                            try
                            {
                                multCh.Add("\na) " + multAtblides[0]);
                                multCh.Add("\nb) " + multAtblides[1]);
                                multCh.Add("\nc) " + multAtblides[2]);
                                multCh.Add("\nd) " + multAtblides[3]);
                            }
                            catch { }
                            document.Add(multCh);
                            i++;
                        }
                        else
                        {

                            iTextSharp.text.Image pic = null;
                            if (whiteSpaceTypes[i - 1].Item1 == SubMenuTestGen.WhiteSpace.lines)
                            {
                                pic = iTextSharp.text.Image.GetInstance(linePanelImg, System.Drawing.Imaging.ImageFormat.Jpeg);

                            }
                            if (whiteSpaceTypes[i - 1].Item1 == SubMenuTestGen.WhiteSpace.grid)
                            {
                                pic = iTextSharp.text.Image.GetInstance(gridPanelImg, System.Drawing.Imaging.ImageFormat.Jpeg);
                                pic.Border = iTextSharp.text.Rectangle.BOX;
                                pic.BorderColor = iTextSharp.text.BaseColor.BLACK;
                                pic.BorderWidth = 2f;

                            }
                            if (whiteSpaceTypes[i - 1].Item1 == SubMenuTestGen.WhiteSpace.blank)
                            {
                                pic = iTextSharp.text.Image.GetInstance(blank, System.Drawing.Imaging.ImageFormat.Jpeg);

                            }
                            if (pic != null)
                            {
                                pic.ScaleAbsolute(500, 70*whiteSpaceTypes[i-1].Item2);
                                document.Add(pic);
                            }
                            i++;
                        }
                    }
                    atblides.Add(atb);

                }
                document.Close();
                

                //Atbilžu lapas izveide
                Document document2 = new Document();
                PdfWriter.GetInstance(document2, new FileStream(fileName + " atbildes "
                    + varinataNr.ToString()+". variants.pdf", FileMode.Create));
                document2.Open();
                Paragraph atbText = new Paragraph("ATBILŽU LAPA\n"+ varinataNr.ToString()+ ". variants" , bigFont);
                atbText.Alignment = Element.ALIGN_CENTER;
                document2.Add(atbText);
                document2.Add(Type);
                document2.Add(Name);
                document2.Add(new Paragraph("\n"));

                PdfPTable answerTable = new PdfPTable(atblides.Count + 1);

                answerTable.AddCell("");
                PdfPCell tableTitle = new PdfPCell(new Phrase("Variants", TextFont));
                tableTitle.HorizontalAlignment = Element.ALIGN_CENTER;
                tableTitle.Colspan = atblides.Count;
                answerTable.AddCell(tableTitle);

                PdfPCell uzdTitle = new PdfPCell(new Phrase("Uzd", TextFont));
                uzdTitle.HorizontalAlignment = Element.ALIGN_CENTER;
                answerTable.AddCell(uzdTitle);

                for (int i = 0; i < atblides.Count; i++)
                {
                    String num = (i + 1).ToString() + ".";
                    PdfPCell cell = new PdfPCell(new Phrase(num, TextFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    answerTable.AddCell(cell);
                }

                for (int i = 0; i < TasksInTest.Count; i++)
                {
                    PdfPCell cell2 = new PdfPCell(new Phrase((i + 1).ToString() + ".", TextFont));
                    cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    answerTable.AddCell(cell2);
                    for (int j = 0; j < atblides.Count; j++)
                    {



                        if (atblides[j][i] == "No answer" || TasksInTest[i].getIsTheory() == true)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(atblides[j][i], TextFont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.Colspan = atblides.Count;
                            answerTable.AddCell(cell);
                            break;

                        }
                        else
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(atblides[j][i], TextFont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            answerTable.AddCell(cell);
                        }

                    }

                }
                document2.Add(answerTable);
                document2.Close();
                
                /*MessageBoxManager.Register();
                DialogResult result1 = MessageBox.Show("Kontroldarbi veiksmīgi eksportēti.",
                "Kontroldarbu eksports", MessageBoxButtons.OK);

                if (result1 == DialogResult.Yes)
                {
                    Process.Start(@saveFileDialog1.FileName);
                }
                MessageBoxManager.Unregister();
                */
            }
        }
        //!Jazeps
    }
}
