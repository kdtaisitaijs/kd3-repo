﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Xml.Linq;


namespace masterVersion
{
    public class page
    {
        //page stuff
        Panel pageScrollPanel;
        TableLayoutPanel newPage;
        TableLayoutPanel header;
        ComboBox testaVeids;
        public List<Uzdevums> TasksInTest = new List<Uzdevums>();
        string modified;
        int taskCount = 1;
        int currentTaskNo;
        int pageRowNo;
        //ToDo TaskPanel vajag savu klasi
        List<TableLayoutPanel> taskPanelList = new List<TableLayoutPanel>();
        List<String> tempTaskTextList = new List<String>();
        List<RichTextBox> taskTextList = new List<RichTextBox>();
        List<Panel> panelList = new List<Panel>();
        public List<Tuple<SubMenuTestGen.WhiteSpace, int>> whiteSpace = new List<Tuple<SubMenuTestGen.WhiteSpace, int>>();
        TableLayoutPanel taskPanel;
        Label uzdevumaNo;
        Label punktuSk;
        TextBox punktuBox;
        TableLayoutPanel uzdevumaNoPanel;
        TableLayoutPanel buttonPanel;
        RichTextBox taskText;
        Panel taskBlankPanel;
        FlowLayoutPanel tootip;
        int PageRow = 3;
        int pageRowHeight = 56;
        int headerCellHeight = 25;
        int headerCellWidth = 134;
        public string Tema;
        public string TestaVeids = "Tests";
        TaskSolve taskSolve = new TaskSolve();
        int points;
        TextBox iegutiP = new TextBox();
        public Dictionary<int, int> puntkiUzdevumos = new Dictionary<int, int>();
        int variants;
        int sum;
        int uzdevumi = 1;
        dialog_main crahsDialog = new dialog_main();

        public Panel createPage(int clientSizeWidth, int clientSizeHeight, string tema, int klase, int variantuSkaits, float scale, FlowLayoutPanel _tooTip)
        {

            pageScrollPanel = new Panel();
            tootip = _tooTip;
            newPage = new TableLayoutPanel();
            newPage.AutoSize = true;
          
            variants = variantuSkaits + 1;
            //TODO: find right hieght
            newPage.MinimumSize = new Size(680, clientSizeHeight);
            //newPage.Size = new Size(680, clientSizeHeight);

            newPage.BackColor = Color.White;
            pageScrollPanel.Size = new Size(newPage.Width + 10, clientSizeHeight);
            pageScrollPanel.BorderStyle = BorderStyle.FixedSingle;
            pageScrollPanel.Location = new Point(
            (clientSizeWidth / 3 + (clientSizeWidth - (2 * clientSizeWidth / 3) - pageScrollPanel.Width / 2)+20), _tooTip.Height + 20);

            newPage.Location = new Point((pageScrollPanel.Width - newPage.Width) / 2, 5);
            pageScrollPanel.Anchor = AnchorStyles.None;
            pageScrollPanel.BackColor = Color.White;
            pageScrollPanel.AutoScroll = true;

            this.createHeader(tema, klase);

            pageScrollPanel.Controls.Add(newPage);
            return pageScrollPanel;
        }


        private void createHeader(string tema, int klase)
        {
            Tema = tema;
            header = new TableLayoutPanel();
            TableLayoutRowStyleCollection stylesHeader = header.RowStyles;
            foreach (RowStyle style in stylesHeader)
            {
                style.SizeType = SizeType.Absolute;
                style.Height = 25;
            }

            TableLayoutColumnStyleCollection stylesHeaderCol = header.ColumnStyles;
            foreach (ColumnStyle style in stylesHeaderCol)
            {
                style.SizeType = SizeType.Absolute;
                style.Width = 34;
            }

            header.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
            header.BackColor = Color.White;
            header.ColumnCount = 5;
            header.RowCount = 8;

            header.Size = new Size(680, 180);
            testaVeids = new MetroFramework.Controls.MetroComboBox();
            testaVeids.DropDownStyle = ComboBoxStyle.DropDownList;
            testaVeids.DrawMode = DrawMode.OwnerDrawFixed;
            testaVeids.DrawItem += new System.Windows.Forms.DrawItemEventHandler(ComboBoxCenter);
            testaVeids.MaxLength = 36;
            testaVeids.Items.Add(("                                              Tests"));
            testaVeids.Items.Add(("                                              Ieskaite"));
            testaVeids.Items.Add(("                                           Klases darbs"));
            testaVeids.SelectedIndex = 0;
            testaVeids.TextChanged += TestaVeids_TextChanged;
            testaVeids.SelectedValueChanged += TestaVeids_TextChanged;
            testaVeids.Size = new Size(3 * headerCellWidth, 2 * headerCellHeight);
            testaVeids.Font = new System.Drawing.Font(testaVeids.Font.Name, testaVeids.Font.Size + 10, testaVeids.Font.Style, testaVeids.Font.Unit);

            header.Controls.Add(testaVeids, 1, 0);
            header.SetColumnSpan(testaVeids, 3);
            header.SetRowSpan(testaVeids, 2);

            TextBox variantsText = new TextBox();
            variantsText.Width = 3 * headerCellWidth;
            variantsText.Text = variants.ToString() + ". Variants";
            variantsText.Font = new System.Drawing.Font(variantsText.Font.Name, variantsText.Font.Size + 5, variantsText.Font.Style, variantsText.Font.Unit);
            variantsText.TextAlign = HorizontalAlignment.Center;
            variantsText.BorderStyle = BorderStyle.None;
            variantsText.ReadOnly = true;
            variantsText.BackColor = Color.White;

            header.Controls.Add(variantsText, 1, 4);
            header.SetColumnSpan(variantsText, 3);


            header.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            TextBox kdTema = new TextBox();
            kdTema.MaxLength = 45;
            kdTema.Font = new System.Drawing.Font(testaVeids.Font.Name, testaVeids.Font.Size + 5, testaVeids.Font.Style, testaVeids.Font.Unit);
            kdTema.BorderStyle = BorderStyle.FixedSingle;
            kdTema.BackColor = Color.White;
            kdTema.Size = new Size(3 * headerCellWidth, headerCellHeight);
            if (tema != null)
            {
                kdTema.Text = "\"" + tema + "\"";
                if (kdTema.Text.Length > 36)
                {
                    kdTema.Font = new System.Drawing.Font(testaVeids.Font.Name, testaVeids.Font.Size - 3, testaVeids.Font.Style, testaVeids.Font.Unit);
                }
                kdTema.TextAlign = HorizontalAlignment.Center;
                kdTema.BorderStyle = BorderStyle.None;
            }

            TextBox klaseText = new TextBox();
            klaseText.Size = new Size(headerCellWidth, headerCellHeight);
            klaseText.Text = klase.ToString() + "____ klase";
            klaseText.BorderStyle = BorderStyle.None;
            klaseText.BackColor = Color.White;
            klaseText.Font = new System.Drawing.Font(klaseText.Font.Name, klaseText.Font.Size + 3, klaseText.Font.Style, klaseText.Font.Unit);
            klaseText.ReadOnly = true;
            TextBox vards = new TextBox();
            vards.Size = new Size(headerCellWidth, headerCellHeight);
            vards.Text = "Vārds";
            vards.BorderStyle = BorderStyle.None;
            vards.BackColor = Color.White;
            vards.Font = klaseText.Font;
            vards.ReadOnly = true;
            TextBox uzvards = new TextBox();
            uzvards.Size = new Size(headerCellWidth, headerCellHeight);
            uzvards.Text = "Uzvārds";
            uzvards.BorderStyle = BorderStyle.None;
            uzvards.BackColor = Color.White;
            uzvards.Font = klaseText.Font;
            uzvards.ReadOnly = true;

            iegutiP = new TextBox();
            iegutiP.BorderStyle = BorderStyle.FixedSingle;
            iegutiP.ReadOnly = true;
            iegutiP.BackColor = Color.White;
            iegutiP.Font = klaseText.Font;

            Label pMax = new Label();
            pMax.Text = "No";
            pMax.Font = klaseText.Font;
            pMax.TextAlign = ContentAlignment.MiddleRight;
            Label pGot = new Label();
            pGot.Text = "Iegūti";
            pGot.Font = klaseText.Font;
            pGot.TextAlign = ContentAlignment.MiddleRight;
            Label lblMax = new Label();
            lblMax.Text = "punkti.";
            lblMax.Font = klaseText.Font;
            Label lblgot = new Label();
            lblgot.Text = "punkti.";
            lblgot.Font = klaseText.Font;

            header.Controls.Add(iegutiP, 3, 5);

            header.Controls.Add(pMax, 2, 5);
            header.Controls.Add(pGot, 2, 6);

            header.Controls.Add(lblMax, 4, 5);
            header.Controls.Add(lblgot, 4, 6);

            header.Controls.Add(klaseText, 0, 4);
            header.Controls.Add(vards, 0, 5);
            header.Controls.Add(uzvards, 0, 6);

            header.Controls.Add(kdTema, 1, 2);
            header.SetColumnSpan(kdTema, 3);
            header.SetRowSpan(kdTema, 2);

            newPage.Controls.Add(header, 0, 0);
            newPage.SetRowSpan(header, 3);
        }

        private void ComboBoxCenter(object sender, DrawItemEventArgs e)
        {
            helpers.centerComboBox.cbxDesign_DrawItem(sender, e);
        }


        private string modifyString(string _toModify)
        {
            modified = null;
            int lenght = _toModify.Length;
            int toMid = (testaVeids.MaxLength - lenght) / 2;

            for (int i = 0; i < toMid; i++)
            {
                modified += "  ";
            }
            return " " + modified + _toModify;
        }
        private void TestaVeids_TextChanged(object sender, EventArgs e)
        {
            TestaVeids = testaVeids.Text;
        }
        public void addUzdevums(Uzdevums pievienots)
        {
            createTask(pievienots);
        }
        private void createTask(Uzdevums _uzdevumsTest)
        {
            TasksInTest.Add(_uzdevumsTest);

            taskPanelList.Add(taskPanel = new TableLayoutPanel());
            taskPanel.Name = taskCount.ToString();
            whiteSpace.Add(new Tuple<SubMenuTestGen.WhiteSpace, int>(SubMenuTestGen.WhiteSpace.blank, 1));
            uzdevumaNo = new Label();
            uzdevumaNo.Text = taskPanelList.Count() + ". Uzdevums.";
            uzdevumaNo.Font = new System.Drawing.Font(uzdevumaNo.Font.Name, uzdevumaNo.Font.Size + 4, uzdevumaNo.Font.Style, uzdevumaNo.Font.Unit);
            uzdevumaNo.Size = new Size(440, 40);
            uzdevumaNo.BorderStyle = BorderStyle.None;
            uzdevumaNo.BackColor = Color.White;

            punktuSk = new Label();
            punktuSk.Text = "punkti";
            punktuSk.Font = uzdevumaNo.Font;

            punktuBox = new TextBox();
            punktuBox.Size = new Size(40, 40);
            punktuBox.BorderStyle = BorderStyle.FixedSingle;
            punktuBox.Name = taskCount.ToString();
            punktuBox.TextChanged += TextChanged;


           // taskPanel.Size = new Size(newPage.Width, (pageRowHeight * 5) - 30);
            taskPanel.Size = new Size(700, (pageRowHeight * 5) - 30);

            taskPanel.BackColor = Color.White;
            taskPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;


            uzdevumaNoPanel = new TableLayoutPanel();
            uzdevumaNoPanel.Size = new Size(600, 30);
            uzdevumaNoPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
            uzdevumaNoPanel.SetColumnSpan(uzdevumaNo, 5);
            //uzdevumaNoPanel.BackColor = Color.Red;

            buttonPanel = new TableLayoutPanel();
            buttonPanel.Size = new Size(50, 250);

            Button up = new Button();
            up.Name = taskPanelList.Count().ToString();
            up.Tag = PageRow;
            up.Size = new Size(35, 35);
            up.Image = Properties.Resources.up_icon;
            up.Click += Up_Click;

            Button down = new Button();
            down.Size = new Size(35, 35);
            down.Name = taskPanelList.Count().ToString();
            down.Tag = PageRow;
            down.Image = Properties.Resources.down_icon;
            down.Click += Down_Click;

            Button remove = new Button();
            remove.Size = new Size(35, 35);
            remove.Name = taskPanelList.Count().ToString();
            remove.Image = Properties.Resources.rem_icon;
            remove.Click += Remove_Click;

            Button moreSpace = new Button();
            moreSpace.Size = new Size(35, 35);
            moreSpace.Name = taskPanelList.Count().ToString();
            moreSpace.Click += MoreSpace_Click;
            moreSpace.Image = Properties.Resources.increase_size_option;

            Button lessSpace = new Button();
            lessSpace.Size = new Size(35, 35);
            lessSpace.Name = taskPanelList.Count().ToString();
            lessSpace.Click += LessSpace_Click;
            lessSpace.Image = Properties.Resources.resize_option;

            Button addPicture = new Button();
            addPicture.Size = new Size(35, 35);
            addPicture.Image = Properties.Resources.add_icon;
            addPicture.Click += AddPicture_Click;

            buttonPanel.Controls.Add(up, 0, 0);
            buttonPanel.Controls.Add(down, 0, 1);
            buttonPanel.Controls.Add(remove, 0, 2);
            buttonPanel.Controls.Add(moreSpace, 0, 3);
            buttonPanel.Controls.Add(lessSpace, 0, 4);
            //buttonPanel.Controls.Add(addPicture, 0, 5);

            uzdevumaNoPanel.Controls.Add(uzdevumaNo, 0, 0);
            uzdevumaNoPanel.Controls.Add(punktuSk, 10, 0);
            uzdevumaNoPanel.Controls.Add(punktuBox, 5, 0);

            taskTextList.Add(taskText = new RichTextBox());
            taskText.Name = taskCount.ToString();


            taskText.BackColor = Color.White;

            taskText.Font = new System.Drawing.Font(taskText.Font.Name, taskText.Font.Size + 4, taskText.Font.Style, taskText.Font.Unit);
            taskText.Text = taskSolve.generatePlainText(_uzdevumsTest.getText(), false);
            taskText.TextChanged += TaskText_TextChanged;
            int test = taskText.Text.Length;

            if (test > 200 &&  test < 300)
            {
                taskText.Size = new Size(600, 75);
            }
            else if(test > 300 && test < 400)
            {
                taskText.Size = new Size(600, 110);
                taskPanel.Height = taskPanel.Height + 35;
            }
            else if (taskText.Text.Length > 400)
            {
                taskText.Size = new Size(600, 130);
                taskPanel.Height = taskPanel.Height + 55;
            }
            else
            {
                taskText.Size = new Size(600, 50);
            }
  
            panelList.Add(taskBlankPanel = new Panel());
            taskBlankPanel.Size = new Size(600, 125);
            taskBlankPanel.BorderStyle = BorderStyle.FixedSingle;
            taskBlankPanel.BackColor = Color.White;

            PictureBox pictureSpace_1 = new PictureBox();
            pictureSpace_1.Size = new Size(360, 125);
            pictureSpace_1.BackColor = Color.Red;
            pictureSpace_1.Visible = false;

            PictureBox pictureSpace_2 = new PictureBox();
            pictureSpace_2.Size = new Size(360, 125);
            pictureSpace_2.BackColor = Color.Blue;
            pictureSpace_2.Visible = false;

            taskPanel.SetRowSpan(taskText, 2);
            taskPanel.SetColumnSpan(taskText, 11);

            taskPanel.Controls.Add(uzdevumaNoPanel, 0, 0);
            taskPanel.SetColumnSpan(uzdevumaNoPanel, 11);

            taskPanel.Controls.Add(taskText, 0, 1);
            taskPanel.Controls.Add(buttonPanel, 0, 2);


            taskPanel.SetRowSpan(taskBlankPanel, 2);
            taskPanel.SetColumnSpan(taskBlankPanel, 11);
            taskPanel.Controls.Add(taskBlankPanel, 0, 3);
            taskPanel.Controls.Add(buttonPanel, 11, 0);

            taskPanel.SetRowSpan(buttonPanel, 4);
            // taskPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;


            newPage.Controls.Add(taskPanel, 0, PageRow);
            PageRow++;
            taskCount++;


        }

        private void TaskText_TextChanged(object sender, EventArgs e)
        {
            RichTextBox Richtext = sender as RichTextBox;
            int number = int.Parse(Richtext.Name);
            if (taskPanelList.Count > (number - 1))
            {
                if (Richtext.Text.Length > 200 && Richtext.Text.Length < 300)
                {
                    Richtext.Size = new Size(600, 75);
                    if (taskPanelList.ElementAt(number - 1).Height != 250)
                    {
                        taskPanelList.ElementAt(number - 1).Height = 250;
                    }
                }
                else if (Richtext.Text.Length > 300 && Richtext.Text.Length < 400)
                {
                    Richtext.Size = new Size(600, 110);
                    if (taskPanelList.ElementAt(number - 1).Height != 285)
                    {
                        taskPanelList.ElementAt(number - 1).Height = 285;
                    }
                }
                else if (Richtext.Text.Length > 400)
                {
                    Richtext.Size = new Size(600, 130);

                    if(taskPanelList.ElementAt(number - 1).Height != 320)
                    { 
                        taskPanelList.ElementAt(number - 1).Height = 320;
                    }
                }
                else
                {
                Richtext.Size = new Size(600, 50);

                    if (taskPanelList.ElementAt(number - 1).Height != 250)
                    {
                        taskPanelList.ElementAt(number - 1).Height = 250;
                    }
                }
            }
        }

        private void LessSpace_Click(object sender, EventArgs e)
        {
            Button lessClick = sender as Button;
            int itemId = int.Parse(lessClick.Name);

            if (taskPanelList.ElementAt(itemId - 1).Height > 250)
            {
                whiteSpace[itemId - 1] = new Tuple<SubMenuTestGen.WhiteSpace, int>(whiteSpace[itemId - 1].Item1, whiteSpace[itemId - 1].Item2 - 1);
                taskPanelList.ElementAt(itemId - 1).Height += -100;
                panelList.ElementAt(itemId - 1).Height += -100;
            }
            else
            {
                MessageBox.Show("Mazākais izmērs");
            }
        }

        private void AddPicture_Click(object sender, EventArgs e)
        {
            Button addClick = sender as Button;
            int itemId = int.Parse(addClick.Name);

            taskPanelList.ElementAt(itemId - 1).Height += 100;
            panelList.ElementAt(itemId - 1).Height += 100;
        }

        private void MoreSpace_Click(object sender, EventArgs e)
        {
            Button moreClick = sender as Button;
            int itemId = int.Parse(moreClick.Name);
            whiteSpace[itemId - 1] = new Tuple<SubMenuTestGen.WhiteSpace, int>(whiteSpace[itemId - 1].Item1, whiteSpace[itemId - 1].Item2 + 1);
            taskPanelList.ElementAt(itemId - 1).Height += 100;
            panelList.ElementAt(itemId - 1).Height += 100;

        }

        private void Remove_Click(object sender, EventArgs e)
        {
            Button remClick = sender as Button;
            int itemId = int.Parse(remClick.Name);

            if (taskPanelList.Count > (itemId - 1))
            {
                for (int i = itemId; i < taskTextList.Count; i++)
                {
                    for (int k = 0; k < taskTextList.Count; k++)
                    {
                        tempTaskTextList.Add(taskTextList.ElementAt(k).Text);
                    }
                    taskTextList.ElementAt(i - 1).Text = tempTaskTextList.ElementAt(i);
                    taskTextList.ElementAt(i).Text = tempTaskTextList.ElementAt(i - 1);
                    tempTaskTextList.Clear();
                }
                taskTextList.RemoveAt(taskTextList.Count - 1);
                newPage.Controls.Remove(taskPanelList.ElementAt(taskTextList.Count));
                TasksInTest.RemoveAt(int.Parse(remClick.Name) - 1);
                whiteSpace.RemoveAt(int.Parse(remClick.Name) - 1);
                taskPanelList.RemoveAt(int.Parse(remClick.Name) - 1);
            }
            else
            {
                this.doCrashdialog();
            }
        }
        private void SwapTasks(int index1, int index2)
        {     
            if (index1 < TasksInTest.Count && index2 < TasksInTest.Count)
            {
                if (index2 < 0 || index1 < 0)
                    return;
                Uzdevums tempTask = TasksInTest[index1];
                var tempTuple = whiteSpace[index1];

                //whiteSpace[index1] = whiteSpace[index2];
                //whiteSpace[index2] = tempTuple;

                string text = taskPanelList.ElementAt(index1).Controls.ToString();
                taskPanelList.ElementAt(index1).ToString();

                TasksInTest[index1] = TasksInTest[index2];
                TasksInTest[index2] = tempTask;
            }
            else
            {
                this.doCrashdialog();
            }
        }
        private void Down_Click(object sender, EventArgs e)
        {
            Button downClick = sender as Button;
            int itemId = int.Parse(downClick.Name);
            //TODO : Swap whitespaces!
            if (taskPanelList.Count > (itemId - 1))
            { 
                if (itemId == TasksInTest.Count)
                    return;

                for (int k = 0; k < taskTextList.Count; k++)
                {
                    tempTaskTextList.Add(taskTextList.ElementAt(k).Text);
                }

                taskTextList.ElementAt(itemId - 1).Text = tempTaskTextList.ElementAt(itemId);
                taskTextList.ElementAt(itemId).Text = tempTaskTextList.ElementAt(itemId - 1);
                tempTaskTextList.Clear();
                SwapTasks(itemId, itemId - 1);
            }
            else
            {
                this.doCrashdialog();
            }
        }

        private void doCrashdialog()
        {
            MessageBox.Show("Ir noticis kaut kas negaidīts.Programma nespēj to apstrādāt.Ziņojums ir nostūtīts Ingenium Drīz ar to tiksim galā." + "/r/n" + "Paldies par sapratni!");
        }

        private void Up_Click(object sender, EventArgs e)
        {
            Button upClick = sender as Button;
            int itemId = int.Parse(upClick.Name);
            if (taskPanelList.Count > (itemId - 1))
            {
                if (itemId == 1)
                    return;
                for (int k = 0; k < taskTextList.Count; k++)
                {
                    tempTaskTextList.Add(taskTextList.ElementAt(k).Text);
                }

                taskTextList.ElementAt(itemId - 1).Text = tempTaskTextList.ElementAt(itemId - 2);
                taskTextList.ElementAt(itemId - 2).Text = tempTaskTextList.ElementAt(itemId - 1);
                tempTaskTextList.Clear();
                SwapTasks(itemId - 1, itemId - 2);
            }
            else
            {
                this.doCrashdialog(); 
            }
        }

        private void TextChanged(object sender, EventArgs e)
        {
            //ToDO needs fixing - nevar erti 10p parveidot uz 20p met error 
            TextBox punkti = sender as TextBox;
            int uzdP = int.Parse(punkti.Name.ToString());
            int test;
            if (punkti.Text != "")
            {
                if (int.TryParse(punkti.Text, out points) == false || int.Parse(punkti.Text) == 0)
                {
                    MessageBox.Show("Uzdevumam nevar būt 0 punktu");
                }
                else
                {
                    if (puntkiUzdevumos.TryGetValue(uzdP, out test) == false)
                    {
                        puntkiUzdevumos.Add(uzdP, points);
                    }
                    else
                    {
                        puntkiUzdevumos.Remove(uzdP);
                        puntkiUzdevumos.Add(uzdP, points);
                    }
                }
            }
            iegutiP.Text = this.getSumofPoints().ToString();
        }

        private int getSumofPoints()
        {
            sum = 0;
            for (int i = 0; i < puntkiUzdevumos.Count; i++)
            {
                if (puntkiUzdevumos.ContainsKey(i + 1))
                {
                    sum += puntkiUzdevumos[i + 1];
                }
            }
            return sum;
        }

    }
}
