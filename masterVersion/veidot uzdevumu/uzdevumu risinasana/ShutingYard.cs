﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion
{

	//==================================================================================================
	//==================================================================================================
	//==================================================================================================
	//==================================================================================================
	public abstract class ShuntingYardAlgorithm
	{
		public struct PrecedensAssociativity
		{
			public PrecedensAssociativity(int p, Asso a)
			{
				Prec = p;
				Associativity = a;
			}
			public int Prec;
			public enum Asso { Left, Right };
			public Asso Associativity;
		}
		private static Dictionary<string, PrecedensAssociativity> operators = new Dictionary<string, PrecedensAssociativity>()
		{
			//Why does it start with 2 !??
			{ "+", new PrecedensAssociativity(2,PrecedensAssociativity.Asso.Left)},
			{ "-", new PrecedensAssociativity(2,PrecedensAssociativity.Asso.Left)},
			{ "*", new PrecedensAssociativity(3,PrecedensAssociativity.Asso.Left)},
			{ "/", new PrecedensAssociativity(3,PrecedensAssociativity.Asso.Left)},
			{ "^", new PrecedensAssociativity(4,PrecedensAssociativity.Asso.Right)}
		};

		//TODO find ont if this kind of storage is needed, might work with something simpler
		private static Dictionary<string, PrecedensAssociativity> functions = new Dictionary<string, PrecedensAssociativity>()
		{
			{ "sin", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "cos", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "acos", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "asin", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "tan", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "atan", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},

			{ "sqrt", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "abs", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
	        { "exp", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},

			{ "log", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "log10", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)},
			{ "log2", new PrecedensAssociativity(5,PrecedensAssociativity.Asso.Right)}
		};

		static public List<string> GetStacks(List<string> InputList)
		{
			Stack<string> reversePolish = new Stack<string>(); // output queue in reverse polsih notation
			Stack<string> operatorStack = new Stack<string>(); // operator stack
			List<string> variables = new List<string>();//all found variables

			foreach (string s in InputList)
			{

				if (IsOperator(s))
				{
					while (operatorStack.Count > 0)
					{
						string ot = operatorStack.Peek();
						// if ot is operator && o < ot
						if (IsOperator(ot) && (
							(Association(s) == PrecedensAssociativity.Asso.Left && Precedence(s, ot) <= 0) ||
							(Association(s) == PrecedensAssociativity.Asso.Right && Precedence(s, ot) < 0))
							)
							reversePolish.Push(operatorStack.Pop()); // stack to output
						else
							break;
					}
					operatorStack.Push(s);
				}
				else if (isFunction(s))
				{
					operatorStack.Push(s);
				}
				else if (s.Equals("("))
				{
					operatorStack.Push("(");
				}
				else if (s.Equals(")"))
				{
					bool pe = false;
					while (operatorStack.Count > 0)// opr to out until (
					{
						string sc = operatorStack.Peek();
						if (sc.Equals("("))
						{
							pe = true;
							break;
						}
						else
							reversePolish.Push(operatorStack.Pop());
					}
					if (!pe) throw new Exception("Incorrect parentheses");
					operatorStack.Pop(); // pop off (
					try
					{
						if (isFunction(operatorStack.Peek()))//if next operator is function add it to reverse polish
							reversePolish.Push(operatorStack.Pop());
					}
					catch (Exception e) { }
				}

				else if (s.Equals(","))//function argument seperator
				{
					bool pe = false;
					while (operatorStack.Count > 0)// opr to out until (
					{
						string sc = operatorStack.Peek();
						if (sc.Equals("("))
						{
							pe = true;
							break;
						}
						else
							reversePolish.Push(operatorStack.Pop());
					}
					if (!pe) throw new Exception("Parentheses or seperator(,) misplaced");

				}
				else //if (IsIdentifier(s))
				{
					reversePolish.Push(s);
					try
					{
						Convert.ToDouble(reversePolish.Peek());
					}
					catch (FormatException e)
					{
						variables.Add(reversePolish.Peek());
					}
				}

			}


			//copy operatorstack to reverse polish
			while (operatorStack.Count > 0)
				reversePolish.Push(operatorStack.Pop());

			List<string> rpn = new List<string>();

			while (reversePolish.Count > 0)
				rpn.Add(reversePolish.Pop());

			
			return rpn;
            //return stack in reverse polish notation
			// return Tuple.Create(inter, opr);
		}



		public static double Calculate(List<string> reversePolishNotation)
		{
			Stack<string> operations = new Stack<string>();
			Stack<double> valueStack = new Stack<double>();
			foreach (string i in reversePolishNotation)
				operations.Push(i);

			while (operations.Count > 0)
			{
				string s = operations.Pop();

				if (IsOperator(s))
				{
					valueStack.Push(Evaluate(valueStack.Pop(), s, valueStack.Pop()));
				}
				else if (isFunction(s))
				{
					//TODO generalize for functions with many argumentrs
					valueStack.Push(Evaluate(s, valueStack.Pop()));
				}
				else
				{
					try
					{
						valueStack.Push(Convert.ToDouble(s));
					}
					catch (FormatException e)
					{
						throw new Exception("Value in stack is neither operator, function or number");
					}
				}

			}
			if (valueStack.Count == 1)
				return valueStack.Peek(); // return result
			else
				throw new Exception("Something is wrong! can not produce single number output");
		}







		public static bool IsOperator(String opr)
		{
			if (operators.ContainsKey(opr))
				return true;
			return false;
		}
		public static bool isFunction(String opr)
		{
			if (functions.ContainsKey(opr))
				return true;
			return false;
		}
		public static PrecedensAssociativity.Asso Association(string opr)
		{
			if (!operators.ContainsKey(opr))
				throw new Exception("Wrong operator!!");
			return operators[opr].Associativity;
		}

		public static int Precedence(string opr1, string opr2)
		{
			if (!operators.ContainsKey(opr1))
				throw new Exception("Wrong operator!!");
			if (!operators.ContainsKey(opr2))
				throw new Exception("Wrong operator!!");
			if (operators[opr1].Prec > operators[opr2].Prec) return 1;
			if (operators[opr1].Prec < operators[opr2].Prec) return -1;
			return 0;
		}

		public static double Evaluate(double result1, string opr, double result2)
		{
			switch (opr)
			{
				case "+":
					return (double)result1 + result2;
				case "-":
					return (double)result2 - result1;
				case "*":
					return (double)result1 * result2;
				case "/":
					return (double)result2 / result1;
				case "^":
					return Math.Pow(result2, result1);
			}
			throw new Exception("Wrong operator!!");
		}

		//TODO generalize this shit for all functions not only 1 argument ones
		public static double Evaluate(string function, double number)
		{
			switch (function)
			{
				case "sin":
					return Math.Sin(number);
				case "cos":
					return Math.Cos(number);
				case "asin":
					return Math.Asin(number);
				case "acos":
					return Math.Acos(number);
				case "tan":
					return Math.Tan(number);
				case "atan":
					return Math.Atan(number);
				case "sqrt":
					return Math.Sqrt(number);
				case "abs":
					return Math.Abs(number);
				case "exp":
					return Math.Exp(number);
				case "log":
					return Math.Log(number);
				case "log10":
					return Math.Log10(number);
				case "log2":
					return Math.Log(number) / Math.Log(2);


			}
			throw new Exception("function does not exsists!");
		}


	}

}
