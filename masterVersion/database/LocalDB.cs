﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion.database
{
    class LocalDB
    {


        string address = "";
        public static List<Uzdevums> DB;
        
        public static void LoadDB()
        {
            string line;
            string[] parts;
            string filePath = System.IO.Path.GetFullPath("Data//out.csv");
            System.IO.StreamReader file =    new System.IO.StreamReader(filePath,System.Text.Encoding.UTF8);
            DB = new List<Uzdevums>();
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    //line = line.Replace("\"", "");
                    Uzdevums uzd = new Uzdevums();
                    parts = line.Split(new string[] { ",\"" }, StringSplitOptions.None);
                    for(int i = 0; i < parts.Length; i++)
                    {
                        parts[i] = parts[i].Replace("\"", "");
                    }
                    int grade = 9;
                    Int32.TryParse(parts[1], out grade);
                    uzd.setGrade(grade);
                    uzd.setTopic(parts[2]);
                    uzd.setText(parts[3]);
                    int diff = 0;
                    Int32.TryParse(parts[4], out diff);
                    uzd.setDifficulty(diff);
                    uzd.parseEquation(parts[5]);
                    uzd.parseMultChoices(parts[6]);
                    if (parts[7] == "NULL")
                    {
                        uzd.setAnswer(null);
                        uzd.setIsTheory(false);
                    }
                    else
                    {
                        uzd.setAnswer(parts[7]);
                        uzd.setIsTheory(true);
                    }
                    if (parts[9] == "1")
                        uzd.setIsMultChoice(true);
                    else
                        uzd.setIsMultChoice(false);
                    uzd.setSubTopic(parts[10]);
                    DB.Add(uzd);
                    System.Console.WriteLine(line);

                }
            }
            catch
            {
                int o = 33;
            }
            file.Close();


        }








    }
}
