﻿namespace masterVersion
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.panel2 = new MetroFramework.Controls.MetroPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.BTTN_OpenTestGen = new System.Windows.Forms.Button();
            this.ComboBox_tema = new MetroFramework.Controls.MetroComboBox();
            this.ComboBox_klase = new MetroFramework.Controls.MetroComboBox();
            this.ComboBox_subject = new MetroFramework.Controls.MetroComboBox();
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BTTNOpenGenTest = new System.Windows.Forms.Button();
            this.Profile = new System.Windows.Forms.Button();
            this.LoginMenu = new MetroFramework.Controls.MetroPanel();
            this.wrong_password = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.showPassword = new System.Windows.Forms.Button();
            this.PasswordTxt = new System.Windows.Forms.TextBox();
            this.LoginTxt = new System.Windows.Forms.TextBox();
            this.Help = new System.Windows.Forms.Button();
            this.Register = new System.Windows.Forms.Button();
            this.Contact = new System.Windows.Forms.Button();
            this.Login = new System.Windows.Forms.Button();
            this.but_close = new System.Windows.Forms.Button();
            this.logo = new System.Windows.Forms.PictureBox();
            this.htmlToolTip1 = new MetroFramework.Drawing.Html.HtmlToolTip();
            this.currentFeat = new System.Windows.Forms.Label();
            this.info_but_help = new System.Windows.Forms.Button();
            this.info_but_feedback = new System.Windows.Forms.Button();
            this.info_but_contacts = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.buttonPanel.SuspendLayout();
            this.LoginMenu.SuspendLayout();
            this.wrong_password.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.BTTN_OpenTestGen);
            this.panel2.Controls.Add(this.ComboBox_tema);
            this.panel2.Controls.Add(this.ComboBox_klase);
            this.panel2.Controls.Add(this.ComboBox_subject);
            this.panel2.HorizontalScrollbarBarColor = true;
            this.panel2.HorizontalScrollbarHighlightOnWheel = false;
            this.panel2.HorizontalScrollbarSize = 10;
            this.panel2.Location = new System.Drawing.Point(261, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(690, 149);
            this.panel2.TabIndex = 1;
            this.panel2.VerticalScrollbarBarColor = true;
            this.panel2.VerticalScrollbarHighlightOnWheel = false;
            this.panel2.VerticalScrollbarSize = 10;
            this.panel2.Visible = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(273, 39);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(197, 29);
            this.button5.TabIndex = 11;
            this.button5.Text = "Izveidot";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.openTaskCreator);
            this.button5.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.button5.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(476, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(197, 29);
            this.button3.TabIndex = 10;
            this.button3.Text = "Ģenerēt";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.StartProcess);
            this.button3.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.button3.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // BTTN_OpenTestGen
            // 
            this.BTTN_OpenTestGen.BackColor = System.Drawing.Color.Transparent;
            this.BTTN_OpenTestGen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BTTN_OpenTestGen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BTTN_OpenTestGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTTN_OpenTestGen.Location = new System.Drawing.Point(273, 39);
            this.BTTN_OpenTestGen.Name = "BTTN_OpenTestGen";
            this.BTTN_OpenTestGen.Size = new System.Drawing.Size(197, 29);
            this.BTTN_OpenTestGen.TabIndex = 9;
            this.BTTN_OpenTestGen.Text = "Izveidot";
            this.BTTN_OpenTestGen.UseVisualStyleBackColor = false;
            this.BTTN_OpenTestGen.Click += new System.EventHandler(this.button8_Click);
            this.BTTN_OpenTestGen.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.BTTN_OpenTestGen.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // ComboBox_tema
            // 
            this.ComboBox_tema.FormattingEnabled = true;
            this.ComboBox_tema.ItemHeight = 23;
            this.ComboBox_tema.Location = new System.Drawing.Point(341, 3);
            this.ComboBox_tema.Name = "ComboBox_tema";
            this.ComboBox_tema.Size = new System.Drawing.Size(332, 29);
            this.ComboBox_tema.TabIndex = 7;
            this.ComboBox_tema.UseSelectable = true;
            this.ComboBox_tema.SelectedIndexChanged += new System.EventHandler(this.ComboBox_tema_SelectedIndexChanged);
            // 
            // ComboBox_klase
            // 
            this.ComboBox_klase.FormattingEnabled = true;
            this.ComboBox_klase.ItemHeight = 23;
            this.ComboBox_klase.Location = new System.Drawing.Point(214, 3);
            this.ComboBox_klase.Name = "ComboBox_klase";
            this.ComboBox_klase.Size = new System.Drawing.Size(121, 29);
            this.ComboBox_klase.TabIndex = 6;
            this.ComboBox_klase.UseSelectable = true;
            this.ComboBox_klase.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // ComboBox_subject
            // 
            this.ComboBox_subject.FormattingEnabled = true;
            this.ComboBox_subject.ItemHeight = 23;
            this.ComboBox_subject.Location = new System.Drawing.Point(3, 3);
            this.ComboBox_subject.Name = "ComboBox_subject";
            this.ComboBox_subject.Size = new System.Drawing.Size(205, 29);
            this.ComboBox_subject.TabIndex = 2;
            this.ComboBox_subject.UseSelectable = true;
            this.ComboBox_subject.SelectedIndexChanged += new System.EventHandler(this.ComboBox_subject_SelectedIndexChanged);
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.button2);
            this.buttonPanel.Controls.Add(this.button1);
            this.buttonPanel.Controls.Add(this.BTTNOpenGenTest);
            this.buttonPanel.Controls.Add(this.Profile);
            this.buttonPanel.Location = new System.Drawing.Point(26, 63);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(229, 611);
            this.buttonPanel.TabIndex = 0;
            this.buttonPanel.Visible = false;
            this.buttonPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(3, 358);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(210, 58);
            this.button2.TabIndex = 15;
            this.button2.Tag = "4";
            this.button2.Text = "Izveidot uzdevumu";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.taskCreator_Click);
            this.button2.MouseEnter += new System.EventHandler(this.Mouse_Enter_1);
            this.button2.MouseLeave += new System.EventHandler(this.Mouse_Leave_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gray;
            this.button1.Location = new System.Drawing.Point(3, 294);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 58);
            this.button1.TabIndex = 13;
            this.button1.Tag = "3";
            this.button1.Text = "Mani kontroldarbi";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // BTTNOpenGenTest
            // 
            this.BTTNOpenGenTest.BackColor = System.Drawing.Color.Transparent;
            this.BTTNOpenGenTest.FlatAppearance.BorderSize = 0;
            this.BTTNOpenGenTest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BTTNOpenGenTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTTNOpenGenTest.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTTNOpenGenTest.ForeColor = System.Drawing.Color.Black;
            this.BTTNOpenGenTest.Location = new System.Drawing.Point(3, 230);
            this.BTTNOpenGenTest.Name = "BTTNOpenGenTest";
            this.BTTNOpenGenTest.Size = new System.Drawing.Size(210, 58);
            this.BTTNOpenGenTest.TabIndex = 11;
            this.BTTNOpenGenTest.Tag = "0";
            this.BTTNOpenGenTest.Text = "Izveidot kontrodarbu";
            this.BTTNOpenGenTest.UseVisualStyleBackColor = false;
            this.BTTNOpenGenTest.Click += new System.EventHandler(this.generate_Click);
            this.BTTNOpenGenTest.MouseEnter += new System.EventHandler(this.Mouse_Enter_1);
            this.BTTNOpenGenTest.MouseLeave += new System.EventHandler(this.Mouse_Leave_1);
            // 
            // Profile
            // 
            this.Profile.BackColor = System.Drawing.Color.Transparent;
            this.Profile.FlatAppearance.BorderSize = 0;
            this.Profile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Profile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Profile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Profile.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Profile.ForeColor = System.Drawing.Color.Maroon;
            this.Profile.Image = ((System.Drawing.Image)(resources.GetObject("Profile.Image")));
            this.Profile.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Profile.Location = new System.Drawing.Point(27, 92);
            this.Profile.Name = "Profile";
            this.Profile.Size = new System.Drawing.Size(160, 124);
            this.Profile.TabIndex = 10;
            this.Profile.Text = "User Profile";
            this.Profile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Profile.UseVisualStyleBackColor = false;
            this.Profile.MouseHover += new System.EventHandler(this.Profile_MouseHover);
            // 
            // LoginMenu
            // 
            this.LoginMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoginMenu.Controls.Add(this.wrong_password);
            this.LoginMenu.Controls.Add(this.showPassword);
            this.LoginMenu.Controls.Add(this.PasswordTxt);
            this.LoginMenu.Controls.Add(this.LoginTxt);
            this.LoginMenu.Controls.Add(this.Help);
            this.LoginMenu.Controls.Add(this.Register);
            this.LoginMenu.Controls.Add(this.Contact);
            this.LoginMenu.Controls.Add(this.Login);
            this.LoginMenu.Controls.Add(this.but_close);
            this.LoginMenu.Controls.Add(this.logo);
            this.LoginMenu.HorizontalScrollbarBarColor = true;
            this.LoginMenu.HorizontalScrollbarHighlightOnWheel = false;
            this.LoginMenu.HorizontalScrollbarSize = 10;
            this.LoginMenu.Location = new System.Drawing.Point(9, 12);
            this.LoginMenu.Name = "LoginMenu";
            this.LoginMenu.Size = new System.Drawing.Size(52, 45);
            this.LoginMenu.TabIndex = 3;
            this.LoginMenu.VerticalScrollbarBarColor = true;
            this.LoginMenu.VerticalScrollbarHighlightOnWheel = false;
            this.LoginMenu.VerticalScrollbarSize = 10;
            // 
            // wrong_password
            // 
            this.wrong_password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wrong_password.Controls.Add(this.richTextBox1);
            this.wrong_password.Controls.Add(this.button4);
            this.wrong_password.Location = new System.Drawing.Point(376, 306);
            this.wrong_password.Name = "wrong_password";
            this.wrong_password.Size = new System.Drawing.Size(301, 70);
            this.wrong_password.TabIndex = 18;
            this.wrong_password.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(296, 25);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "Lietotājvārds un/vai parole nav pareizs/a!";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(118, 34);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(62, 27);
            this.button4.TabIndex = 0;
            this.button4.Text = "OK";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // showPassword
            // 
            this.showPassword.BackColor = System.Drawing.Color.Transparent;
            this.showPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.showPassword.FlatAppearance.BorderSize = 0;
            this.showPassword.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.showPassword.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.showPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showPassword.Image = global::masterVersion.Properties.Resources.eye_close;
            this.showPassword.Location = new System.Drawing.Point(627, 271);
            this.showPassword.Name = "showPassword";
            this.showPassword.Size = new System.Drawing.Size(25, 29);
            this.showPassword.TabIndex = 17;
            this.showPassword.UseVisualStyleBackColor = false;
            this.showPassword.Click += new System.EventHandler(this.showPassword_Click_1);
            this.showPassword.MouseEnter += new System.EventHandler(this.Mouse_ShowPassWord_Enter);
            this.showPassword.MouseLeave += new System.EventHandler(this.Mouse_ShowPassWord_Leave);
            // 
            // PasswordTxt
            // 
            this.PasswordTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PasswordTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordTxt.Location = new System.Drawing.Point(435, 271);
            this.PasswordTxt.Name = "PasswordTxt";
            this.PasswordTxt.Size = new System.Drawing.Size(186, 22);
            this.PasswordTxt.TabIndex = 16;
            this.PasswordTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PasswordTxt.Enter += new System.EventHandler(this.PasswordTxt_Enter);
            this.PasswordTxt.Leave += new System.EventHandler(this.PasswordTxt_Leave);
            // 
            // LoginTxt
            // 
            this.LoginTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LoginTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginTxt.Location = new System.Drawing.Point(435, 236);
            this.LoginTxt.Name = "LoginTxt";
            this.LoginTxt.Size = new System.Drawing.Size(186, 22);
            this.LoginTxt.TabIndex = 15;
            this.LoginTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LoginTxt.Enter += new System.EventHandler(this.LoginTxt_Enter);
            this.LoginTxt.Leave += new System.EventHandler(this.LoginTxt_Leave);
            // 
            // Help
            // 
            this.Help.Enabled = false;
            this.Help.FlatAppearance.BorderSize = 0;
            this.Help.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Help.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Help.Location = new System.Drawing.Point(435, 400);
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(186, 23);
            this.Help.TabIndex = 14;
            this.Help.Text = "Palīgs";
            this.Help.UseVisualStyleBackColor = true;
            this.Help.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.Help.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // Register
            // 
            this.Register.BackColor = System.Drawing.Color.Transparent;
            this.Register.FlatAppearance.BorderSize = 0;
            this.Register.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Register.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Register.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Register.Location = new System.Drawing.Point(435, 354);
            this.Register.Name = "Register";
            this.Register.Size = new System.Drawing.Size(186, 40);
            this.Register.TabIndex = 13;
            this.Register.Text = "Reģistrēties";
            this.Register.UseVisualStyleBackColor = false;
            this.Register.Click += new System.EventHandler(this.Register_Click);
            this.Register.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.Register.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // Contact
            // 
            this.Contact.BackColor = System.Drawing.Color.Transparent;
            this.Contact.FlatAppearance.BorderSize = 0;
            this.Contact.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Contact.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Contact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Contact.Location = new System.Drawing.Point(435, 429);
            this.Contact.Name = "Contact";
            this.Contact.Size = new System.Drawing.Size(186, 23);
            this.Contact.TabIndex = 12;
            this.Contact.Text = "Sazināties";
            this.Contact.UseVisualStyleBackColor = false;
            this.Contact.Click += new System.EventHandler(this.Contact_Click);
            this.Contact.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.Contact.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.Color.Transparent;
            this.Login.FlatAppearance.BorderSize = 0;
            this.Login.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login.Location = new System.Drawing.Point(435, 306);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(186, 40);
            this.Login.TabIndex = 11;
            this.Login.Text = "Ienākt";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            this.Login.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.Login.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // but_close
            // 
            this.but_close.Location = new System.Drawing.Point(98, 70);
            this.but_close.Margin = new System.Windows.Forms.Padding(2);
            this.but_close.Name = "but_close";
            this.but_close.Size = new System.Drawing.Size(41, 24);
            this.but_close.TabIndex = 9;
            this.but_close.Text = "button4";
            this.but_close.UseVisualStyleBackColor = true;
            this.but_close.Click += new System.EventHandler(this.appExit);
            this.but_close.MouseEnter += new System.EventHandler(this.Mouse_Exit_Enter);
            this.but_close.MouseLeave += new System.EventHandler(this.Mouse_Exit_Leave);
            // 
            // logo
            // 
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("logo.BackgroundImage")));
            this.logo.Location = new System.Drawing.Point(246, 40);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(623, 188);
            this.logo.TabIndex = 8;
            this.logo.TabStop = false;
            // 
            // htmlToolTip1
            // 
            this.htmlToolTip1.OwnerDraw = true;
            // 
            // currentFeat
            // 
            this.currentFeat.AutoSize = true;
            this.currentFeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentFeat.Location = new System.Drawing.Point(260, 36);
            this.currentFeat.Name = "currentFeat";
            this.currentFeat.Size = new System.Drawing.Size(60, 24);
            this.currentFeat.TabIndex = 5;
            this.currentFeat.Text = "label1";
            this.currentFeat.Visible = false;
            // 
            // info_but_help
            // 
            this.info_but_help.BackColor = System.Drawing.Color.Transparent;
            this.info_but_help.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.info_but_help.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.info_but_help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info_but_help.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info_but_help.Location = new System.Drawing.Point(88, 3);
            this.info_but_help.Name = "info_but_help";
            this.info_but_help.Size = new System.Drawing.Size(74, 23);
            this.info_but_help.TabIndex = 7;
            this.info_but_help.Text = "Palīdzība";
            this.info_but_help.UseVisualStyleBackColor = false;
            this.info_but_help.Visible = false;
            this.info_but_help.Click += new System.EventHandler(this.info_but_help_Click);
            this.info_but_help.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.info_but_help.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // info_but_feedback
            // 
            this.info_but_feedback.BackColor = System.Drawing.Color.Transparent;
            this.info_but_feedback.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.info_but_feedback.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.info_but_feedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info_but_feedback.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info_but_feedback.Location = new System.Drawing.Point(167, 3);
            this.info_but_feedback.Name = "info_but_feedback";
            this.info_but_feedback.Size = new System.Drawing.Size(74, 23);
            this.info_but_feedback.TabIndex = 8;
            this.info_but_feedback.Text = "Atsauksmes";
            this.info_but_feedback.UseVisualStyleBackColor = false;
            this.info_but_feedback.Visible = false;
            this.info_but_feedback.Click += new System.EventHandler(this.info_but_feedback_Click);
            this.info_but_feedback.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.info_but_feedback.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // info_but_contacts
            // 
            this.info_but_contacts.BackColor = System.Drawing.Color.Transparent;
            this.info_but_contacts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.info_but_contacts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.info_but_contacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.info_but_contacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info_but_contacts.Location = new System.Drawing.Point(247, 3);
            this.info_but_contacts.Name = "info_but_contacts";
            this.info_but_contacts.Size = new System.Drawing.Size(74, 23);
            this.info_but_contacts.TabIndex = 9;
            this.info_but_contacts.Text = "Kontakti";
            this.info_but_contacts.UseVisualStyleBackColor = false;
            this.info_but_contacts.Visible = false;
            this.info_but_contacts.Click += new System.EventHandler(this.info_but_contacts_Click);
            this.info_but_contacts.MouseEnter += new System.EventHandler(this.Mouse_Login_Menu_Enter);
            this.info_but_contacts.MouseLeave += new System.EventHandler(this.Mouse_Login_Menu_Leave);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1034, 596);
            this.Controls.Add(this.info_but_contacts);
            this.Controls.Add(this.info_but_feedback);
            this.Controls.Add(this.info_but_help);
            this.Controls.Add(this.currentFeat);
            this.Controls.Add(this.LoginMenu);
            this.Controls.Add(this.buttonPanel);
            this.Controls.Add(this.panel2);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "main";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.Text = "main";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.main_Load);
            this.panel2.ResumeLayout(false);
            this.buttonPanel.ResumeLayout(false);
            this.LoginMenu.ResumeLayout(false);
            this.LoginMenu.PerformLayout();
            this.wrong_password.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroPanel panel2;
        private MetroFramework.Controls.MetroComboBox ComboBox_subject;
        private MetroFramework.Controls.MetroComboBox ComboBox_klase;
        private MetroFramework.Controls.MetroComboBox ComboBox_tema;
        private MetroFramework.Controls.MetroPanel LoginMenu;
        private System.Windows.Forms.Panel buttonPanel;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.Button Profile;
        private System.Windows.Forms.Button BTTNOpenGenTest;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button but_close;
        private System.Windows.Forms.Button Help;
        private System.Windows.Forms.Button Register;
        private System.Windows.Forms.Button Contact;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.TextBox PasswordTxt;
        private System.Windows.Forms.TextBox LoginTxt;
        private System.Windows.Forms.Button showPassword;
        private System.Windows.Forms.Panel wrong_password;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button4;
        private MetroFramework.Drawing.Html.HtmlToolTip htmlToolTip1;
        private System.Windows.Forms.Label currentFeat;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button BTTN_OpenTestGen;
        private System.Windows.Forms.Button info_but_help;
        private System.Windows.Forms.Button info_but_feedback;
        private System.Windows.Forms.Button info_but_contacts;
        private System.Windows.Forms.Button button5;
    }
}