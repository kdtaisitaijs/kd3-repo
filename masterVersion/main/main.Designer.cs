﻿namespace masterVersion.main_menu
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new MetroFramework.Controls.MetroPanel();
            this.button2 = new MetroFramework.Controls.MetroButton();
            this.BTTN_OpenTestGen = new MetroFramework.Controls.MetroButton();
            this.button1 = new MetroFramework.Controls.MetroButton();
            this.BTTN_ImportTetst = new MetroFramework.Controls.MetroButton();
            this.BTTN_OpenGenTest = new MetroFramework.Controls.MetroButton();
            this.panel2 = new MetroFramework.Controls.MetroPanel();
            this.ComboBox_subject = new MetroFramework.Controls.MetroComboBox();
            this.ComboBox_tema = new MetroFramework.Controls.MetroComboBox();
            this.ComboBox_klase = new MetroFramework.Controls.MetroComboBox();
            this.button3 = new MetroFramework.Controls.MetroButton();
            this.toolTip1 = new MetroFramework.Components.MetroToolTip();
            this.Title = new MetroFramework.Drawing.Html.HtmlLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.BTTN_ImportTetst);
            this.panel1.Controls.Add(this.BTTN_OpenGenTest);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.BTTN_OpenTestGen);
            this.panel1.HorizontalScrollbarBarColor = true;
            this.panel1.HorizontalScrollbarHighlightOnWheel = false;
            this.panel1.HorizontalScrollbarSize = 10;
            this.panel1.Location = new System.Drawing.Point(13, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(116, 416);
            this.panel1.TabIndex = 0;
            this.panel1.VerticalScrollbarBarColor = true;
            this.panel1.VerticalScrollbarHighlightOnWheel = false;
            this.panel1.VerticalScrollbarSize = 10;
            // 
            // button2
            // 
            this.button2.AccessibleName = "x";
            this.button2.Location = new System.Drawing.Point(10, 329);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 73);
            this.button2.TabIndex = 1;
            this.button2.Text = "Create Task";
            this.button2.UseSelectable = true;
            // 
            // BTTN_OpenTestGen
            // 
            this.BTTN_OpenTestGen.AccessibleName = "x";
            this.BTTN_OpenTestGen.Location = new System.Drawing.Point(10, 92);
            this.BTTN_OpenTestGen.Name = "BTTN_OpenTestGen";
            this.BTTN_OpenTestGen.Size = new System.Drawing.Size(96, 73);
            this.BTTN_OpenTestGen.TabIndex = 2;
            this.BTTN_OpenTestGen.Text = "Create Test";
            this.BTTN_OpenTestGen.UseSelectable = true;
            // 
            // button1
            // 
            this.button1.AccessibleName = "x";
            this.button1.Location = new System.Drawing.Point(10, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 73);
            this.button1.TabIndex = 3;
            this.button1.Text = "My Tests";
            this.button1.UseSelectable = true;
            // 
            // BTTN_ImportTetst
            // 
            this.BTTN_ImportTetst.AccessibleName = "x";
            this.BTTN_ImportTetst.Location = new System.Drawing.Point(10, 250);
            this.BTTN_ImportTetst.Name = "BTTN_ImportTetst";
            this.BTTN_ImportTetst.Size = new System.Drawing.Size(96, 73);
            this.BTTN_ImportTetst.TabIndex = 4;
            this.BTTN_ImportTetst.Text = "Import Test";
            this.BTTN_ImportTetst.UseSelectable = true;
            // 
            // BTTN_OpenGenTest
            // 
            this.BTTN_OpenGenTest.AccessibleName = "x";
            this.BTTN_OpenGenTest.Location = new System.Drawing.Point(10, 13);
            this.BTTN_OpenGenTest.Name = "BTTN_OpenGenTest";
            this.BTTN_OpenGenTest.Size = new System.Drawing.Size(96, 73);
            this.BTTN_OpenGenTest.TabIndex = 5;
            this.BTTN_OpenGenTest.Text = "Generate Test";
            this.BTTN_OpenGenTest.UseSelectable = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.ComboBox_klase);
            this.panel2.Controls.Add(this.ComboBox_tema);
            this.panel2.Controls.Add(this.ComboBox_subject);
            this.panel2.HorizontalScrollbarBarColor = true;
            this.panel2.HorizontalScrollbarHighlightOnWheel = false;
            this.panel2.HorizontalScrollbarSize = 10;
            this.panel2.Location = new System.Drawing.Point(135, 379);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(686, 100);
            this.panel2.TabIndex = 1;
            this.panel2.VerticalScrollbarBarColor = true;
            this.panel2.VerticalScrollbarHighlightOnWheel = false;
            this.panel2.VerticalScrollbarSize = 10;
            // 
            // ComboBox_subject
            // 
            this.ComboBox_subject.FormattingEnabled = true;
            this.ComboBox_subject.ItemHeight = 23;
            this.ComboBox_subject.Location = new System.Drawing.Point(21, 13);
            this.ComboBox_subject.Name = "ComboBox_subject";
            this.ComboBox_subject.Size = new System.Drawing.Size(165, 29);
            this.ComboBox_subject.TabIndex = 2;
            this.ComboBox_subject.UseSelectable = true;
            // 
            // ComboBox_tema
            // 
            this.ComboBox_tema.FormattingEnabled = true;
            this.ComboBox_tema.ItemHeight = 23;
            this.ComboBox_tema.Location = new System.Drawing.Point(21, 48);
            this.ComboBox_tema.Name = "ComboBox_tema";
            this.ComboBox_tema.Size = new System.Drawing.Size(246, 29);
            this.ComboBox_tema.TabIndex = 3;
            this.ComboBox_tema.UseSelectable = true;
            // 
            // ComboBox_klase
            // 
            this.ComboBox_klase.FormattingEnabled = true;
            this.ComboBox_klase.ItemHeight = 23;
            this.ComboBox_klase.Location = new System.Drawing.Point(192, 13);
            this.ComboBox_klase.Name = "ComboBox_klase";
            this.ComboBox_klase.Size = new System.Drawing.Size(75, 29);
            this.ComboBox_klase.TabIndex = 4;
            this.ComboBox_klase.UseSelectable = true;
            // 
            // button3
            // 
            this.button3.AccessibleName = "x";
            this.button3.Location = new System.Drawing.Point(287, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 64);
            this.button3.TabIndex = 6;
            this.button3.Text = "Go";
            this.button3.UseSelectable = true;
            // 
            // toolTip1
            // 
            this.toolTip1.Style = MetroFramework.MetroColorStyle.Blue;
            this.toolTip1.StyleManager = null;
            this.toolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // Title
            // 
            this.Title.AutoScroll = true;
            this.Title.AutoScrollMinSize = new System.Drawing.Size(228, 48);
            this.Title.AutoSize = false;
            this.Title.BackColor = System.Drawing.SystemColors.Window;
            this.Title.Location = new System.Drawing.Point(135, 8);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(279, 58);
            this.Title.TabIndex = 2;
            this.Title.Text = " Generate Test";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 524);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "main";
            this.Text = "main";
            this.Load += new System.EventHandler(this.main_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel panel1;
        private MetroFramework.Controls.MetroButton button2;
        private MetroFramework.Controls.MetroButton BTTN_ImportTetst;
        private MetroFramework.Controls.MetroButton BTTN_OpenGenTest;
        private MetroFramework.Controls.MetroButton button1;
        private MetroFramework.Controls.MetroButton BTTN_OpenTestGen;
        private MetroFramework.Controls.MetroPanel panel2;
        private MetroFramework.Controls.MetroComboBox ComboBox_tema;
        private MetroFramework.Controls.MetroComboBox ComboBox_subject;
        private MetroFramework.Controls.MetroButton button3;
        private MetroFramework.Controls.MetroComboBox ComboBox_klase;
        private MetroFramework.Components.MetroToolTip toolTip1;
        private MetroFramework.Drawing.Html.HtmlLabel Title;
    }
}