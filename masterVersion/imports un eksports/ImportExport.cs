﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace masterVersion
{
    class ImportExport
    {
        main mainForm;
        List<Uzdevums> exportTasks = new List<Uzdevums>();
        List<Uzdevums> export = new List<Uzdevums>();
        Uzdevums uzdevums = new Uzdevums();
        
        DatabaseAccess databaseAcess = new DatabaseAccess();
        SaveFileDialog dialog = new SaveFileDialog();
        OpenFileDialog dialogOpen = new OpenFileDialog();

        List<string> task = new List<string>();
        List<List<string>> taskParm = new List<List<string>>();
       
        List<int> idList = new List<int>();
        ToDoDialog toDoImport;
        int id;
        int idTolist;
        string folderPath;
        string inputLine;
        string text;
        int diff;
        int grade;
        string[] formulas;
        string topic;
        string[] atbildes;
        string atbilde;
        string formula;
        bool isTheory;
        string saveString;
        public void sendDataForExport(KdData data)
        {
            

            if (data.Uzdevumi.Count > 0)
            {
                dialog.InitialDirectory = Path.GetFullPath("mani kontroldarbi");
                dialog.RestoreDirectory = true;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    folderPath = dialog.FileName + ".ing";
                    FileStream stream = File.Create(folderPath);
                    var formatter = new BinaryFormatter();                    
                    formatter.Serialize(stream,data);
                    stream.Close();
                }              
            }
            else
            {
                MessageBox.Show("Kotroldarbā nav neviens uzdevums. Never veikt eksportu");
            }
        }

        public KdData importTest(main _main,string filePath= "")
        {
            if(filePath == "") {
                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "Ingenium files (*.ing)|*.ing;|All files (*.*)|*.*";
                DialogResult result = open.ShowDialog();

                task.Clear();
                KdData importedTest = new KdData();
                if (result == DialogResult.OK)
                {
                    FileStream stream = File.OpenRead(open.FileName);
                    var formatter = new BinaryFormatter();
                    importedTest = (KdData)formatter.Deserialize(stream);
                    stream.Close();
                }

                if ((importedTest.Uzdevumi.Count > 0))
                {


                    toDoImport = new ToDoDialog(importedTest, _main);
                    toDoImport.Show();
                }
                else
                {
                    MessageBox.Show("Kļūda importā");
                }
                return null;
            }else
            {
                KdData importedTest = new KdData();
                FileStream stream = File.OpenRead(filePath);
                var formatter = new BinaryFormatter();
                importedTest = (KdData)formatter.Deserialize(stream);
                stream.Close();
                return importedTest;
            }
            
        }
    }
}
