﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    class VeidotUzdViela
    {
        PievienotUzdevumuMaster PievienotForm;
        Panel container;
        RichTextBox taskText;
        public static ConstTables table = null;
        public VeidotUzdViela(PievienotUzdevumuMaster PievienotForm)
        {
            container = PievienotForm.GetSpecificpanel();
            taskText = PievienotForm.GetTaskField();
            this.PievienotForm = PievienotForm;

            /*Label subtemaLabel = new Label();
            subtemaLabel.Text = "Apakštēma";
            container.Controls.Add(subtemaLabel);*/

            
                       
            Button RandomBlivumsBtn = new Button();
            RandomBlivumsBtn.Text = "Pievienot random blīvumu";
            RandomBlivumsBtn.Size = new System.Drawing.Size(200, RandomBlivumsBtn.Height);
            RandomBlivumsBtn.Click += new EventHandler(AddRandomBlivums);
            container.Controls.Add(RandomBlivumsBtn);

            Button BlivumaTabulaBtn = new Button();
            BlivumaTabulaBtn.Text = "Atvērt blīvuma tabulu";
            BlivumaTabulaBtn.Size = new System.Drawing.Size(200, RandomBlivumsBtn.Height*2+5);
            BlivumaTabulaBtn.Click += new EventHandler(OpenDensTable);
            container.Controls.Add(BlivumaTabulaBtn);


        }
        public void OpenDensTable(object sender, EventArgs e)
        {
            if (table == null)
            {
                table = new ConstTables(PievienotForm,TablesAndConstants.tableTypes.blivums);
                table.Text = "Blīvuma tabula";
                table.Show();
            }
            else
            {
                table.BringToFront();
            } 
        }


        private void AddRandomBlivums(object sender, EventArgs e)
        {
            taskText.Text += "?blīvums";
        }

    }
}
