﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class dialog_main : MetroFramework.Forms.MetroForm
    {
        public void setText(string text, bool okOnly)
        {
            richTextBox1.Text = "Nav izvēlēta" + text + "." + "Tiks atlasīti visi uzdevumi no šīs klases." + "\r\n" + "Vai turpināt?";

            if(okOnly == true)
            {
                but_Ok.Visible = false;
                but_back.Text = "Okay";
            }
        }


   

        public dialog_main()
        {
            InitializeComponent();
            but_back.DialogResult = DialogResult.Cancel;
            but_Ok.DialogResult = DialogResult.OK;
        }

        private void dialog_main_Load(object sender, EventArgs e)
        {

        }
    }
}
