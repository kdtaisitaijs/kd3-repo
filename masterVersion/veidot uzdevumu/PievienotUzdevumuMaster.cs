﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;



namespace masterVersion 
{
    public partial class PievienotUzdevumuMaster : MetroFramework.Forms.MetroForm
    {
        public ConstTables constTable = null;
        public ConstTables greekTable = null;
        public ConstTables functionTable = null;
        private WpfMath.TexFormulaParser formulaParser;
        SubMenuTestGen mainForm;
        AddPicture addImageWindow;
        Random rnd;
        String answer;
        float taskTime = 5;
        int difficulty = -1;
        int formulaCount = 0;
        int klase = 10;
        string tema = "";
        bool multipleChoice = false;
        bool theoretical = false;
        string prieksmets;
        string apaksTema = "";
        TextBox currentTextBox;
        KlasuTemas klTemas;
        RichTextBox currentRichTextBox;
        List<object[]> formulaArray;
        List<object[]> numberArray;
        List<string> formulas;
        List<object[]> orderArray;
        List<String> solutions;
        List<TextBox> unitBoxes;
        List<Button> unitButtons;
        List<Button> diffButtons;
        List<Bitmap> images = new List<Bitmap>();
        List<string> imagePaths = new List<string>();
        List<Stack<char>> operatorStack;
        String[] multipleChoiceOptions;
        Dictionary<String, String> unitDict;

        Dictionary<String, float> TextConstDict;
        Uzdevums Uzd;
        TaskSolve taskSolve;
        PievienotUzdSpecific spec;
        public static Dictionary<int, Dictionary<string, string[]>> subTemas = new Dictionary<int, Dictionary<string, string[]>>();

        public PievienotUzdevumuMaster(string Prieksmets,int Klase, string Tema)
        {
            WpfMath.TexFormulaParser.Initialize();    
            // TODO neiet kkadu iemeslu dēļ
            this.formulaParser = new WpfMath.TexFormulaParser();
            string readThemes = File.ReadAllText("Data//temasFizika.json");
            subTemas = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, string[]>>>(readThemes);
            prieksmets = Prieksmets;
            klase = Klase;
            tema = Tema;
            klTemas = new KlasuTemas();
            taskSolve = new TaskSolve();            
            InitializeComponent();
            CreateThemeBox();
            //richTextBox4.Visible = false;
            richTextBox2.LostFocus += RichTextBox2_LostFocus;
            diffButtons = new List<Button>();
            diffButtons.Add(diffBtn1);
            diffButtons.Add(diffBtn2);
            diffButtons.Add(diffBtn3);
            diffButtons.Add(diffBtn4);
            diffButtons.Add(diffBtn5);
            Clear();            
            rnd = new Random();
            flowLayoutPanel3.HorizontalScroll.Enabled = false;
            flowLayoutPanel3.HorizontalScroll.Visible = false;
            spec = new PievienotUzdSpecific(Tema,this);
            string readText = File.ReadAllText("units.txt");
            unitDict = JsonConvert.DeserializeObject<Dictionary<String, String>>(readText);
            //TextConstDict = new Dictionary<string, float>();
            FillTextConstDict();
            
            
        }
        private void CreateThemeBox()
        {            
            subtemaBox.TextChanged += SubtemaBox_TextChanged;
            String[] subtemaNames = subTemas[klase][tema];
            subtemaBox.Size = new System.Drawing.Size(200, subtemaBox.Height);
            subtemaBox.Items.AddRange(subtemaNames);
            subtemaBox.Text = "Choose sub-topic";            
        }

        private void SubtemaBox_TextChanged(object sender, EventArgs e)
        {
            ComboBox subBox = sender as ComboBox;
            if (subBox.Text != "Choose sub-topic")
                apaksTema = subBox.Text;
        }

        private void RichTextBox2_LostFocus(object sender, EventArgs e)
        {
            try
            {
                List<String> words = SplitFormula(richTextBox2.Text.Split('=')[1]);
                for (int i = 0; i < words.Count; i++)
                {
                   /* if (TextConstDict.ContainsKey(words[i]))
                    {
                        words[i] = TextConstDict[words[i]].ToString();
                    }*/
                }
                richTextBox2.Text = richTextBox2.Text.Split('=')[0]+"="+String.Join("", words.ToArray());
            }
            catch { }

        }

        private void Clear()
        {            
            Uzd = new Uzdevums();
            label3.Text = "";
            label6.Text = "";      
            label4.Text = "";
            theoreticalCheck.Checked = false;
            checkBox1.Checked = false;
            multipleChoiceOptions = new String[4];
            currentRichTextBox = richTextBox2;
            setDiffIcons(1);            
            panel2.Visible = true;
            panel3.Visible = true;            
            formulas = new List<string>();
            solutions = new List<string>();
            operatorStack = new List<Stack<char>>();
            formulaArray = new List<object[]>();
            numberArray = new List<object[]>();
            orderArray = new List<object[]>();            
            unitButtons = new List<Button>();
            unitBoxes = new List<TextBox>();
            /*typeButton.Text = "Mainīt tipu - brīva atblide";
            klase = 10;
            SetThemes(klase);
            tema = "";*/
            panel1.Visible = true;
            //richTextBox1.Width = 631;
            richTextBox1.Text = "";
            richTextBox2.Text = "";
            richTextBox3.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox4.Text = "";
            label3.Text = "";
            taskTime = 5;
            difficulty = 1;
            //comboBox1.Text = "5";
            multipleChAns.Text = "";
            theoretical = false;
            multipleChoice = false;
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();
            flowLayoutPanel3.Controls.Clear();
            formulaPanel.Controls.Clear();
            formulaCount = 0;
            panel5.Visible = true;

        }
        private void FillTextConstDict()
        {
            //TextConstDict["π"] = 3.1415f;
            //TextConstDict["g"] = 9.81f;

            
        }
      
        private void BTTN_backToMain_Click(object sender, EventArgs e)
        {
            //if (panel6.Visible)
            //{
                mainForm.Show();
                this.Hide();
            /*}
            else
            {
                typeButton.Visible = true;
                panel6.Visible = true;
                panel5.Visible = false;
            }*/
        }

        private void AddVar(object sender, EventArgs e) {
            var btn = sender as Button;
            string unit = "";
            var index = unitButtons.IndexOf(btn);
            unit = unitBoxes.ElementAt(index).Text;
            CheckBox ch = flowLayoutPanel3.Controls[index] as CheckBox;
            if (ch.Checked)
            {
                unit = "#" + unit;
            }
            richTextBox1.SelectedText += "{" + btn.Text + "} " + unit;
            richTextBox1.Focus();
            //richTextBox1.SelectionStart = richTextBox1.Text.Length;
            //richTextBox1.SelectionLength = 0;
        }
        private void PassFocus(int cursorPos)
        {
            if (currentTextBox != null)
            {
                currentTextBox.Focus();                
                currentTextBox.SelectionStart = cursorPos;
                currentTextBox.SelectionLength = 0;

            }
            else
            {
                if (currentRichTextBox != null)
                {
                    currentRichTextBox.Focus();
                    currentRichTextBox.SelectionStart = cursorPos;
                    currentRichTextBox.SelectionLength = 0;
                }
            }
        }
        private void ShowMessage(string Text)
        {
            try
            {
                MessageBoxManager.Register();
            }
            catch
            {

            }
            MessageBoxManager.OK = "Ok";
            MessageBox.Show(Text, "Add task");
            MessageBoxManager.Unregister();
        }
        private void AddFormula(object sender, EventArgs e)
        {
                      
            String formula =richTextBox2.Text;
            if (formula == "")
            {
                ShowMessage("Add Formula");
                return;
            }
            if (!formula.Contains("="))
            {
                ShowMessage("Formula has to end with (=) ");
                return;
            }

                
                      
            List<String> formulaParts = formula.Split('=').ToList<String>();                       
            List<String> forMath = SplitFormula(formulaParts[1]);            
            List<String> formulaSplit = new List<string>();
            foreach (string st in forMath)
            {
                if (Regex.IsMatch(st, @"^[a-zA-Z\p{IsGreekandCoptic}]+$"))
                    formulaSplit.Add(st);
                else if (Regex.IsMatch(st, @"^([A-Za-z]+[0-9]+|[0-9]+[A-Za-z]+)[A-Za-z0-9]*$"))
                    formulaSplit.Add(st);
            }





            // Tuple<Stack<object>,Stack<char>> formulaStacks= SY.GetStacks(forMath, null);
            //List<string> formulaStacks = ShuntingYardAlgorithm.GetStacks(forMath);
            //Test solve
            Uzdevums testUzd = new Uzdevums();            

            //TODO fix this stupid type casting
            string[] f=new string[1];
            f[0] = formula;
            testUzd.setText("$" + string.Join(" $", forMath));
            testUzd.setEquation(f);

            var ans =taskSolve.computeFormula(testUzd);

            //! Test solve
            if(ans.Item1.Contains("Failed to calculate."))
            {
                ShowMessage("Failed to add formula");
                return;
            }

            foreach (string variable in formulaSplit)
            {
                if (ShuntingYardAlgorithm.isFunction(variable))
                {
                    continue;
                }
                Button varButton = new MetroFramework.Controls.MetroButton();
                TextBox unitTextBox = new TextBox();
                try
                {
                    unitTextBox.Text = unitDict[variable];
                }
                catch
                {

                }
                unitTextBox.Width = 70;
                unitTextBox.Height = 20;
                unitTextBox.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
                unitBoxes.Add(unitTextBox);
                varButton.Text = variable.ToString();
                varButton.Click += new EventHandler(AddVar);
                varButton.Width = 50;
                varButton.Height = 30;
                unitButtons.Add(varButton);
                flowLayoutPanel1.Controls.Add(varButton);
                flowLayoutPanel2.Controls.Add(unitTextBox);
                CheckBox isSI = new CheckBox();
                flowLayoutPanel3.Controls.Add(isSI);
            }

            if (!theoretical)
                panel3.Visible = true;
            solutions.Add(formulaParts[0]);
            formulas.Add(richTextBox2.Text);
            richTextBox2.Text = "";
            Label formulaText = new Label();
            formulaText.Text = formula;
            formulaPanel.Controls.Add(formulaText);

            formulaCount++;
        }

     
        //Parsing of entered formula text
        //FIX: I Think this should be in Uzdevums class!
        public static List<String> SplitFormula(String toSplit)
        {
            String currentString = "";
            int type = 0;
            
            List<String> toReturn = new List<String>();
            foreach (char item in toSplit)
            {                
                if (Char.IsDigit(item)|| item =='.')
                {
                    if (1.Equals(type) || 2.Equals(type))
                        currentString += item;
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 1;
                }
                else if (Char.IsLetter(item))
                {
                    if (2.Equals(type) || 1.Equals(type))
                    {
                        currentString += item;                        
                    }
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 2;
                }
                else if (Char.Equals(item, '(') || Char.Equals(item, ')'))
                {
                    if (3.Equals(type))
                        currentString += item;
                    else
                    {
                        toReturn.Add(currentString);
                        currentString = item.ToString();
                    }
                    type = 3;
                }
                else
                {
                    if (Char.IsPunctuation(item) || Char.IsSymbol(item))
                    {
                        if (4.Equals(type))
                            currentString += item;
                        else
                        {
                            toReturn.Add(currentString);
                            currentString = item.ToString();
                        }
                        type = 4;
                    }
                }
            }
            toReturn.Add(currentString);
            toReturn.RemoveAll(string.IsNullOrWhiteSpace);
            return toReturn;
        }
        private void RenderText(string Text)
        {
            label3.Text = Text;
            /*WpfMath.TexFormula formula = null;
            formula = this.formulaParser.Parse(Text);            
            var renderer = formula.GetRenderer(WpfMath.TexStyle.Text, 15d);
            BitmapSource bmpSource = renderer.RenderToBitmap(80, 410);
            Bitmap bmp = BitmapFromSource(bmpSource);
            label3.Image = bmp;*/
            //SaveTextAsImage(bmp);

        }
        private Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }
            return bitmap;
        }
        private void SaveTextAsImage(Bitmap image)
        {
            try
            {
                image.Save("bilde2.png", System.Drawing.Imaging.ImageFormat.Png);
            }
            catch
            {

            }
        }
        //Checks if all variables are set for any equation
        private void computeFormula(object sender, EventArgs e)
        {

            //RenderText("and ris\n juris a=b*c \n \\frac{2n-1}{\\infty}");
                


            SetupUzdSolve();
            Tuple<string,string> Result = taskSolve.computeFormula(Uzd);
            String taskText = Result.Item1;
            double Solution;
            
            String answer = Result.Item2;
            
            if (theoretical)
            {
                if (multipleChoice)
                {
                    multipleChAns.Text = answer;
                }
            }
            else
            {

                //label3.Text = taskText;
                
                if(Double.TryParse(answer, out Solution))
                {                    
                        label4.Text = "Answer: " + Solution.ToString("F");
                        RenderText(taskText + "\nAnswer: "  + Solution.ToString("F"));
                }
                else
                {
                    label4.Text = "Answer: " + answer;
                    RenderText(taskText + "\nAnswer: " + answer);
                }
                
                multipleChAns.Text = answer.ToString();
                //Tekstu aizpilde atbilžu variantiem
                float result = float.Parse(answer);
                if(textBox1.Text =="")
                    textBox1.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
                if (textBox2.Text == "")
                    textBox2.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
                if (textBox4.Text == "")
                    textBox4.Text = (result + (int)(((double)rnd.Next(1, 10) - 5f) * result/5)).ToString();
            }
        }

        private void diffBtnClick(object sender, EventArgs e)
        {
            difficulty = diffButtons.IndexOf(sender as Button);            
        }

        private void diffBtnHover(object sender, EventArgs e)
        {
            //(sender as Button).ImageIndex = 1;
            int index = diffButtons.IndexOf(sender as Button);
            setDiffIcons(index);
        }
        private void setDiffIcons(int index)
        {
            for (int i = 0; i < 5; i++)
            {
                if (i <= index)
                    diffButtons[i].ImageIndex = 1;
                else
                    diffButtons[i].ImageIndex = 0;

            }
        }

        private void diffBtnLeave(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                if (i <= difficulty)
                    diffButtons[i].ImageIndex = 1;
                else
                    diffButtons[i].ImageIndex = 0;

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            taskTime = float.Parse((sender as ComboBox).Text);
        }

        private void SetupUzdOther()
        {
            if (difficulty == -1)
                difficulty = 3;
            Uzd.setSubTopic(apaksTema);
            Uzd.setDifficulty(difficulty);
            Uzd.setGrade(klase);
            Uzd.setTopic(tema);
            /*String[] tags = richTextBox4.Text.Split(',');
            int i = 0;
            foreach(string tag in tags)
            {
               tags[i] = tag.Trim();
               i++;
            }              */
            for(int j = 0; j < images.Count; j++)
            {
                images[j].Save(imagePaths[j]);
            }
            Uzd.setImagePaths(imagePaths.ToArray());
            Uzd.setEquation(formulas.ToArray());
            if (multipleChoice)
            {
                String[] MultAns = new String[4];
                MultAns[0] = multipleChAns.Text;
                MultAns[1] = textBox1.Text;
                MultAns[2] = textBox2.Text;
                MultAns[3] = textBox4.Text;
                Uzd.setMultChoices(MultAns);
            }

            
        }

        private void SetupUzdSolve()
        {
            Uzd.setText(richTextBox1.Text);
            Uzd.setEquation(formulas.ToArray());

            Uzd.setIsMultChoice(multipleChoice);
            Uzd.setIsTheory(theoretical);
            if (theoretical)
            {
                Uzd.setAnswer(answer);
            }
        }

        private void AddToDatabase(object sender, EventArgs e)
        {
            MessageBoxManager.Yes = "Yes";
            MessageBoxManager.No = "No";
            MessageBoxManager.OK = "Ok";
            MessageBoxManager.Register();
            DialogResult result1 = MessageBox.Show("Are you sure you want to add task to database?",                
            "Add task", MessageBoxButtons.YesNo);
            if(richTextBox1.Text == "")
            {
                ShowMessage("Add taks text");
                MessageBoxManager.Unregister();
                return;
            }
            if(apaksTema == "")
            {
                ShowMessage("Select sub-topic");
                return;
            }
            if (tema == "")
            {
                ShowMessage("Select topic");                
                return;
            }
            if (theoretical)
            {
                if(answer == "")
                {
                    ShowMessage("Add answer ");                
                    return;
                }
                if(multipleChoice && (textBox1.Text =="" || textBox2.Text == "" || textBox4.Text == ""))
                {
                    ShowMessage("Add answer to choices");                    
                    return;
                }

            }else
            {
                if (formulaCount < 1)
                {
                    ShowMessage("Add Formula ");                    
                    return;
                }
            }

            if (result1 == DialogResult.Yes)
            {
                DatabaseAccess db = new DatabaseAccess();
                SetupUzdOther();
                SetupUzdSolve();
                bool isConnected = db.Connect();               
                if (isConnected)
                {
                    db.insertExercise(Uzd);                    
                    db.closeConncetion();
                    Clear();
                    MessageBox.Show("Task has been added to database", "Add task");
                }else
                {
                    MessageBox.Show("Failed to add task", "Add task");
                }
            }
            MessageBoxManager.Unregister();
        }

        private void typeButton_Click(object sender, EventArgs e)
        {
            //panel6.Visible = false;
            panel5.Visible = true;
            //typeButton.Visible = false;
            currentTextBox = null;
            currentRichTextBox = richTextBox2;
            
        }

        private void theoreticalCheck_CheckedChanged(object sender, EventArgs e)
        {
            theoretical = !theoretical;
            panel2.Visible = theoretical;
            panel3.Visible = !theoretical;
        }

        private void richTextBox3_TextChanged_1(object sender, EventArgs e)
        {
            multipleChAns.Text = richTextBox3.Text;
            answer = richTextBox3.Text;
        }      

        private void button3_Click(object sender, EventArgs e)
        {
            if (greekTable == null)
            {
                greekTable = new ConstTables(this, TablesAndConstants.tableTypes.grieku);
                greekTable.Text = "Greek Alphabeth";
                greekTable.Show();
            }
            else
            {
                greekTable.BringToFront();
            }
        }
       

        
        private void button25_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (currentTextBox != null)
            {
                currentTextBox.SelectedText = btn.Text;
                PassFocus(currentTextBox.SelectionStart);
                return;
            }
            if (currentRichTextBox != null)
            {
                if (currentRichTextBox.Text.Contains("="))
                {
                    currentRichTextBox.Text += btn.Text;
                    PassFocus(currentRichTextBox.Text.Length);
                }
                else
                {
                    currentRichTextBox.SelectedText = btn.Text;
                    PassFocus(currentRichTextBox.SelectionStart);
                }
            }
            

        }

        private void multipleChAns_Click(object sender, EventArgs e)
        {
            
           currentTextBox = sender as TextBox;
            
            if(currentTextBox== null)
            {
                currentRichTextBox = sender as RichTextBox;
                currentTextBox = null;
            }
        }

        

        

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            label3.Text = taskSolve.generatePlainText(richTextBox1.Text,true);  
            /*try
            {
                RenderText(taskSolve.generatePlainText(richTextBox1.Text, true));
            }
            catch
            {

            }    */
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (multipleChoice)
            {
                //typeButton.Text = "Mainīt tipu - brīva atblide";
                panel1.Visible = false;
                //richTextBox1.Width = 631;
            }
            else
            {
                //richTextBox1.Width = 335;
                panel1.Visible = true;
               //typeButton.Text = "Mainīt tipu - atbližu varianti";
            }
            multipleChoice = !multipleChoice;
        }


        public Panel GetSpecificpanel(){
            return SpecificPanel;
        }
        public RichTextBox GetTaskField()
        {
            return richTextBox1;
        }
        public RichTextBox GetFormulaField()
        {
            return richTextBox2;
        }
        public RichTextBox GetCurrentTextField()
        {
            return currentRichTextBox;
        }
          /*
        private void SetThemes(int klase)
        {
            comboBox2.Items.Clear();

            //TODO error fix

            String[] texts = klTemas.getTemas(klase).ToArray();
            System.Object[] ItemObject = new System.Object[texts.Length];
            for (int i = 0; i < texts.Length; i++)
            {
                ItemObject[i] = texts[i];
            }
            comboBox2.Items.AddRange(ItemObject);
            tema = "";
            
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            klase = int.Parse(comboBox3.Text);
            SetThemes(klase);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            tema = comboBox2.Text;
        }*/

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (constTable == null)
            {
                constTable = new ConstTables(this, TablesAndConstants.tableTypes.konstantes);
                constTable.Text = "Constants";
                constTable.Show();
            }
            else
            {
                constTable.BringToFront();
            }
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        public void RemovePicture()
        {
            //TODO: removes all images, not specified images.
            imagePaths.Clear();
            images.Clear();
        }
        public void AddPicture(string imagePath)
        {
            Bitmap img = new Bitmap(imagePath);
            images.Add(img);
            imagePaths.Add("fakeFTP\\" + RandomString(12) + ".png");
            
        }
        private void AddImageButton_Click(object sender, EventArgs e)
        {
            if (images.Count == 0)
            {
                addImageWindow = new AddPicture(this);
            }
            else
            {
                addImageWindow = new AddPicture(this,images[0]);
            }
            addImageWindow.Show();      
        }

        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rnd.Next(s.Length)]).ToArray());
        }

        private void functionBtn_Click(object sender, EventArgs e)
        {
            if (functionTable == null)
            {
                functionTable = new ConstTables(this, TablesAndConstants.tableTypes.funkcijas);
                functionTable.Text = "Math functions";
                functionTable.Show();
            }
            else
            {
                functionTable.BringToFront();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        /*private void richTextBox4_TextChanged(object sender, EventArgs e)
        {

        }*/
    }
}
