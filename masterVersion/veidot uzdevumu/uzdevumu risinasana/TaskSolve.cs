﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion
{
    class TaskSolve
    {
        Random rnd;
        Dictionary<String, float> orderDict;
        List<int> numForPlainText;
        public TaskSolve()
        {
            rnd = new Random();
            orderDict = new Dictionary<string, float>();
            FillOrderDict();
            numForPlainText = new List<int>();
        }
        public String generatePlainText(String taskText,bool hasConstNums)
        {
            try
            {
                taskText = taskText.Split(new[] { "//" }, StringSplitOptions.RemoveEmptyEntries)[0];
                
            }
            catch
            {

            }

            String[] Text = taskText.Split(' ');
            int numsAdded = 0;

            
            for (int i=0; i<Text.Length;i++)
            {

                //Check for properties (blivums, siltumietilpiba) form tables
                if (Text[i].StartsWith("?"))
                {
                    String word2 = Text[i].Substring(1);
                    if (word2.Equals("blīvums"))
                    {
                        int keyNr = rnd.Next(TablesAndConstants.BlivumaTabula.Count - 1);
                        Text[i] = TablesAndConstants.BlivumaTabula.Keys.ElementAt(keyNr);
                        int blivumaVal;
                        TablesAndConstants.BlivumaTabula.TryGetValue(Text[i], out blivumaVal);                        
                    }

                }
				if (Text[i].Contains("{") && Text[i].Contains("}"))
				{
					int startIndex = Text[i].IndexOf('{') + 1;
					int endIndex = Text[i].IndexOf('}');
					bool isConst = false;
					String smallString = Text[i];
					String word2 =smallString.Substring(startIndex,endIndex-startIndex);
					if (word2.Contains("="))
					{
						word2 = word2.Split('=')[0];
						isConst = true;
					}
					if (isConst)
					{
						Text[i] = Text[i].Substring(0, Text[i].Length - 1).Split('=')[1];
					}
					else
					{
						if (hasConstNums)
						{
							try
							{
								Text[i] = numForPlainText[numsAdded].ToString();
							}
							catch
							{
								numForPlainText.Add(rnd.Next(1, 10));
								Text[i] = numForPlainText[numsAdded].ToString();
							}
							numsAdded++;
						}
						else
						{
							Text[i] = ((float)rnd.Next(1, 10)).ToString();
						}
					}
					
				}
            }

            String returnText = string.Join(" ", Text);
            //returnText= returnText.Replace("}", "");
            
            String[] list = returnText.Split(' ');
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Contains('='))
                {
                    list[i] = list[i].Split('=')[1];
                }
            }
            returnText = String.Join(" ", list);
            
            return returnText;

        }
        private Tuple< List<string[]>, List<string[]>> genNumBuf(String Text, List<string[]> formulaArr)
        {

            List<string[]>  numberArray = new List<string[]>();
            //List<object[]> orderArray = new List<object[]>();
            numberArray = DeepCopy(formulaArr);
			//orderArray = DeepCopy(formulaArr);
			//for (int i = 0; i < orderArray.Count; i++)
			//{
			//    for (int j = 0; j < orderArray[i].Length; j++)
			//    {
			//        orderArray[i][j] = "1";
			//    }
			//}
			String[] Texts = Text.Split(new[] { "//" }, StringSplitOptions.RemoveEmptyEntries);
			Texts[0] = Texts[0].Replace("=", " = ");
			Texts[0] = Texts[0].Replace(".", " .");
			Text = String.Join(" ", Texts);
            String[] taskText = Text.Split(' ');
			try
			{
				for (int k = 0; k < taskText.Length; k++)
				{
					float order = 1;
					bool isConst = false;
					string smallText = taskText[k];
					if (smallText.Contains("{") && smallText.Contains("}"))
					{
						int startIndex = smallText.IndexOf('{');
						int endindex = smallText.IndexOf('}') + 1;
						smallText = smallText.Substring(startIndex, endindex - startIndex);
						taskText[k] = smallText;
					}



					if (taskText[k].StartsWith("$") || taskText[k].StartsWith("?") || (taskText[k].StartsWith("{") && taskText[k].EndsWith("}")))
					{

						if (ShuntingYardAlgorithm.isFunction(taskText[k].Substring(1)) || ShuntingYardAlgorithm.IsOperator(taskText[k].Substring(1)) || taskText[k].Substring(1).Equals("(") || taskText[k].Substring(1).Equals(")"))
						{
							continue;
						}
						try
						{
							if (taskText[k + 1].StartsWith("#"))
							{

								order = orderDict[taskText[k + 1][1].ToString()];
								if (taskText[k + 1][2] == 'g')
								{
									order = order / 1000;
								}

							}
						}
						catch { }
						String word2 = taskText[k].Substring(1).Replace("}", "");
						if (word2.Equals("blīvums"))
						{
							word2 = "ρ";
						}
						if (word2.Contains("=") && word2[word2.Length - 1] != '=')
						{
							word2 = word2.Split('=')[0];
							isConst = true;
						}
						for (int i = 0; i < numberArray.Count; i++)
						{
							for (int j = 0; j < numberArray[i].Length; j++)
							{
								if (numberArray[i][j].Equals(word2))
								{
									if (isConst)
									{
										float value = float.Parse(taskText[k].Split('=')[1].Replace("}", "").Replace(",","."));
										numberArray[i][j] = (value * order).ToString();
									}
									else
									{
										if (numberArray[i][j].Equals("ρ"))
										{

											int blivums = TablesAndConstants.BlivumaTabula.Values.ElementAt(rnd.Next(TablesAndConstants.BlivumaTabula.Count - 1));
											numberArray[i][j] = blivums.ToString();
										}
										else
										{
											numberArray[i][j] = (rnd.Next(1, 10) * order).ToString();
										}
									}
									// orderArray[i][j] = order.ToString();
									break;
								}
								else
								{

								}
							}
						}


					}
				}
			}catch(Exception ex)
			{
				Console.Write(ex.Message);
			}
            return Tuple.Create(numberArray, new List<string[]>());          


        }

        public static T DeepCopy<T>(T obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;

                return (T)formatter.Deserialize(stream);
            }
        }

        //Checks if all variables are set for any equation
        private string[] ListFullnessChecker(List<string[]> numberArray)
        {
            foreach (string[] numArr in numberArray)
            {
                bool isGood = true;
                foreach (string value in numArr)
                {
                    float forTestParsre;

                    if (!ShuntingYardAlgorithm.isFunction(value) && !ShuntingYardAlgorithm.IsOperator(value) && !float.TryParse(value, out forTestParsre)
                        && !value.Equals("(") && !value.Equals(")"))
                        isGood = false;

                }
                if (isGood)
                    return numArr;
            }
            return null;

        }
        public Tuple<String,String> computeFormula(Uzdevums uzd)
        {

            List<String> solutions = new List<string>();
            List<string[]> formulaArray = new List<string[]>();
            String taskText = uzd.getText();

            foreach(String equation in uzd.getEquation())
            {
                List<String> parsedFormula = PievienotUzdevumuMaster.SplitFormula(equation.Split('=')[1]);
                formulaArray.Add(parsedFormula.ToArray());
            }
            //List<Stack<char>> operatorStack =  new List<Stack<char>>();
            //var tempList= uzd.getVarStacks().ToList();
            //foreach(String st in tempList)
            //{
            //    String[] str = st.Split(':');
            //    solutions.Add(str[0]);
            //    formulaArray.Add(str[1].Split(','));
            //}
            
            Tuple < List<string[]>,List <string[] >> Arrays  = genNumBuf(uzd.getText(), formulaArray);
            List<string[]> numberArray = Arrays.Item1;

            //var tempList2 = uzd.getOperatorStacks().ToList();
            //foreach (String st in tempList2)
            //{
            //    String[] charArr =st.Split(',');
            //    Stack<char> charStack = new Stack<char>();
            //    foreach(string str in charArr)
            //    {
            //        try
            //        {
            //            charStack.Push(Convert.ToChar(str));
            //        }
            //        catch { }
            //    }
            //    operatorStack.Add(charStack);
            //}

            if (uzd.getIsTheory())
            {
                return Tuple.Create(uzd.getText(),uzd.getAnswer());
            }
            else
            {
                              
                double result = 0;
                for (int j = 0; j < formulaArray.Count; j++)
                {
                    //check all numArrays, if they have all vars (all the variables of one equation are present in the text)
                    
                    //TODO::This Shit Does not pass if equation contain formula
                    string[] eqToSolve = ListFullnessChecker(numberArray);
                    if (eqToSolve == null)
                    {
                        return Tuple.Create("Neizdevās izpildīt uzdevumu. Visticamāk nav doti pietiekoši daudz mainīgie.", "-1");

                    }

                    //solve equation that has all varibles present in task text
                    //Stack<object> tempStack = new Stack<object>(eqToSolve.Reverse());
                    int index = numberArray.IndexOf(eqToSolve);

                    object[] eqToSolve2 = new object[eqToSolve.Length];
                    for (int k = 0; k < eqToSolve.Length; k++)
                    {
                        try
                        {
                            float f = 0;
                            if (float.TryParse(eqToSolve[k].ToString(), out f))
                            {
                                eqToSolve2[k] = (float.Parse(eqToSolve[k].ToString())).ToString();
                            }
                            else
                            {
                                eqToSolve2[k] = char.Parse(eqToSolve[k].ToString());
                            }
                        }
                        catch { }
                    }
                    var bllaa = eqToSolve2.Reverse();
                    Stack<object> tempStack = new Stack<object>(bllaa);
                    //var bal = operatorStack[index];
                    //Stack<char> tempStack2 = new Stack<char>(bal);
                    //List<String> a = numberArray.ConvertAll(obj => obj.ToString());
                    

                    List<string>  reversePolishNot = ShuntingYardAlgorithm.GetStacks(numberArray[j].ToList());

                    result = ShuntingYardAlgorithm.Calculate(reversePolishNot);

                    //Set up task Text                    
                    for (int i = 0; i < eqToSolve.Length; i++)
                    {
                        try
                        {
                            taskText = taskText.Replace("{" + formulaArray[index][i].ToString()+"}", eqToSolve[i].ToString());
                            int o = 0;
                            if (formulaArray[index][i].Equals("ρ")){
                                int blivums;
                                foreach (String key in TablesAndConstants.BlivumaTabula.Keys)
                                {
                                    TablesAndConstants.BlivumaTabula.TryGetValue(key, out blivums);
                                    int compareTo;
                                    int.TryParse(eqToSolve[i].ToString(),out compareTo);
                                    if (blivums == compareTo)
                                    {
                                        taskText = taskText.Replace("?blīvums", key +" kg/m^3");
                                        break;
                                    }
                                }
                                
                                //taskText.Replace("?blīvums",);

                            }

                        }
                        catch (Exception)
                        {
                            
                        }
                    }
                    //Insert solution in other equation
                    //string solution = solutions[index];
                    string solution = "adsadsadsadsad";
                    foreach (object[] numArr in numberArray)
                    {
                        foreach (object value in numArr)
                        {
                            try
                            {
                                if ((value as string).Equals(solution))
                                {
                                    int localIndex = Array.IndexOf(numArr, value);
                                    result = Math.Round(result, 3);
                                    numArr[localIndex] = result.ToString();
                                    eqToSolve[0] = "solved";
                                }
                            }
                            catch
                            {

                            }
                        }
                    }



                }

                taskText = generatePlainText(taskText,false);
                return Tuple.Create(taskText, result.ToString());
            }
        }
        private void FillOrderDict()
        {
            orderDict["E"] = 1000000000000000000;
            orderDict["P"] = 1000000000000000;
            orderDict["T"] = 1000000000000;
            orderDict["G"] = 1000000000;
            orderDict["M"] = 1000000;
            orderDict["k"] = 1000;
            orderDict["h"] = 100;
            orderDict["da"] = 10;
            orderDict["d"] = 0.1f;
            orderDict["c"] = 0.01f;
            orderDict["m"] = 0.001f;
            orderDict["μ"] = 0.000001f;
            orderDict["n"] = 0.000000001f;
            orderDict["p"] = 0.000000000001f;
            orderDict["f"] = 0.000000000000001f;
            orderDict["a"] = 0.000000000000000001f;
        }

    }
}
