﻿namespace masterVersion
{
    partial class Message
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_MF_OK = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.checkBox_MF_rem = new MetroFramework.Controls.MetroCheckBox();
            this.SuspendLayout();
            // 
            // but_MF_OK
            // 
            this.but_MF_OK.BackColor = System.Drawing.Color.Transparent;
            this.but_MF_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.but_MF_OK.Location = new System.Drawing.Point(238, 196);
            this.but_MF_OK.Name = "but_MF_OK";
            this.but_MF_OK.Size = new System.Drawing.Size(75, 42);
            this.but_MF_OK.TabIndex = 0;
            this.but_MF_OK.Text = "OK";
            this.but_MF_OK.UseVisualStyleBackColor = false;
            this.but_MF_OK.Click += new System.EventHandler(this.but_MF_OK_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(36, 62);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(276, 117);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // checkBox_MF_rem
            // 
            this.checkBox_MF_rem.AutoSize = true;
            this.checkBox_MF_rem.Location = new System.Drawing.Point(36, 208);
            this.checkBox_MF_rem.Name = "checkBox_MF_rem";
            this.checkBox_MF_rem.Size = new System.Drawing.Size(136, 15);
            this.checkBox_MF_rem.TabIndex = 2;
            this.checkBox_MF_rem.Text = "Otreiz nerādīt šo logu";
            this.checkBox_MF_rem.UseSelectable = true;
            this.checkBox_MF_rem.CheckedChanged += new System.EventHandler(this.checkBox_MF_rem_CheckedChanged);
            // 
            // Message
            // 
            this.AcceptButton = this.but_MF_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 261);
            this.Controls.Add(this.checkBox_MF_rem);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.but_MF_OK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Movable = false;
            this.Name = "Message";
            this.Resizable = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Message";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_MF_OK;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private MetroFramework.Controls.MetroCheckBox checkBox_MF_rem;
    }
}