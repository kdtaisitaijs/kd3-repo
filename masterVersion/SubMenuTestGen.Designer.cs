﻿namespace masterVersion
{
    partial class SubMenuTestGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.filepanel = new MetroFramework.Controls.MetroPanel();
            this.button3 = new MetroFramework.Controls.MetroButton();
            this.button2 = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.VariationCount = new MetroFramework.Controls.MetroComboBox();
            this.exportBtn = new MetroFramework.Controls.MetroButton();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.filepanel.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button6);
            this.flowLayoutPanel1.Controls.Add(this.button5);
            this.flowLayoutPanel1.Controls.Add(this.button7);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(973, 40);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // filepanel
            // 
            this.filepanel.Controls.Add(this.button3);
            this.filepanel.Controls.Add(this.button2);
            this.filepanel.HorizontalScrollbarBarColor = true;
            this.filepanel.HorizontalScrollbarHighlightOnWheel = false;
            this.filepanel.HorizontalScrollbarSize = 10;
            this.filepanel.Location = new System.Drawing.Point(6, 83);
            this.filepanel.Name = "filepanel";
            this.filepanel.Size = new System.Drawing.Size(145, 222);
            this.filepanel.TabIndex = 1;
            this.filepanel.VerticalScrollbarBarColor = true;
            this.filepanel.VerticalScrollbarHighlightOnWheel = false;
            this.filepanel.VerticalScrollbarSize = 10;
            this.filepanel.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 32);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Choose test";
            this.button3.UseSelectable = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Create test";
            this.button2.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.VariationCount);
            this.metroPanel1.Controls.Add(this.exportBtn);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 316);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(145, 222);
            this.metroPanel1.TabIndex = 2;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            this.metroPanel1.Visible = false;
            // 
            // VariationCount
            // 
            this.VariationCount.FormattingEnabled = true;
            this.VariationCount.ItemHeight = 23;
            this.VariationCount.Location = new System.Drawing.Point(6, 4);
            this.VariationCount.Name = "VariationCount";
            this.VariationCount.Size = new System.Drawing.Size(121, 29);
            this.VariationCount.TabIndex = 4;
            this.VariationCount.UseSelectable = true;
            // 
            // exportBtn
            // 
            this.exportBtn.Location = new System.Drawing.Point(3, 39);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(139, 23);
            this.exportBtn.TabIndex = 3;
            this.exportBtn.Text = "Export";
            this.exportBtn.UseSelectable = true;
            // 
            // button4
            // 
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 29);
            this.button4.TabIndex = 5;
            this.button4.Text = "Parādīt variantus";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.showVariants);
            this.button4.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button4.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // button1
            // 
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(119, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 29);
            this.button1.TabIndex = 6;
            this.button1.Text = "Pievienot variantu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.AddVariants);
            this.button1.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // button6
            // 
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(235, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(110, 29);
            this.button6.TabIndex = 7;
            this.button6.Text = "Izveidot PDF";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.CreatePdf);
            this.button6.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button6.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // button5
            // 
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(351, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 29);
            this.button5.TabIndex = 8;
            this.button5.Text = "Pievienot uzdevumu";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            this.button5.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button5.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // button7
            // 
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(482, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(125, 29);
            this.button7.TabIndex = 9;
            this.button7.Text = "Eksportēt kontroldarbu";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.exportTest);
            this.button7.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button7.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // SubMenuTestGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 551);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.filepanel);
            this.Controls.Add(this.flowLayoutPanel1);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Movable = false;
            this.Name = "SubMenuTestGen";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.Text = "SubMenuTestGen";
            this.Load += new System.EventHandler(this.SubMenuTestGen_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.filepanel.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroPanel filepanel;
        private MetroFramework.Controls.MetroButton button3;
        private MetroFramework.Controls.MetroButton button2;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroComboBox VariationCount;
        private MetroFramework.Controls.MetroButton exportBtn;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
    }
}