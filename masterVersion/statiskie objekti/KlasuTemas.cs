﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion
{
    public class KlasuTemas
    {
        Dictionary<int, List<string>>fizikastemas = new Dictionary<int, List<string>>();
        Dictionary<string, List<int>> subjectClasses = new Dictionary<string, List<int>>();
        bool update = true;
        List<string> klase8 = new List<string>();
        List<string> klase9 = new List<string>();
        List<string> klase10 = new List<string>();
        List<string> klase11 = new List<string>();
        List<string> klase12 = new List<string>();

        List<int> fizika = new List<int>();
        List<int> matematika = new List<int>();

        int klase;

        private void init()
        {
            if (update == true)
            {
                this.populateLists();
                this.populateDictionaries();
            }
        }


        private void populateLists()
        {
            fizika.Add(8);
            fizika.Add(9);
            fizika.Add(10);
            fizika.Add(11);
            fizika.Add(12);

            matematika.Add(4);
            matematika.Add(5);
            matematika.Add(6);
            matematika.Add(7);
            matematika.Add(8);
            matematika.Add(9);
            matematika.Add(10);
            matematika.Add(11);
            matematika.Add(12);
        }


        private void populateDictionaries()
        {
            klase8.Add("Viela");
            klase8.Add("Gaisma");
            klase8.Add("Skaņa");
            klase8.Add("Siltums");
            klase8.Add("Visas");
            //klase8.Add("Test"); //ToDo remove

            klase9.Add("Elektriskā strāva");
            klase9.Add("Elektriskie slēgumi");
            klase9.Add("Elektrība un magnēti");
            klase9.Add("Darbs un spēks");
            klase9.Add("Vielas uzbūve");
            klase9.Add("Visas");

            klase10.Add("Eksperimentālā zinātne");
            klase10.Add("Ķermeņu kustība");
            klase10.Add("Mijiedarbība un spēki");
            klase10.Add("Gravitācija un kustība gravitācijas laukā");
            klase10.Add("Enerģija un impulss");
            klase10.Add("Mehāniskās svārstības un viļņi");
            klase10.Add("Visas");

            klase11.Add("Gāzes");
            klase11.Add("Spēks un darbs");
            klase11.Add("Materiālu termiskās īpašības");
            klase11.Add("Elektriskie lādiņi un elektrostatiskie lauki");
            klase11.Add("Elektriskā strāva");
            klase11.Add("Elektrovadītspēja");
            klase11.Add("Magnētisms");
            klase11.Add("Visas");

            klase12.Add("Maiņstrāva");
            klase12.Add("Elektromagnētiskie viļņi");
            klase12.Add("Gaismas izplatīšanās");
            klase12.Add("Apgaismojums un attēli");
            klase12.Add("Interference un polarizācija");
            klase12.Add("Gaismas kvantu daba");
            klase12.Add("Atomi un kodolreakcijas");
            klase12.Add("Elementārdaļiņas");
            klase12.Add("Visas");

            fizikastemas.Add(8, klase8);
            fizikastemas.Add(9, klase9);
            fizikastemas.Add(10, klase10);
            fizikastemas.Add(11, klase11);
            fizikastemas.Add(12, klase12);

            subjectClasses.Add("Fizika", fizika);
           // subjectClasses.Add("Mathematics", matematika);
        }

        public List<string> getTemas(int _klase)
        {
            this.init();
            update = false;
            List<string> allTemas = new List<String>();
            if (_klase == 0)
            {
                for(int i = 0; i < klase8.Count; i++)
                {
                    allTemas.Add(klase8.ElementAt(i));
                }

                for(int i = 0; i < klase9.Count; i++)
                {
                    allTemas.Add(klase9.ElementAt(i));
                }

                for(int i = 0; i < klase10.Count; i++)
                {
                    allTemas.Add(klase10.ElementAt(i));
                }

                for(int i = 0; i < klase11.Count; i++)
                {
                    allTemas.Add(klase11.ElementAt(i));
                }

                for (int i = 0; i < klase12.Count; i++)
                {
                    allTemas.Add(klase12.ElementAt(i));
                }
            }
            else
            {
                update = false;
                fizikastemas.TryGetValue(_klase, out allTemas);
            }
            return allTemas;
        }

        public int getTemasKlasi(string _tema)
        {
            if (klase8.Contains(_tema))
            {
                klase =  8;
            }
            if (klase9.Contains(_tema))
            {
                klase = 9;
            }
            if (klase10.Contains(_tema))
            {
                klase = 10;
            }
            if (klase11.Contains(_tema))
            {
                klase = 11;
            }
            if (klase12.Contains(_tema))
            {
                klase = 12;
            }
            return klase;
        }

        public List<int> prieksmetaKlases(string _subject)
        {
            List<int> classes;
            this.init();
            update = false;
            subjectClasses.TryGetValue(_subject, out classes);

            return classes;
        }
    }
}
