﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masterVersion
{

    [Serializable()]
    public class KdData
    {
        public List<List<Uzdevums>> Uzdevumi = new List<List<Uzdevums>>();
        public string VariantuSk;
        public string KdTips;
        public string KdNosaukums;
        public string Prieksmets;
        public int Klase;
        public List<List<Tuple<SubMenuTestGen.WhiteSpace,int>>> WhiteSpaces = new List<List<Tuple<SubMenuTestGen.WhiteSpace,int>>>();
        public List<Dictionary<int, int>> Punkti = new List<Dictionary<int, int>>();
        public KdData()
        {

        }
        public KdData(List<List<Uzdevums>> uzdevumi,List<Dictionary<int,int>> punkti, int variantuSk, string kdTips, string kdNosaukums, int klase,string prieksmets,List<List<Tuple<SubMenuTestGen.WhiteSpace,int>>> whiteSpaces)
        {
            Prieksmets = prieksmets;
            Punkti = punkti;
            Klase = klase;
            Uzdevumi = uzdevumi;
            VariantuSk = variantuSk.ToString();
            KdTips = kdTips;
            KdNosaukums = kdNosaukums;
            WhiteSpaces = whiteSpaces;

        }
    }
}
